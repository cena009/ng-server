

package com.ng.sys.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ng.common.exception.NgException;
import com.ng.common.log.annotation.SysLog;
import com.ng.common.security.SecurityUtils;
import com.ng.common.utils.R;
import com.ng.sys.entity.SysMenuEntity;
import com.ng.sys.entity.SysUserEntity;
import com.ng.sys.service.SysMenuService;

/**
 * 系统菜单
 * 
 * @author lyf
 * @date 2017年10月27日 下午9:58:15
 */
@RestController
@RequestMapping("/sys/menu")
public class SysMenuController {
	@Autowired
	private SysMenuService sysMenuService;
//	@Autowired
//	private SysUserService sysUserService;
	
	@Autowired
	private SecurityUtils authUtils ;

	/**
	 * 导航菜单
	 */
	@SuppressWarnings("rawtypes")
	@GetMapping("/nav")
	public R<Map> nav(){
		
		SysUserEntity  user = authUtils.getCurrentUser();
		
		List<SysMenuEntity> menuList = sysMenuService.getUserMenuList(user.getUserId());
		Set<String> permissions = sysMenuService.queryAllPerms(user.getUserId());
		
		Map<String, Object> map = new HashMap<>();
		map.put("menuList", menuList);
		map.put("permissions", permissions);
		
		return R.ok(Map.class).setData(map);
	}
	
	/**
	 * 所有菜单列表
	 */
	@SuppressWarnings("rawtypes")
	@GetMapping("/list")
	public R<List> list(){
		
		QueryWrapper<SysMenuEntity> menuQuery = new QueryWrapper<>();
		menuQuery.orderByAsc("order_num");
		
		List<SysMenuEntity> menuList = sysMenuService.list(menuQuery);
		
		Map<String, String> id_name = new HashMap<>();
		for(SysMenuEntity m : menuList) {
			id_name.put(m.getMenuId(), m.getName());
		}
		
		for(SysMenuEntity sysMenuEntity : menuList){
			String pname = id_name.get(sysMenuEntity.getParentId());
			
			if(pname != null){
				sysMenuEntity.setParentName(pname);
			}
		}
		
		

		return R.ok(List.class).setData(menuList);
	}
	
	/**
	 * 选择菜单(添加、修改菜单)
	 */
	@SuppressWarnings("rawtypes")
	@GetMapping("/select")
	public R<List> select(){
		//查询列表数据
		List<SysMenuEntity> menuList = sysMenuService.queryNotButtonList();
		
		//添加顶级菜单
		SysMenuEntity root = new SysMenuEntity();
		root.setMenuId("0");
		root.setName("一级菜单");
		root.setParentId("-1");
		root.setOpen(true);
		menuList.add(root);
		
		return R.ok(List.class).setData( menuList);
	}
	
	/**
	 * 菜单信息
	 */
	@GetMapping("/info/{menuId}")
	public R<SysMenuEntity> info(@PathVariable("menuId") String menuId){
		SysMenuEntity menu = sysMenuService.getById(menuId);
		return R.ok(SysMenuEntity.class).setData( menu);
	}
	
	/**
	 * 保存
	 */
	@SysLog("保存菜单")
	@PostMapping("/save")
	@RequiresPermissions("sys:menu:save")
	public R<Object> save(@RequestBody SysMenuEntity menu){
		//数据校验
		verifyForm(menu);
		
		sysMenuService.save(menu);
		
		return R.ok();
	}
	
	/**
	 * 修改
	 */
	@SysLog("修改菜单")
	@PostMapping("/update")
	@RequiresPermissions("sys:menu:update")
	public R<Object> update(@RequestBody SysMenuEntity menu){
		//数据校验
		verifyForm(menu);
				
		sysMenuService.updateById(menu);
		
		return R.ok();
	}
	
	/**
	 * 删除
	 */
	@SysLog("删除菜单")
	@PostMapping("/delete/{menuId}")
	@RequiresPermissions("sys:menu:delete")
	public R<Object> delete(@PathVariable("menuId") String menuId){
		/*if(menuId <= 31){
			return R.error("系统菜单，不能删除");
		}
*/
		//判断是否有子菜单或按钮
		List<SysMenuEntity> menuList = sysMenuService.queryListParentId(menuId);
		if(menuList.size() > 0){
			return R.error("请先删除子菜单或按钮");
		}

		sysMenuService.delete(menuId);

		return R.ok();
	}
	
	/**
	 * 验证参数是否正确
	 */
	private void verifyForm(SysMenuEntity menu){
		if(StringUtils.isBlank(menu.getName())){
			throw new NgException("菜单名称不能为空");
		}
		
		if(menu.getParentId() == null){
			throw new NgException("上级菜单不能为空");
		}
		
		//菜单
		/*if(menu.getType() == MenuType.MENU ){
			if(StringUtils.isBlank(menu.getUrl())){
				throw new RRException("菜单URL不能为空");
			}
		}
		
		//上级菜单类型
		MenuType parentType = MenuType.CATALOG ;
		if(!menu.getParentId().equals("0")){
			SysMenuEntity parentMenu = sysMenuService.getById(menu.getParentId());
			parentType = parentMenu.getType();
		}*/
		
		//目录、菜单
		/*if(menu.getType() == MenuType.CATALOG.getValue() ||
				menu.getType() == MenuType.MENU.getValue()){
			if(parentType != MenuType.CATALOG.getValue()){
				throw new RRException("上级菜单只能为目录类型");
			}
			return ;
		}
		
		//按钮
		if(menu.getType() == MenuType.BUTTON.getValue()){
			if(parentType != MenuType.MENU.getValue()){
				throw new RRException("上级菜单只能为菜单类型");
			}
			return ;
		}*/
	}
}
