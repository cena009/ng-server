package com.ng.sys.controller;

import java.util.Arrays;
import java.util.List;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ng.common.log.annotation.SysLog;
import com.ng.common.log.annotation.SysModule;
import com.ng.common.utils.PageUtils;
import com.ng.common.utils.R;
import com.ng.sys.entity.SysDictEntity;
import com.ng.sys.service.SysDictService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;



/**
 * 数据字典
 *
 * @author lyf
 * @email jjxliu306@163.com
 * @date 2019-03-20 10:54:34
 */
@RestController
@RequestMapping("sys/sysdict")
@Api(value = "字典表-通用", tags = {"字典表-通用"})
public class SysDictController {
    @Autowired
    private SysDictService sysDictService;

    /**
     * 列表
     */
    @GetMapping("/list")
    @ApiOperation(httpMethod = "GET",value ="字典表列表信息")
   // @RequiresPermissions("sys:sysdict:list")
    public R<PageUtils> list(SysDictEntity entity , PageUtils page){
        PageUtils pageData = sysDictService.queryPage(entity , page);

        return R.ok(PageUtils.class).setData(pageData);
    }
    
    /**
     * 列表
     */
    @GetMapping("/getAll")
    @ApiOperation(httpMethod = "GET",value ="字典表全部信息")
   // @RequiresPermissions("sys:sysdict:list")
    public R<Object> getAll(SysDictEntity entity , PageUtils page){
        List<SysDictEntity> list = sysDictService.list();

        return R.ok().setData(list);
    }
    
    /**
     *  类型列表
     */
    @GetMapping("/types")
    @ApiOperation(httpMethod = "GET",value ="字典表类型列表信息")
   // @RequiresPermissions("sys:sysdict:list")
    public R<PageUtils> types(String type, PageUtils page){
        PageUtils pageData = sysDictService.queryTypePage(type, page);

        return R.ok(PageUtils.class).setData(pageData);
    } 
    /**
     * 信息
     */
    @GetMapping("/info/{dictId}")
    @ApiOperation("查询字典表详情")
    public R<SysDictEntity> info(@PathVariable("dictId")  String dictId){
			SysDictEntity sysDict = sysDictService.getById(dictId);

        return R.ok(SysDictEntity.class).setData( sysDict);
    }


    /**
     * 保存
     */
    @PostMapping("/save")
    @ApiOperation(httpMethod = "POST",value ="保存字典表信息")
    @SysLog(value="保存字典表信息" , system=SysModule.sys)
    @RequiresPermissions("sys:sysdict:save")
    public R<Object> save(@RequestBody SysDictEntity sysDict){
			sysDictService.save(sysDict);

        return R.ok();
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    @ApiOperation(httpMethod = "POST",value ="修改字典表信息")
    @SysLog(value="修改字典表信息" , system=SysModule.sys)
    @RequiresPermissions("sys:sysdict:update")
    public R<Object> update(@RequestBody SysDictEntity sysDict){
			sysDictService.updateById(sysDict);

        return R.ok();
    }
    

    /**
     * 修改
     */
    @PostMapping("/updateType")
    @ApiOperation(httpMethod = "POST",value ="修改字典表信息")
    @SysLog(value="修改字典表信息" , system=SysModule.sys)
    @RequiresPermissions("sys:sysdict:update")
    public R<Object> updateType(@RequestBody SysDictEntity sysDict){
		sysDictService.updateType(sysDict);

        return R.ok();
    }
    
    
    @SuppressWarnings("rawtypes")
	@GetMapping("/findByType")
    @ApiOperation(httpMethod = "GET",value ="按照类型查询字典表信息") 
    public R<List> findByType(String type) {
    	
    	List<SysDictEntity> list = sysDictService.findByType(type);
    	
    	return R.ok(List.class).setData(list);
    }
    
    @GetMapping("/findByTypeValue")
    @ApiOperation(httpMethod = "GET",value ="按照类型和值查询字典表信息") 
    public R<SysDictEntity> findByTypeValue(String type ,String value) {
    	 
    	SysDictEntity data = sysDictService.byTypeDictValue(type , value);
    
    	return R.ok(SysDictEntity.class).setData( data);
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    @ApiOperation(httpMethod = "POST",value ="删除字典表信息")
    @RequiresPermissions("sys:sysdict:delete")
    @SysLog(value="删除字典表信息" , system=SysModule.sys)
    public R<Object> delete(@RequestBody String[] dictIds){
			sysDictService.removeByIds(Arrays.asList(dictIds));

        return R.ok();
    }


    /**
     * 删除
     */
    @PostMapping("/deleteType")
    @ApiOperation(httpMethod = "POST",value ="删除字典表信息")
    @RequiresPermissions("sys:sysdict:delete")
    @SysLog(value="删除字典表信息" , system=SysModule.sys)
    public R<Object> deleteType(@RequestBody String type){
    	QueryWrapper<SysDictEntity> query = new QueryWrapper<>();
    	query.eq("type", type);
    	
		sysDictService.remove(query);

        return R.ok();
    }

     
    

}
