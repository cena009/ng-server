

package com.ng.sys.controller;


import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ng.common.log.annotation.SysLog;
import com.ng.common.utils.PageUtils;
import com.ng.common.utils.R;
import com.ng.common.validator.ValidatorUtils;
import com.ng.sys.entity.SysConfigEntity;
import com.ng.sys.service.SysConfigService;

/**
 * 系统配置信息
 * 
 * @author lyf
 * @email liuyf@gs-softwares.com
 * @date 2016年12月4日 下午6:55:53
 */
@RestController
@RequestMapping("/sys/config")
public class SysConfigController {
	@Autowired
	private SysConfigService sysConfigService;
	 
	
	/**
	 * 所有配置列表
	 */
	@GetMapping("/list")
//	@RequiresPermissions("sys:config:list")
	public R<PageUtils> list(SysConfigEntity entity , PageUtils page){
		PageUtils pageData = sysConfigService.queryPage(entity , page);

		return R.ok(PageUtils.class).setData(pageData);
	}
/*	
	@GetMapping("/clearCache")
	public R<Object> clearCache() {
		
		redisUtils.clearMemCache();
		
		return R.ok();
	}*/
	
	/**
	 * 配置信息
	 */
	@GetMapping("/info/{id}")
//	@RequiresPermissions("sys:config:info")
	public R<SysConfigEntity> info(@PathVariable("id") String id){
		SysConfigEntity config = sysConfigService.getById(id);
		
		return R.ok(SysConfigEntity.class).setData(config);
	}
	
	 
	
	/**
	 * 保存配置
	 */
	@SysLog("保存配置")
	@PostMapping("/save")
	@RequiresPermissions("sys:config:save")
	public R<Object> save(@RequestBody SysConfigEntity config){
		ValidatorUtils.validateEntity(config);

		sysConfigService.save(config);
		
		return R.ok();
	}
	
	/**
	 * 修改配置
	 */
	@SysLog("修改配置")
	@PostMapping("/update")
	@RequiresPermissions("sys:config:update")
	public R<Object> update(@RequestBody SysConfigEntity config){
		ValidatorUtils.validateEntity(config);
		
		sysConfigService.updateById(config);
		
		return R.ok();
	}
	
	/**
	 * 删除配置
	 */
	@SysLog("删除配置")
	@PostMapping("/delete")
	@RequiresPermissions("sys:config:delete")
	public R<Object> delete(@RequestBody String[] keys){
		sysConfigService.deleteBatch(keys);
		
		return R.ok();
	}

 



}
