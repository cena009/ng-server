

package com.ng.sys.controller;


import java.util.LinkedList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 系统配置信息
 * 
 * @author lyf
 * @email liuyf@gs-softwares.com
 * @date 2016年12月4日 下午6:55:53
 */
@RestController
@RequestMapping
public class SysAnonController {
	 
	
	/**
	 * 所有配置列表
	 */
	@GetMapping("/anons") 
	public List<String> list(){
		List<String> anosUrls = new LinkedList<>();
		
		anosUrls.add("/sys/sysdict/*");
		anosUrls.add("/sys/api/*");
		anosUrls.add("/sys/config/*"); 
		 
		return anosUrls;
	}
	 

	
	
}
