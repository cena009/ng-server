package com.ng.sys.controller;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ng.common.log.annotation.SysLog;
import com.ng.common.security.SecurityUtils;
import com.ng.common.utils.PageUtils;
import com.ng.common.utils.R;
import com.ng.sys.entity.SysRoleEntity;
import com.ng.sys.entity.SysUserEntity;
import com.ng.sys.service.SysRoleMenuService;
import com.ng.sys.service.SysRoleService;

/**
 * 角色管理
 * 
 * @author lyf
 * @email liuyf@gs-softwares.com
 * @date 2016年11月8日 下午2:18:33
 */
@RestController
@RequestMapping("/sys/role")
public class SysRoleController  {
	@Autowired
	private SysRoleService sysRoleService;
	@Autowired
	private SysRoleMenuService sysRoleMenuService;
	
	@Autowired
	private SecurityUtils authUtils;

	/**
	 * 角色列表
	 */
	@GetMapping("/list")
//	@RequiresPermissions("sys:role")
	public R<PageUtils> list(SysRoleEntity entity , PageUtils page){
		//如果不是超级管理员，则只查询自己创建的角色列表
//		if(!getUserId().equals(Constant.SUPER_ADMIN) ){
//			params.put("createUserId", getUserId());
//		}

		PageUtils pageData = sysRoleService.queryPage(entity , page);

		return R.ok(PageUtils.class).setData(pageData);
	}
	
	/**
	 * 角色列表
	 */
	@GetMapping("/select")
//	@RequiresPermissions("sys:role")
	public R<Object> select(){
		Map<String, Object> map = new HashMap<>();
		
		//如果不��超级管理员，则只查询自己所拥有的角色列表
//		if(!getUserId().equals(Constant.SUPER_ADMIN)){
//			map.put("createUserId", getUserId());
//		}
		Collection<SysRoleEntity> list = sysRoleService.listByMap(map);
		
		return R.ok().setData( list);
	}
	
	/**
	 * 角色信息
	 */
	@GetMapping("/info/{roleId}")
//	@RequiresPermissions("sys:role")
	public R<SysRoleEntity> info(@PathVariable("roleId") String roleId){
		SysRoleEntity role = sysRoleService.getById(roleId);
		
		//查询角色对应的菜单
		List<String> menuIdList = sysRoleMenuService.queryMenuIdList(roleId);
		role.setMenuIdList(menuIdList);
		
		return R.ok(SysRoleEntity.class).setData(role);
	}
	
	/**
	 * 保存角色
	 */
	@SysLog("保存角色")
	@PostMapping("/save")
	@RequiresPermissions("sys:role:save")
	public R<Object> save(@RequestBody SysRoleEntity role){
//		ValidatorUtils.validateEntity(role);
		SysUserEntity  user = authUtils.getCurrentUser();
		role.setCreateUserId(user.getUserId());
		sysRoleService.save(role);
		
		return R.ok();
	}
	
	/**
	 * 修改角色
	 */
	@SysLog("修改角色")
	@PostMapping("/update")
	@RequiresPermissions("sys:role:update")
	public R<Object> update(@RequestBody SysRoleEntity role){
//		ValidatorUtils.validateEntity(role);
		
		SysUserEntity  user = authUtils.getCurrentUser();
		
		role.setCreateUserId(user.getUserId());
		sysRoleService.update(role);
		
		return R.ok();
	}
	
	/**
	 * 删除角色
	 */
	@SysLog("删除角色")
	@PostMapping("/delete")
	@RequiresPermissions("sys:role:delete")
	public R<Object> delete(@RequestBody String[] roleIds){
		sysRoleService.deleteBatch(roleIds);
		
		return R.ok();
	}
}
