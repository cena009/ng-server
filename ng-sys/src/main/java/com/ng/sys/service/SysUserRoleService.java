package com.ng.sys.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ng.sys.entity.SysUserRoleEntity;



/**
 * 用户与角色对应关系
 * 
 * @author lyf
 * @date 2017年9月18日 上午9:43:24
 */
public interface SysUserRoleService extends IService<SysUserRoleEntity> {
	
	void saveOrUpdate(String userId, List<String> roleIdList);
	
	/**
	 * 根据用户ID，获取角色ID列表
	 */
	List<String> queryRoleIdList(String userId);

	/**
	 * 根据角色ID数组，批量删除
	 */
	int deleteBatch(String[] roleIds);
}
