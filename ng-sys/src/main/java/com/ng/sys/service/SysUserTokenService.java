package com.ng.sys.service;

import java.util.Set;

/**
 * 用户Token
 * 
 * @author lyf
 * @email liuyf@gs-softwares.com
 * @date 2017-03-23 15:22:07
 */
public interface SysUserTokenService  {

	/**
	 * 生成token
	 * @param userId  用户ID
	 */
	String createToken(String userId);

	/**
	 * 退出，修改token值
	 * @param userId  用户ID
	 */
	void logout(String userId);
	
	/**
	 * 根据token获取用户ID 针对不能重复登录的时候,能重复登录的时候基于token的jwt去解析用户ID
	 * @param token
	 * @return
	 */
	String queryUserIdByToken(String token);
	
	/**
	 * 根据用户ID获取当前用户的token 针对不能重复登录的时候
	 * @param userId
	 * @return
	 */
	public Set<String> queryTokenByUserId(String userId);
	
	/**
	 * 刷新用户的token过期时间
	 * @param token
	 */
	void expireToken(String token);

	/**
	 * 清理用户的登录信息缓存
	 * @param userId
	 */
	void clearUserTokenCache(String userId);

}
