package com.ng.sys.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ng.common.cache.annotation.TrCache;
import com.ng.common.cache.annotation.TrCacheEvict;
import com.ng.sys.dao.SysUserRoleDao;
import com.ng.sys.entity.SysUserRoleEntity;
import com.ng.sys.service.SysUserRoleService;



/**
 * 用户与角色对应关系
 * 
 * @author lyf 
 * @date 2017年9月18日 上午9:45:48
 */
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleDao, SysUserRoleEntity> implements SysUserRoleService {

	@Override
	@TrCacheEvict("#userId")
	public void saveOrUpdate(String userId, List<String> roleIdList) {
		//先删除用户与角色关系
		
		UpdateWrapper<SysUserRoleEntity> deleteWrapper = new UpdateWrapper<>();
		deleteWrapper.eq("user_id", userId);
				
				
		this.remove(deleteWrapper);
		
		//this.removeByMap(new MapUtils().put("user_id", userId));

		if(roleIdList == null || roleIdList.size() == 0){
			return ;
		}

		//保存用户与角色关系
		List<SysUserRoleEntity> list = new ArrayList<>(roleIdList.size());
		for(String roleId : roleIdList){
			SysUserRoleEntity sysUserRoleEntity = new SysUserRoleEntity();
			sysUserRoleEntity.setUserId(userId);
			sysUserRoleEntity.setRoleId(roleId);

			list.add(sysUserRoleEntity);
		}
		this.saveBatch(list);
	}

	@Override
	@TrCache()
	public List<String> queryRoleIdList(String userId) {
		return baseMapper.queryRoleIdList(userId);
	}

	@Override
	@TrCacheEvict
	public int deleteBatch(String[] roleIds){
		return baseMapper.deleteBatch(roleIds);
	}
}
