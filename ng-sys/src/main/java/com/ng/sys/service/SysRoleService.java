package com.ng.sys.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ng.common.utils.PageUtils;
import com.ng.sys.entity.SysRoleEntity;


/**
 * 角色
 * 
 * @author lyf
 * @email liuyf@gs-softwares.com
 * @date 2016年9月18日 上午9:42:52
 */
public interface SysRoleService extends IService<SysRoleEntity> {

	PageUtils queryPage(SysRoleEntity entity , PageUtils page);
 

	void update(SysRoleEntity role);

	void deleteBatch(String[] roleIds);

	
	/**
	 * 查询用户创建的角色ID列表
	 */
	List<Long> queryRoleIdList(String createUserId);
	
	SysRoleEntity getById(String roleId);


	boolean save(SysRoleEntity role);


	List<SysRoleEntity> list();
}
