
package com.ng.sys.service.impl;

import java.awt.image.BufferedImage;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.code.kaptcha.Producer;
import com.ng.common.exception.NgException;
import com.ng.common.redis.RedisUtils;
import com.ng.sys.dao.SysCaptchaDao;
import com.ng.sys.entity.SysCaptchaEntity;
import com.ng.sys.service.SysCaptchaService;

/**
 * 验证码
 *
 * @author Mark sunlightcs@gmail.com
 * @since 2.0.0 2018-02-10
 */
@Service
public class SysCaptchaServiceImpl extends ServiceImpl<SysCaptchaDao, SysCaptchaEntity> implements SysCaptchaService {
    @Autowired
    private Producer producer;
    
    @Autowired
    private RedisUtils redisUtils;
     

    @Override
    public BufferedImage nextCaptcha(String uuid) {
        if(StringUtils.isBlank(uuid)){
            throw new NgException("uuid不能为空");
        }
        //生成文字验证码
        String code = producer.createText();
        
        SysCaptchaEntity captchaEntity = new SysCaptchaEntity();
        captchaEntity.setUuid(uuid);
        captchaEntity.setCode(code);
        //5分钟后过期 
        
        captchaEntity.setExpireTime(DateUtils.addMonths(new Date(),  5));
        
        
        //开始redis 则只在redis中进行验证码的缓存 不走数据库
        redisUtils.set(getClass().getSimpleName() + "-" + uuid, captchaEntity ,   5 * 60);
        	 
       
         

        return producer.createImage(code);
    }

    @Override
    public boolean validate(String uuid, String code) {
    	 SysCaptchaEntity captchaEntity = null;
    	
    	//从redis取
    	captchaEntity = redisUtils.get(getClass().getSimpleName() + "-" + uuid , SysCaptchaEntity.class);
    	
        if(captchaEntity == null){
            return false;
        }
 

        if(captchaEntity.getCode().equalsIgnoreCase(code) && captchaEntity.getExpireTime().getTime() >= System.currentTimeMillis()){
            return true;
        }

        return false;
    }
}
