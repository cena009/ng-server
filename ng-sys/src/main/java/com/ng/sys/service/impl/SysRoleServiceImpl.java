package com.ng.sys.service.impl;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ng.common.cache.annotation.TrCache;
import com.ng.common.cache.annotation.TrCacheEvict;
import com.ng.common.exception.NgException;
import com.ng.common.redis.RedisUtils;
import com.ng.common.utils.Constant;
import com.ng.common.utils.PageUtils;
import com.ng.common.utils.Query;
import com.ng.sys.dao.SysMenuDao;
import com.ng.sys.dao.SysRoleDao;
import com.ng.sys.entity.SysRoleEntity;
import com.ng.sys.service.SysMenuService;
import com.ng.sys.service.SysRoleMenuService;
import com.ng.sys.service.SysRoleService;
import com.ng.sys.service.SysUserRoleService;

/**
 * 角色
 * 
 * @author lyf
 * @email liuyf@gs-softwares.com
 * @date 2016年9月18日 上午9:45:12
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleDao, SysRoleEntity> implements SysRoleService {
	@Autowired
	private SysRoleMenuService sysRoleMenuService;
 
    @Autowired
    private SysUserRoleService sysUserRoleService;
 
    
    @Autowired
    private SysMenuDao sysMenuDao ;
    
    @Autowired
    private RedisUtils redisUtils;

	@Override
	public PageUtils queryPage(SysRoleEntity entity , PageUtils page) {
		String roleName = entity.getRoleName() ;// (String)params.get("roleName");
		//String createUserId =  (String)params.get("createUserId");

		IPage<SysRoleEntity> ipage = this.page(
			new Query<SysRoleEntity>(page).getPage(),
			new QueryWrapper<SysRoleEntity>()
				.like(StringUtils.isNotBlank(roleName),"role_name", roleName)
				//.eq(createUserId != null,"create_user_id", createUserId)
		);

		return new PageUtils(ipage);
	}

    @Override
    @Transactional(rollbackFor = Exception.class)
    @TrCacheEvict 
    public boolean save(SysRoleEntity role) {
        role.setCreateTime(new Date());
        boolean s = super.save(role);

        //检查权限是否越权
        checkPrems(role);

        //保存角色与菜单关系
        sysRoleMenuService.saveOrUpdate(role.getRoleId(), role.getMenuIdList());
        
        return s ;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(SysRoleEntity role) {
        this.updateById(role);

        //检查权限是否越权
        checkPrems(role);

        //更新角色与菜单关系
        sysRoleMenuService.saveOrUpdate(role.getRoleId(), role.getMenuIdList());
        
        clearRoleUserCache();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @TrCacheEvict
    public void deleteBatch(String[] roleIds) {
        //删除角色
        this.removeByIds(Arrays.asList(roleIds));

        //删除角色与菜单关联
        sysRoleMenuService.deleteBatch(roleIds);

        //删除角色与用户关联
        sysUserRoleService.deleteBatch(roleIds);
        
        clearRoleUserCache();
    }


    @Override
	public List<Long> queryRoleIdList(String createUserId) {
		return baseMapper.queryRoleIdList(createUserId);
	}

	/**
	 * 检查权限是否越权
	 */
	private void checkPrems(SysRoleEntity role){
		//如果不是超级管理员，则需要判断角色的权限是否超过自己的权限
		if(role.getCreateUserId().equals(Constant.SUPER_ADMIN) ){
			return ;
		}
		
		//查询用户所拥有的菜单列表
		List<String> menuIdList = sysMenuDao.queryAllMenuId(role.getCreateUserId());
		
		//判断是否越权
		if(!menuIdList.containsAll(role.getMenuIdList())){
			throw new NgException("新增角色的权限，已超出你的权限范围");
		}
	}
	
	@Override
	@TrCache
	public List<SysRoleEntity> list() {
		// TODO Auto-generated method stub
		return super.list();
	}
 

	@Override
	@TrCache
	public SysRoleEntity getById(String roleId) {
		// TODO Auto-generated method stub
		return super.getById(roleId);
	}
	
	/**
     * 清理用户得缓存，规避角色清理或者更新以后用户得权限信息不发生变动
     * 2020-01-28
     */
    private void clearRoleUserCache( ) {
    	
    	redisUtils.deleteStartKey(SysUserRoleServiceImpl.class.getSimpleName());
    	
    	// 2021-02-28 清理所有用户的user_perms 和 user_menus 缓存
    	
    	redisUtils.deleteContains(SysMenuService.CACHE_USER_MENU);
    	redisUtils.deleteContains(SysMenuService.CACHE_USER_PERM);
    }
}
