package com.ng.sys.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ng.common.utils.PageUtils;
import com.ng.sys.entity.SysDictEntity;

/**
 * 数据字典
 *
 * @author lyf
 * @email jjxliu306@163.com
 * @date 2019-03-20 10:54:34
 */
public interface SysDictService extends IService<SysDictEntity> {

    PageUtils queryPage(SysDictEntity entity , PageUtils page);
    
	public PageUtils queryTypePage(String type , PageUtils page) ;
	
	/**
	 * 更新数据字典类型数据
	 * @param entity
	 */
	public void updateType(SysDictEntity entity);
	
	/**
	 * 根据字典分类查询
	 * @param type
	 * @return
	 */
	public List<SysDictEntity> findByType(String type);
	 
    
    /**
     * 根据 type ,dict_value 联合查询数据
     */
    SysDictEntity byTypeDictValue(String type,String value);

    /**
     * 根据 type ,dict_value 联合查询数据
     */
    List<SysDictEntity> selectByTypeValue(String type,List<String> value);

	/**
	 *  根据多个拼接vlue返回一个翻译后的拼接字符串
	 * @param type
	 * @param value
	 * @return
	 */
    public String findByTypeAndValue(String type,String value);

}

