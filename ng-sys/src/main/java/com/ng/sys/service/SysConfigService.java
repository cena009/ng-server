

package com.ng.sys.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.ng.common.utils.PageUtils;
import com.ng.sys.entity.SysConfigEntity;

/**
 * 系统配置信息
 * 
 * @author lyf
 * @email liuyf@gs-softwares.com
 * @date 2016年12月4日 下午6:49:01
 */
public interface SysConfigService extends IService<SysConfigEntity>  {

	PageUtils queryPage(SysConfigEntity entity , PageUtils page);
	 
	
	/**
	 * 根据key，更新value
	 */
	public void updateValueByKey(String key, String value);
	
	/**
	 * 删除配置信息
	 */
	public void deleteBatch(String[] keys);
	
	/**
	 * 根据key，获取配置的value值
	 * 
	 * @param key           key
	 */
	public String getValue(String key);
	
	/**
	 * 根据key，获取value的Object对象
	 * @param key    key
	 * @param clazz  Object对象
	 */
	public <T> T getConfigObject(String key, Class<T> clazz);
	
}
