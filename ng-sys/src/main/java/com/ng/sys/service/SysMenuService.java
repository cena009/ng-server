

package com.ng.sys.service;


import java.util.List;
import java.util.Set;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ng.sys.entity.SysMenuEntity;


/**
 * 菜单管理
 * 
 * @author lyf
 * @email liuyf@gs-softwares.com
 * @date 2016年9月18日 上午9:42:16
 */
public interface SysMenuService extends IService<SysMenuEntity> {
 

	/**
	 * 用户菜单缓存
	 */
	public static final String CACHE_USER_MENU = "user_menu";
	
	/**
	 * 用户权限缓存
	 */
	public static final String CACHE_USER_PERM = "user_perm";

	
	/**
	 * 根据父菜单，查询子菜单
	 * @param parentId 父菜单ID
	 */
	List<SysMenuEntity> queryListParentId(String parentId);
	
	/**
	 * 查询用户所有得权限码
	 * @param userId
	 * @return
	 */
	Set<String> queryAllPerms(String userId);
	
	/**
	 * 获取不包含按钮的菜单列表
	 */
	List<SysMenuEntity> queryNotButtonList();
	
	/**
	 * 获取用户菜单列表
	 */
	List<SysMenuEntity> getUserMenuList(String userId);

	/**
	 * 删除
	 */
	void delete(String menuId);

	/**
	 * 清理用户的权限和菜单缓存
	 * @param userId
	 */
	void clearUserPerms(String userId);
	 
 
}
