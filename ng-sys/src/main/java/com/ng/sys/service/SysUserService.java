package com.ng.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ng.common.utils.PageUtils;
import com.ng.sys.entity.SysUserEntity;


/**
 * 系统用户
 * 
 * @author lyf
 * @email liuyf@gs-softwares.com
 * @date 2016年9月18日 上午9:43:39
 */
public interface SysUserService extends IService<SysUserEntity> {

	PageUtils queryPage(SysUserEntity entity , PageUtils page);
 

	/**
	 * 根据用户名，查询系统用户
	 */
	SysUserEntity queryByUserNo(String userNo);
 

	/**
	 * 修改用户
	 */
	boolean saveUser(SysUserEntity user);

	/**
	 * 修改用户
	 */
	void update(SysUserEntity user);
	
	/**
	 * 删除用户
	 */
	void deleteBatch(String[] userIds);

	/**
	 * 修改密码
	 * @param userId       用户ID
	 * @param password     原密码
	 * @param newPassword  新密码
	 */
	boolean updatePassword(String userId, String password, String newPassword);
 
	
	/**
	 * 重置用户密码
	 * @param userId 用户ID
	 * @param newPass 新密码,经过加密后的
	 * @return
	 */
	public boolean resetPass(String userId , String newPass);
  
  

	SysUserEntity getById(String userId);
}
