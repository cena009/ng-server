package com.ng.sys.form;

import lombok.Data;

@Data
public class ResetPassForm {
	
	private String userId;
	
	private String newPass;

}
