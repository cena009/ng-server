package com.ng.sys.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ng.sys.entity.SysUserEntity;

/**
 * 系统用户
 * 
 * @author lyf
 * @date 2017年9月18日 上午9:34:11
 */
@Mapper
public interface SysUserDao extends BaseMapper<SysUserEntity> {
	

	 
	/**
	 * 重置管理员用户密码
	 * @param userId
	 * @param newPass
	 */
	@Update("update sys_user set password = #{password} where user_id = #{userId}")
	public void resetPass(@Param("userId") String userId ,@Param("password") String newPass);



	/**
	 * @Author: linfei
	 * @Description: 用户名 唯一性校验
	 * @Param:
	 * @return:
	 * @Date: 2020/1/9
	 */
	@Select("select count(*) from sys_user where user_no = #{userNo}")
	int uniquenessCheckUserNo(String userNo);

	@Update("<script>" + 
		"  update sys_user " + 
		" set  " +
		"     <if test='user.password != null and user.password !=\"\"  '>" +
	    "        password  = #{user.password} ," +
	    "     </if>" +   
	    "        status  = #{user.status} ," + 
	    "        sex  = #{user.sex} ," + 
	    "     mobile  = #{user.mobile} ," + 
	    "        username  = #{user.username} ," + 
	    "        depart_id  = #{user.departId} " +  
	    " where  user_id = #{user.userId} " + 
	    "</script>")
	public void updateUser(@Param("user") SysUserEntity user);
}
