package com.ng.sys.dao;


import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ng.sys.entity.SysConfigEntity;

/**
 * 系统配置信息
 * 
 * @author lyf
 * @email liuyf@gs-softwares.com
 * @date 2016年12月4日 下午6:46:16
 */
@Mapper
public interface SysConfigDao extends BaseMapper<SysConfigEntity> {

 
	
}
