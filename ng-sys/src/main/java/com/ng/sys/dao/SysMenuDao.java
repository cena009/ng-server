

package com.ng.sys.dao;

import java.util.List;
import java.util.Set;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ng.sys.entity.SysMenuEntity;

/**
 * 菜单管理
 * 
 * @author lyf
 * @email liuyf@gs-softwares.com
 * @date 2016年9月18日 上午9:33:01
 */
@Mapper
public interface SysMenuDao extends BaseMapper<SysMenuEntity> {
	/**
	 * 根据父菜单，查询子菜单
	 * @param parentId 父菜单ID
	 */
	@Select("select * from sys_menu where parent_id = #{parentId} and enabled = 1 order by order_num asc")
	List<SysMenuEntity> queryListParentId(@Param("parentId") String parentId);
	
	/**
	 * 获取不包含按钮的菜单列表
	 */
	@Select("select * from   sys_menu where type < 2   order by order_num asc")
	List<SysMenuEntity> queryNotButtonList();

	/**
	 * 通过ID获取菜单列表
	 */
	@Select("select f.* FROM   sys_menu f RIGHT JOIN (" +
			" select distinct c.menu_id from   sys_role_menu c RIGHT JOIN" +
			" (SELECT b.role_id FROM   sys_user a " +
			" LEFT JOIN   sys_user_role b ON a.user_id = b.user_id" +
			" where a.user_id = #{userId}) d on  c.role_id = d .role_id ) e on  f.menu_id = e.menu_id  order by f.order_num asc")
	List<SysMenuEntity> queryMenuById(String userId);
	
	/**
	 * 查询用户的所有菜单ID
	 */
	@Select("select distinct rm.menu_id from  sys_user_role ur  " + 
			"			LEFT JOIN   sys_role_menu rm on ur.role_id = rm.role_id  " + 
			"		where ur.user_id = #{userId} ")
	public List<String> queryAllMenuId(@Param("userId") String userId);

	@Select("SELECT distinct parent_id FROM   sys_menu WHERE menu_id IN " +
			" (select menu_id from  sys_role_menu rm LEFT JOIN  sys_user_role ur on " +
			" rm.role_id = ur.role_id where ur.user_id = #{userid} )"  
			 )
	List<String> queryParentByUser(String userid);
	
	/**
	 * 查询用户的所有权限
	 * @param userId  用户ID
	 */
	@Select("select distinct m.perms from   sys_user_role ur " + 
			"			LEFT JOIN  sys_role_menu rm on ur.role_id = rm.role_id " + 
			"			LEFT JOIN  sys_menu m on rm.menu_id = m.menu_id " + 
			"		where ur.user_id = #{userId}")
	public Set<String> queryAllPerms(@Param("userId") String userId);

}
