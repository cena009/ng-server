package com.ng.sys.dao;

import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ng.sys.entity.SysUserDepartmentEntity;

/**
 * 用户部门中间表
 * 
 * @author lyf
 * @email jjxliu306@163.com
 * @date 2019-12-20 14:18:10
 */
@Mapper
public interface SysUserDepartmentDao extends BaseMapper<SysUserDepartmentEntity> {
	
	/*推荐在此直接添加注解写sql,可读性比较好*/
	
}
