package com.ng.sys.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ng.sys.entity.SysRoleEntity;

/**
 * 角色管理
 * 
 * @author lyf
 * @date 2016年9月18日 上午9:33:33
 */
@Mapper
public interface SysRoleDao extends BaseMapper<SysRoleEntity> {
	
	/**
	 * 查询用户创建的角色ID列表
	 */
	@Select("select role_id from   sys_role where create_user_id = #{createUserId} ")
	List<Long> queryRoleIdList(@Param("createUserId") String createUserId);
}
