package com.ng.sys.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ng.sys.entity.SysDictEntity;

/**
 * 数据字典
 * 
 * @author lyf
 * @email jjxliu306@163.com
 * @date 2019-03-20 10:54:34
 */
@Mapper
public interface SysDictDao extends BaseMapper<SysDictEntity> {
 
	@Select(" <script>"+ 
			"select distinct type , type_name "
			+ " from sys_dict t1" 
			+ "<where> " 
			+ " <if test=\"type!=null and type!=''\">"
			+ "  t1.type like concat(concat('%', #{type}),'%') or t1.type_name like concat(concat('%', #{type}),'%') "
			+ " </if >"
			+ " and delete_flag = 0 "
			+ "</where>"
			+ "</script>")
	public IPage<SysDictEntity> queryPageList(Page<SysDictEntity> page , @Param("type") String type  );

}
