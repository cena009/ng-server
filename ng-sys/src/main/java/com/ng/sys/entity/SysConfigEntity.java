

package com.ng.sys.entity;

import javax.validation.constraints.NotBlank;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
 


/**
 * 系统配置信息
 * 
 * @author lyf
 * @email liuyf@gs-softwares.com
 * @date 2016年12月4日 下午6:43:36
 */
@TableName(value="sys_config" )
@Data
@ApiModel("系统配置信息")
public class SysConfigEntity {


	/**
	 * 主键
	 */
//	@TableId(type= IdType.UUID)
//	@TableField("id")
//	@ApiModelProperty("主键")
//	private String id;
	/**
	 * key
	 */
	@NotBlank(message="参数名不能为空")
	@TableField("param_key")
	@TableId(type=IdType.INPUT)
	@ApiModelProperty("key")
	private String paramKey;
	/**
	 * 参数类型 1-字符串,2-数组,3-表格
	 */
//	@NotBlank(message="参数值不能为空")
	@TableField("param_type")
	@ApiModelProperty("参数类型 1-字符串,2-数组,3-表格")
	private Integer paramType= 1;;
	/**
	 * value
	 */
	@TableField("param_value")
	@ApiModelProperty("value")
	private String paramValue;
	/**
	 * 备注
	 */
	@TableField("remark")
	@ApiModelProperty("备注")
	private String remark;
	/**
	 * 状态   0：隐藏   1：显示
	 */
	@TableField("status")
	@ApiModelProperty("状态   0：隐藏   1：显示")
	private Integer status;


}
