package com.ng.sys.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 数据字典
 * 
 * @author lyf
 * @email jjxliu306@163.com
 * @date 2019-03-20 10:54:34
 */
@Data
@TableName(value="sys_dict" )
public class SysDictEntity  implements Serializable {
	 
	/**
	 * 
	 */
	private static final long serialVersionUID = -4754957935328935572L;
	/**
	 * id
	 */
	@TableId(type = IdType.UUID)
	@TableField("id")
	@ApiModelProperty("ID")
	private String id;
	/**
	 * 值
	 */
	@TableField("value")
	@ApiModelProperty("值")
	private String value;
	/**
	 * 标签
	 */
	@ApiModelProperty("标签")
	@TableField("name")
	private String name;
	/**
	 * 字典分类
	 */
	@TableField("type")
	@ApiModelProperty("字典分类")
	private String type;
	
	
	/**
	 * 字典分类名称
	 */
	@TableField("type_name")
	@ApiModelProperty("字典分类名称")
	private String typeName;
	
	/**
	 * 描述
	 */
	@ApiModelProperty("描述")
	@TableField("description")
	private String description;
	/**
	 * 序号
	 */
	@TableField("seq")
	@ApiModelProperty("序号")
	private Integer seq;
	/**
	 * 创建时间
	 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@TableField("create_time")
	@ApiModelProperty(hidden=true)
	private Date createTime;
	/**
	 * 更新时间
	 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@TableField("update_time")
	@ApiModelProperty(hidden=true)
	private Date updateTime;
	/**
	 * 删除状态（0：可用   1：不可用）
	 */
	@TableField("delete_flag")
	@ApiModelProperty("删除状态（0：可用   1：不可用）")
	private Integer deleteFlag;
	/**
	 * 创建Id
	 */
	@TableField("create_id")
	@ApiModelProperty(hidden=true)
	private String createId;
	/**
	 * 更新Id
	 */
	@TableField("update_id")
	@ApiModelProperty(hidden=true)
	private String updateId;


	@TableField(exist = false)
	@ApiModelProperty(hidden=true)
	private int count;


	 
}
