

package com.ng.sys.entity;


import java.io.Serializable;
import java.util.List;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 菜单管理
 * 
 * @author lyf
 * @email liuyf@gs-softwares.com
 * @date 2016年9月18日 上午9:26:39
 */
@TableName(value="sys_menu" )
@Data
@ApiModel("菜单管理")
public class SysMenuEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 菜单ID
	 */
	@TableId(type= IdType.UUID)
	@TableField("menu_id")
	@ApiModelProperty("菜单ID")
	private String menuId;
	/**
	 * 组件的地址
	 */
	@TableField("component_url")
	@ApiModelProperty("组件的地址")
	private String componentUrl;
	/**
	 * 是否启用 1-启用
	 */
	@TableField("enabled")
	@ApiModelProperty("是否启用 1-启用")
	private Integer enabled;
	/**
	 * 菜单图标
	 */
	@TableField("icon")
	@ApiModelProperty("菜单图标")
	private String icon;

	/**
	 * 菜单名称
	 */
	@TableField("name")
	@ApiModelProperty("菜单名称")
	private String name;
	/**
	 * 排序
	 */
	@TableField("order_num")
	@ApiModelProperty("排序")
	private Integer orderNum;
	/**
	 * 父菜单ID，一级菜单为0
	 */
	@TableField("parent_id")
	@ApiModelProperty("父菜单ID，一级菜单为0")
	private String parentId;
	/**
	 * 授权(多个用逗号分隔，如：user:list,user:create)
	 */
	@TableField("perms")
	@ApiModelProperty("授权(多个用逗号分隔，如：user:list,user:create)")
	private String perms;
	/**
	 * 路由名称
	 */
	@TableField("route_name")
	@ApiModelProperty("路由名称")
	private String routeName;
	/**
	 *  是否只创建路由不生成菜单
	 */
	@TableField("route_only")
	@ApiModelProperty("是否只创建路由不生成菜单")
	private Integer routeOnly;
	/**
	 * 类型   0：目录   1：菜单   2：按钮
	 */
	@TableField("type")
	@ApiModelProperty("类型   0：目录   1：菜单   2：按钮")
	private Integer type;
	/**
	 * 菜单URL
	 */
	@TableField("url")
	@ApiModelProperty("菜单URL")
	private String url;

	/**
	 * 父菜单名称
	 */
	@TableField(exist=false)
	private String parentName;
	/**
	 * ztree属性
	 */
	@TableField(exist=false)
	private Boolean open;

	@TableField(exist=false)
	private List<?> list;

}
