package com.ng.sys.entity;
 
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 用户部门中间表
 * 
 * @author lyf
 * @email jjxliu306@163.com
 * @date 2019-12-20 14:18:10
 */
@TableName(value="sys_user_department")
@Data
@ApiModel("用户部门中间表")
public class SysUserDepartmentEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId(type= IdType.UUID)
	@TableField("id")
	@ApiModelProperty("主键")
	private String id;
	/**
	 * 用户ID
	 */
	@TableField("user_id")
	@ApiModelProperty("用户ID")
	private String userId;
	/**
	 * 部门ID
	 */
	@TableField("department_id")
	@ApiModelProperty("部门ID")
	private String departmentId;
 

}
