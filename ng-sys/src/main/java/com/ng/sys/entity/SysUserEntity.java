package com.ng.sys.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Past;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode; 

/**
 * 系统用户
 * 
 * @author lyf
 * @date 2017年9月18日 上午9:28:55
 */
@TableName(value="sys_user" )
@Data
@EqualsAndHashCode(callSuper=false)
public class SysUserEntity   implements Serializable {
 
	/**
	 * 
	 */
	private static final long serialVersionUID = 1365777226993409274L;

 
	    
	
	/**
	 * id
	 */
	@TableId(type= IdType.UUID)
	@TableField("user_id")
	@ApiModelProperty("id")
	private String userId;
	/**
	 * 盐
	 */
	//	@JsonIgnore
	@TableField("salt")
	@Max(value = 20,message = "盐最大长度为20")
	@ApiModelProperty("盐")
	private String salt;
	/**
	 * 性别  1- 男 2- 女
	 */
	@TableField("sex")
	@ApiModelProperty("性别")
	@Max(value = 2,message = "性别最大长度为2")
	private Integer sex;
	/**
	 * 状态  0：禁用   1：正常
	 */
	@TableField("status")
	@ApiModelProperty("状态")
	@Max(value = 16,message = "状态 最大长度为16")
	private Integer status;

	/**
	 * 账号名称
	 */
	@TableField("user_no")
	@Max(value = 50,message = "账号名称最大长度为50")
	@ApiModelProperty("账号名称")
	private String userNo;
 
	/**
	 * 姓名
	 */
	@Max(value = 50,message = "账号类型最大长度为50")
	@TableField("username")
	@ApiModelProperty("姓名")
	private String username;
	/**
	 * 出生日期
	 */
	@Past
	@JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
	@TableField("birthday")
	@ApiModelProperty("出生日期")
	private Date birthday;
	   
	/**
	 * 电子邮箱
	 */
	@Email
	@Max(value = 100,message = "电子邮箱最大长度为100")
	@TableField("email")
	@ApiModelProperty("电子邮箱")
	private String email;
	/**
	 * 最后登录时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
	@TableField("last_login_time")
	@ApiModelProperty("最后登录时间")
	private Date lastLoginTime;
	/**
	 * 头像
	 */
	@TableField("logo")
	@ApiModelProperty("头像")
	private String logo;
	/**
	 * 手机号
	 */
	@Max(value = 11,message = "手机号最大长度为11")
	@TableField("mobile")
	//@NotBlank(message="手机不能为空", groups = {AddGroup.class, UpdateGroup.class})
	@ApiModelProperty("手机号")
	private String mobile;
	/**
	 * 密码
	 */
	@Max(value = 100,message = "密码最大长度为100")
	@TableField("password")
	@ApiModelProperty("密码")
	private String password;
	  
	

	/**
	 * 创建时间
	 */
	@Past
	@JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
	@TableField("create_time")
	@ApiModelProperty("创建时间")
	private Date createTime;

	/**
	 * 创建人
	 */
	@TableField("create_id")
	@ApiModelProperty("创建人")
	private String createId;
	/**
	 * 修改人
	 */
	@TableField("update_id")
	@ApiModelProperty("修改人")
	private String updateId;
	/**
	 * 修改时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
	@TableField("update_time")
	@ApiModelProperty("修改时间")
	private Date updateTime;
	
	/**
	 * 部门ID
	 */
	@TableField("depart_id")
	@ApiModelProperty("部门ID时间")
	private String departId;

	/**
	 * 备注
	 */
	@TableField(exist=false)
	private String context ;

//	@TableField(exist=false)
//	private String roleId;
//
	/**
	 * 角色ID列表
	 */
	@TableField(exist=false)
	private List<String> roleIdList;

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	 
}
