package com.ng.sys.entity;


import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 角色
 * 
 * @author lyf
 * @email liuyf@gs-softwares.com
 * @date 2016年9月18日 上午9:27:38
 */
@TableName(value="sys_role" )
@Data
public class SysRoleEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * 主键
	 */
	@TableId(type= IdType.UUID)
	@TableField("role_id")
	@ApiModelProperty("主键")
	private String roleId;
	/**
	 * 角色名称
	 */
	@NotBlank(message="角色名称不能为空")
	@TableField("role_name")
	@ApiModelProperty("角色名称")
	private String roleName;
	/**
	 * 默认
	 */
	@TableField("user_default")
	@ApiModelProperty("默认")
	private Boolean userDefault;

	/**
	 * 创建时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
	@TableField("create_time")
	@ApiModelProperty("创建时间")
	private Date createTime;
	/**
	 * 创建人id
	 */
	@TableField("create_user_id")
	@ApiModelProperty("创建人id")
	private String createUserId;
	/**
	 * 备注
	 */
	@TableField("remark")
	@ApiModelProperty("备注")
	private String remark;

	@TableField(exist=false)
	private List<String> menuIdList;

}
