package com.ng.sys.entity;


import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 角色与菜单对应关系
 * 
 * @author lyf
 * @email liuyf@gs-softwares.com
 * @date 2016年9月18日 上午9:28:13
 */
@TableName(value="sys_role_menu" )
@Data
@ApiModel("角色与菜单对应关系")
public class SysRoleMenuEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@TableId(type= IdType.UUID)
	@TableField("id")
	@ApiModelProperty("主键")
	private String id;

	/**
	 * 角色ID
	 */
	@TableField("role_id")
	@ApiModelProperty("角色id")
	private String roleId;

	/**
	 * 菜单ID
	 */
	@TableField("menu_id")
	@ApiModelProperty("菜单id")
	private String menuId;
	
	/**
	 * 序号
	 */
	@TableField("seq")
	@ApiModelProperty("序号")
	private Integer seq ;

}
