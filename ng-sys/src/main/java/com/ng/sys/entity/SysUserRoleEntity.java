package com.ng.sys.entity;


import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 用户与角色对应关系
 * 
 * @author lyf
 * @date 2017年9月18日 上午9:28:39
 */
@TableName(value="sys_user_role" )
@Data
@ApiModel("用户与角色对应关系")
public class SysUserRoleEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId(type= IdType.UUID)
	@TableField("id")
	@ApiModelProperty("主键")
	private String id;
	/**
	 * 角色id
	 */
	@TableField("role_id")
	@ApiModelProperty("角色id")
	private String roleId;
	/**
	 * 用户id
	 */
	@TableField("user_id")
	@ApiModelProperty("用户id")
	private String userId;

}
