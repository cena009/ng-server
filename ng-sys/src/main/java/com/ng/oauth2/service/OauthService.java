package com.ng.oauth2.service;

import java.util.Set;

import com.ng.sys.entity.SysUserEntity;

public interface OauthService {
	
	/**
	 * 根据token查询用户  
	 * @param token 用户当前授权token
	 * @return
	 */
	public SysUserEntity queryByToken(String token);
	
	/**
	 * 基于app token查询用户
	 * @param appToken
	 * @return
	 */
	//public SysUser queryByAppToken(String appToken);
	
	/**
	 * 查询用户的权限列表
	 * @param userId
	 * @return
	 */
	public Set<String> queryPermsBytoken(String token);
	
 
}
