package com.ng.common.security;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ng.common.utils.HttpContextUtils;
import com.ng.common.utils.SpringContextUtils;
import com.ng.oauth2.service.OauthService;
import com.ng.sys.entity.SysUserEntity;

/**
 * 认证公共类,获取当前用户等信息
 * @author lyf
 *
 */
@Component
public class SecurityUtils {

	@Autowired
	private OauthService service ;
 
	public SysUserEntity getCurrentUser() {
		// TODO Auto-generated method stub
		
		
		  //获取请求token
    	HttpServletRequest hsr = HttpContextUtils.getHttpServletRequest();
    	
		
		String token = HttpContextUtils.getRequestToken(hsr , "token") ;
		
		if(StringUtils.isBlank(token)) {
			return null ;
		}
		
		SysUserEntity ret = service.queryByToken(token);
		 
		return ret;
	}
	
	
	/**
	 * static 方法请求 当前用户 
	 * @return
	 */
	public static SysUserEntity getUser() {
		
		SecurityUtils securityUtils = SpringContextUtils.getBean(SecurityUtils.class);
		if(securityUtils != null) {
			return securityUtils.getCurrentUser();
		}
		
		return null ;
		
	}
	
	
}
