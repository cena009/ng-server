
-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `param_key` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT 'key',
  `param_value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL COMMENT 'value',
  `status` smallint(6) NULL DEFAULT NULL COMMENT '状态   0：隐藏   1：显示',
  `remark` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL COMMENT '备注',
  `param_type` int(11) NULL DEFAULT NULL COMMENT '参数类型 1-字符串,2-数组,3-表格',
  PRIMARY KEY (`param_key`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES ('login_expire_time', '3600', 1, '用户登录超时时间，单位秒', 1);
INSERT INTO `sys_config` VALUES ('repeat_login', '1', 1, '一个用户是否可以重复登录，1-允许，其他不允许', 1);

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT 'ID',
  `value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '值',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '标签',
  `type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '字典分类',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '类型描述',
  `seq` int(11) NULL DEFAULT NULL COMMENT '序号',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `delete_flag` int(11) NULL DEFAULT NULL COMMENT '删除状态（0：可用   1：不可用）',
  `create_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建Id',
  `update_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '更新Id',
  `type_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '类型名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '数据字典' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
 
INSERT INTO  `sys_dict`(`id`, `value`, `name`, `type`, `description`, `seq`, `create_time`, `update_time`, `delete_flag`, `create_id`, `update_id`, `type_name`) VALUES ('5bb4783f1db3b3df59fae3e5d8982728', '1', '男', 'sex', '', 1, '2022-02-23 10:06:48', '2022-02-23 10:06:48', 0, 'superAdmin', 'superAdmin', '性别');
INSERT INTO  `sys_dict`(`id`, `value`, `name`, `type`, `description`, `seq`, `create_time`, `update_time`, `delete_flag`, `create_id`, `update_id`, `type_name`) VALUES ('7d39661aaf65690912f0a76ce3e5fb3e', '2', '女', 'sex', '', 2, '2022-02-23 10:06:56', '2022-02-23 10:06:56', 0, 'superAdmin', 'superAdmin', '性别');

-- ----------------------------
-- Table structure for sys_file
-- ----------------------------
DROP TABLE IF EXISTS `sys_file`;
CREATE TABLE `sys_file`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '主键id',
  `old_file_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '原始文件名称',
  `file_suffix` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '文件类型，即后缀',
  `file_url` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '文件路径',
  `file_size` int(11) NULL DEFAULT NULL COMMENT '文件大小（byte），预留',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `new_file_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '保存时文件名称',
  `store_type` smallint(6) NOT NULL,
  `create_id` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '文件信息表（上传）' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_file
-- ----------------------------

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log`  (
  `id` bigint(20) auto_increment NOT NULL,
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '用户',
  `operation` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '用户操作',
  `method` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '请求方法',
  `params` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL COMMENT '用户操作',
  `time` bigint(20) NOT NULL COMMENT '执行时长(毫秒)',
  `ip` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'IP地址',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT 'params',
  `system` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '系统模块',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '系统日志' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_log
-- ----------------------------
 
-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '菜单ID',
  `parent_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '父菜单ID，一级菜单为null',
  `name` varchar(254) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '菜单名称',
  `url` varchar(254) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '菜单URL',
  `perms` varchar(254) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '授权(多个用逗号分隔，如：user:list,user:create)',
  `type` int(11) NULL DEFAULT NULL COMMENT '类型   0：目录   1：菜单   2：按钮',
  `icon` varchar(254) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '菜单图标',
  `order_num` int(11) NULL DEFAULT NULL COMMENT '排序',
  `enabled` int(11) NULL DEFAULT NULL COMMENT '是否启用 1-启用',
  `component_url` varchar(254) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `route_name` varchar(254) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `route_only` smallint(6) NULL DEFAULT NULL,
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '菜单管理' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('00954845842afea1b8537744185b6a42', '827c2c75173f7887271d23ce3cc6e105', '修改', '', 'form:template:update', 3, '', 0, 1, '', '', 1);
INSERT INTO `sys_menu` VALUES ('094a9ddd1d9fbbc6cd8388d408e62929', '9c3c36cfabddca5fb932ed27065b4063', '数据登记', '/form/data?code=F00001', 'form:data:list', 1, 'icon-log', 1, 1, 'form/data/index', 'form_data', 1);
INSERT INTO `sys_menu` VALUES ('0f70ee74fe3eb2f5c7f5bc25a3d97c92', '094a9ddd1d9fbbc6cd8388d408e62929', '删除', '', 'form:data:delete', 3, '', 3, 1, '', '', 1);
INSERT INTO `sys_menu` VALUES ('1', '0', '系统管理', NULL, NULL, 0, 'system', 0, 1, 'main/black-main', 'sys', 1);
INSERT INTO `sys_menu` VALUES ('10f9cd8724f945c8f4361215e551b616', '094a9ddd1d9fbbc6cd8388d408e62929', '更新', '', 'form:data:update', 3, '', 1, 1, '', '', 1);
INSERT INTO `sys_menu` VALUES ('2', '1', '用户管理', '/sys/user', 'sys:user', 1, 'admin', 1, 1, 'sys/user/list', 'sys_user', 1);
INSERT INTO `sys_menu` VALUES ('23', '2', '更新', NULL, 'sys:user:update,sys:user:delete,sys:user:resetPass,sys:user:save', 3, NULL, 2, 1, NULL, 'sys_user_update', 1);
INSERT INTO `sys_menu` VALUES ('29', '1', '系统日志', '/sys/log', 'sys:log:list', 1, 'log', 7, 1, 'sys/log', 'sys_log', 0);
INSERT INTO `sys_menu` VALUES ('3', '1', '角色管理', '/sys/role', 'sys:role', 1, 'role', 2, 1, 'sys/role/list', 'sys_role', 1);
INSERT INTO `sys_menu` VALUES ('33', '3', '更新', NULL, 'sys:role:update,sys:role:delete,sys:role:save', 3, NULL, 2, 1, NULL, 'sys_role_update', 1);
INSERT INTO `sys_menu` VALUES ('4', '1', '菜单管理', '/sys/menu', 'sys:menu:list', 1, 'menu', 3, 1, 'sys/menu/list', 'sys_menu', 0);
INSERT INTO `sys_menu` VALUES ('43', '4', '更新', NULL, 'sys:menu:update,sys:menu:save,sys:menu:delete', 3, NULL, 2, 1, NULL, 'sys_menu_update', 1);
INSERT INTO `sys_menu` VALUES ('517517fc505487a279b3ee8e9b34e044', '9ff75d507236cd5a3ef6c516aa378884', '权限列表', 'exter/perms', 'exter:perms:list,exter:perms:info,exter:perms:update,exter:perms:delete,exter:perms:save', 1, 'zhedie', 1, 1, 'exter/perms/list', 'exter_perms', 1);
INSERT INTO `sys_menu` VALUES ('6', '1', '数据字典', '/sys/dict', 'sys:sysdict:list', 1, 'pinglun', 0, 1, 'sys/dict/list', 'sys_dict', 0);
INSERT INTO `sys_menu` VALUES ('63', '6', '更新', NULL, 'sys:sysdict:update,sys:sysdict:delete,sys:sysdict:save', 3, NULL, 3, 1, NULL, 'sys_dict_update', 1);
INSERT INTO `sys_menu` VALUES ('689fac31128df410811d43fc967b51b8', '9ff75d507236cd5a3ef6c516aa378884', '三方应用', '/exter/app', 'exter:app:list,exter:app:info,exter:app:update,exter:app:delete,exter:app:save', 1, 'log', 2, 1, 'exter/app/list', 'exter_app', 1);
INSERT INTO `sys_menu` VALUES ('7', '1', '参数管理', '/sys/config', 'sys:config:list,sys:config:info', 1, 'config', 6, 1, 'sys/config/list', 'sys_config', 0);
INSERT INTO `sys_menu` VALUES ('73', '7', '更新', NULL, 'sys:config:update,sys:config:save,sys:config:delete', 3, NULL, 2, 1, NULL, 'sys_config_update', 1);
INSERT INTO `sys_menu` VALUES ('75b18d7136131a1bab75438bc1dab2bf', '827c2c75173f7887271d23ce3cc6e105', '删除', '', 'form:template:delete', 3, '', 1, 1, '', '', 1);
INSERT INTO `sys_menu` VALUES ('827c2c75173f7887271d23ce3cc6e105', 'd01784ec66976990e76ee52741606038', '表单模板', '/form/template', 'form:template:list', 1, 'icon-copy2', 0, 1, 'form/template/index', 'form_template', 1);
INSERT INTO `sys_menu` VALUES ('880c79d0d137eb431c7d6f1d83489db9', '9ff75d507236cd5a3ef6c516aa378884', '调用记录', '/exter/log', 'exter:log', 1, 'pinglun', 3, 1, 'exter/log/index', 'exter_log', 1);
INSERT INTO `sys_menu` VALUES ('905a2599db4ea9bf9a0d268e4d20a403', '094a9ddd1d9fbbc6cd8388d408e62929', '处理', '', 'form:data:solve', 3, '', 2, 1, '', '', 1);
INSERT INTO `sys_menu` VALUES ('98b8dd41b16e4feabd7cf639090dbd7e', '1', '定时任务', 'job/schedule', 'job:schedule', 1, 'job', 21, 1, 'job/schedule', 'job_scedule', 1);
INSERT INTO `sys_menu` VALUES ('9c3c36cfabddca5fb932ed27065b4063', '0', '信息登记', '', '', 0, 'icon-copy2', 3, 1, '', '', 1);
INSERT INTO `sys_menu` VALUES ('9ff75d507236cd5a3ef6c516aa378884', '0', '三方授权', NULL, NULL, 0, 'role', 1, 1, NULL, NULL, 1);
INSERT INTO `sys_menu` VALUES ('d01784ec66976990e76ee52741606038', '0', '动态表单', '', '', 0, 'icon-feeds', 2, 1, '', '', 1);
INSERT INTO `sys_menu` VALUES ('dfe66974746b39e3f763946495c5e58b', '094a9ddd1d9fbbc6cd8388d408e62929', '新增', '', 'form:data:save', 3, '', 0, 1, '', '', 1);

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '主键',
  `role_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '角色名称',
  `remark` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  `create_user_id` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建人id',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `user_default` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '默认',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('642b25226705c4eb754be80b9790a735', '管理员', '管理员角色', 'superAdmin', '2019-12-23 13:59:36', 'f');

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '主键',
  `role_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '角色id',
  `menu_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '菜单id',
  `seq` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES ('05719de64f4c469fb62b8647bf62a88f', '642b25226705c4eb754be80b9790a735', '43', 4);
INSERT INTO `sys_role_menu` VALUES ('1aaee441070f60a713bb0d631b3f4f52', '642b25226705c4eb754be80b9790a735', '33', 13);
INSERT INTO `sys_role_menu` VALUES ('1b29f26c95b459c14b980c3622d8bec4', '642b25226705c4eb754be80b9790a735', '827c2c75173f7887271d23ce3cc6e105', 14);
INSERT INTO `sys_role_menu` VALUES ('2685f3c5c56190228cf7cdc9aae9ebc4', '642b25226705c4eb754be80b9790a735', '23', 11);
INSERT INTO `sys_role_menu` VALUES ('2aa04c8e6fbd5645e39f60e85ffbca3f', '642b25226705c4eb754be80b9790a735', '880c79d0d137eb431c7d6f1d83489db9', 20);
INSERT INTO `sys_role_menu` VALUES ('2b5d60083fc96805dbc4944efa671cde', '642b25226705c4eb754be80b9790a735', '-666666', 21);
INSERT INTO `sys_role_menu` VALUES ('33da1496c861304a91061e14850416b5', '642b25226705c4eb754be80b9790a735', '2', 10);
INSERT INTO `sys_role_menu` VALUES ('38eda0aea03e4aa1f26de00a246f799f', '642b25226705c4eb754be80b9790a735', '00954845842afea1b8537744185b6a42', 15);
INSERT INTO `sys_role_menu` VALUES ('394fbb2c6429cee7a4b816c2bc459d69', '642b25226705c4eb754be80b9790a735', '75b18d7136131a1bab75438bc1dab2bf', 16);
INSERT INTO `sys_role_menu` VALUES ('44255c65c4cdb7aec5cfb91e6a194867', '642b25226705c4eb754be80b9790a735', '6', 5);
INSERT INTO `sys_role_menu` VALUES ('5cea8a5f701d6621acbc1f5537a44936', '642b25226705c4eb754be80b9790a735', '689fac31128df410811d43fc967b51b8', 18);
INSERT INTO `sys_role_menu` VALUES ('60fa93280f78b5500da78ec6b8827e81', '642b25226705c4eb754be80b9790a735', '3', 12);
INSERT INTO `sys_role_menu` VALUES ('6826c5b7cdc4dafccf1b046a9fcf5919', '642b25226705c4eb754be80b9790a735', '73', 9);
INSERT INTO `sys_role_menu` VALUES ('69a0b960cb690892ed450d228fbbafaa', '642b25226705c4eb754be80b9790a735', '4', 3);
INSERT INTO `sys_role_menu` VALUES ('92a783c8a9070e604d4d18e413f88a0a', '642b25226705c4eb754be80b9790a735', '517517fc505487a279b3ee8e9b34e044', 19);
INSERT INTO `sys_role_menu` VALUES ('97c8e18dd0dc700ace4d1cc8ba468523', '642b25226705c4eb754be80b9790a735', '1', 1);
INSERT INTO `sys_role_menu` VALUES ('b53a2be3154d3373149b398ade1e8969', '642b25226705c4eb754be80b9790a735', '9ff75d507236cd5a3ef6c516aa378884', 17);
INSERT INTO `sys_role_menu` VALUES ('c3351fe2d284fa707bbcbf7219f9a7b1', '642b25226705c4eb754be80b9790a735', '98b8dd41b16e4feabd7cf639090dbd7e', 2);
INSERT INTO `sys_role_menu` VALUES ('ca63edf846c5d184f0488d4b28ed937c', '642b25226705c4eb754be80b9790a735', '29', 7);
INSERT INTO `sys_role_menu` VALUES ('cfc182b974393e2b4df6d295ed8feabd', '642b25226705c4eb754be80b9790a735', '63', 6);
INSERT INTO `sys_role_menu` VALUES ('dcac8c29a09798a871c57582bda9d703', '642b25226705c4eb754be80b9790a735', '7', 8);


-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT 'id',
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '姓名',
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '密码',
  `salt` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '盐',
  `email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '电子邮箱',
  `mobile` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '手机号',
  `status` smallint(6) NULL DEFAULT NULL COMMENT '状态',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `user_no` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '账号名称',
  `sex` smallint(6) NULL DEFAULT NULL COMMENT '性别',
  `logo` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '头像',
  `last_login_time` datetime(0) NULL DEFAULT NULL COMMENT '最后登录时间',
  `depart_id` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '部门id',
  `birthday` date NULL DEFAULT NULL COMMENT '出生日期',
  `create_id` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建人',
  `update_id` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('6c076dd48511c66f1a692b50d3d41bbd', 'admin', '6be95b1449cd098307e27c354c77ec93', 'NIyufT7h9K9muR3sab6R', NULL, '11', 1, '2021-08-08 17:07:32', 'admin', 1, NULL, '2021-08-13 14:48:30', NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '主键',
  `user_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '用户id',
  `role_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '角色id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('78f8a00eed34cc7712088ed3c30cec01', '6c076dd48511c66f1a692b50d3d41bbd', '642b25226705c4eb754be80b9790a735');
