INSERT INTO sys_config(param_key, param_value, status, remark, param_type) VALUES ('login_expire_time', '3600', 1, '用户登录超时时间，单位秒', 1);
INSERT INTO sys_config(param_key, param_value, status, remark, param_type) VALUES ('repeat_login', '1', 1, '一个用户是否可以重复登录，1-允许，其他不允许', 1);
