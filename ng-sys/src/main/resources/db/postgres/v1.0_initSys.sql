CREATE TABLE sys_config
(
    param_key varchar(50) NOT NULL,
    param_value varchar(2000),
    status smallint DEFAULT 1,
    remark varchar(500) ,
    param_type integer DEFAULT 1,
    CONSTRAINT sys_config_pkey PRIMARY KEY (param_key)
);

COMMENT ON COLUMN sys_config.param_key IS 'key';
COMMENT ON COLUMN sys_config.param_value IS 'value';
COMMENT ON COLUMN sys_config.status  IS '状态   0：隐藏   1：显示';
COMMENT ON COLUMN sys_config.remark IS '备注';
COMMENT ON COLUMN sys_config.param_type  IS '参数类型 1-字符串,2-数组,3-表格';


-- Table: sys_dict

-- DROP TABLE sys_dict;

CREATE TABLE sys_dict
(
    id varchar(32) NOT NULL,
    value varchar(255),
    name varchar(50),
    type varchar(50),
    description varchar(255),
    seq integer,
    create_time timestamp(6) without time zone DEFAULT now(),
    update_time timestamp(6) without time zone DEFAULT now(),
    delete_flag integer DEFAULT 0,
    create_id varchar(100),
    update_id varchar(100),
    type_name varchar(50),
    CONSTRAINT sys_dict_pkey PRIMARY KEY (id)
) ;
 

COMMENT ON TABLE sys_dict IS '数据字典';
COMMENT ON COLUMN sys_dict.id  IS 'ID';
COMMENT ON COLUMN sys_dict.value IS '值';
COMMENT ON COLUMN sys_dict.name IS '标签';
COMMENT ON COLUMN sys_dict.type IS '字典分类';
COMMENT ON COLUMN sys_dict.description  IS '类型描述';
COMMENT ON COLUMN sys_dict.seq  IS '序号';
COMMENT ON COLUMN sys_dict.create_time  IS '创建时间';
COMMENT ON COLUMN sys_dict.update_time IS '更新时间';
COMMENT ON COLUMN sys_dict.delete_flag IS '删除状态（0：可用   1：不可用）';
COMMENT ON COLUMN sys_dict.create_id IS '创建Id';
COMMENT ON COLUMN sys_dict.update_id IS '更新Id';
COMMENT ON COLUMN sys_dict.type_name IS '类型名称';
 


-- Table: sys_file

-- DROP TABLE sys_file;

CREATE TABLE sys_file
(
    id varchar(32)  NOT NULL,
    old_file_name varchar(255)  NOT NULL,
    file_suffix varchar(50) ,
    file_url varchar(500)  NOT NULL,
    file_size integer,
    create_time timestamp(0) without time zone NOT NULL,
    new_file_name varchar(255)  NOT NULL,
    store_type smallint NOT NULL,
    create_id varchar(40) ,
    CONSTRAINT file_info_pkey PRIMARY KEY (id)
);

COMMENT ON TABLE sys_file IS '文件信息表（上传）';
COMMENT ON COLUMN sys_file.id IS '主键id';
COMMENT ON COLUMN sys_file.old_file_name IS '原始文件名称';
COMMENT ON COLUMN sys_file.file_suffix IS '文件类型，即后缀';
COMMENT ON COLUMN sys_file.file_url IS '文件路径';
COMMENT ON COLUMN sys_file.file_size  IS '文件大小（byte），预留';
COMMENT ON COLUMN sys_file.create_time IS '创建时间';
COMMENT ON COLUMN sys_file.new_file_name IS '保存时文件名称';


-- Table: sys_log

-- DROP TABLE sys_log;

CREATE TABLE sys_log
(
    id bigserial NOT NULL ,
    username varchar(50) ,
    operation varchar(50) ,
    method varchar(200) ,
    params varchar(5000) ,
    "time" bigint NOT NULL DEFAULT 0,
    ip varchar(64) ,
    create_date timestamp(6) without time zone DEFAULT now(),
    system varchar(20) ,
    CONSTRAINT sys_log_pkey PRIMARY KEY (id)
);

COMMENT ON TABLE sys_log IS '系统日志';
COMMENT ON COLUMN sys_log.username IS '用户';
COMMENT ON COLUMN sys_log.operation IS '用户操作';
COMMENT ON COLUMN sys_log.method IS '请求方法';
COMMENT ON COLUMN sys_log.params IS '用户操作';
COMMENT ON COLUMN sys_log."time" IS '执行时长(毫秒)';
COMMENT ON COLUMN sys_log.ip IS 'IP地址';
COMMENT ON COLUMN sys_log.create_date IS 'params';
COMMENT ON COLUMN sys_log.system IS '系统模块';



-- Table: sys_menu

-- DROP TABLE sys_menu;

CREATE TABLE sys_menu
(
    menu_id varchar(32)  NOT NULL,
    parent_id varchar(32) ,
    name varchar(254) ,
    url varchar(254) ,
    perms varchar(254) ,
    type integer,
    icon varchar(254) ,
    order_num integer,
    enabled integer DEFAULT 1,
    component_url varchar(254) ,
    route_name varchar(254) ,
    route_only smallint DEFAULT 1,
    CONSTRAINT sys_menu_pkey PRIMARY KEY (menu_id)
) ;

COMMENT ON TABLE sys_menu IS '菜单管理';
COMMENT ON COLUMN sys_menu.menu_id IS '菜单ID';
COMMENT ON COLUMN sys_menu.parent_id  IS '父菜单ID，一级菜单为null';
COMMENT ON COLUMN sys_menu.name  IS '菜单名称';
COMMENT ON COLUMN sys_menu.url IS '菜单URL';
COMMENT ON COLUMN sys_menu.perms IS '授权(多个用逗号分隔，如：user:list,user:create)';
COMMENT ON COLUMN sys_menu.type  IS '类型   0：目录   1：菜单   2：按钮';
COMMENT ON COLUMN sys_menu.icon  IS '菜单图标';
COMMENT ON COLUMN sys_menu.order_num IS '排序';
COMMENT ON COLUMN sys_menu.enabled IS '是否启用 1-启用';



INSERT INTO sys_menu(menu_id, parent_id, name, url, perms, type, icon, order_num, enabled, component_url, route_name, route_only) VALUES ('98b8dd41b16e4feabd7cf639090dbd7e', '1', '定时任务', 'job/schedule', 'job:schedule', 1, 'job', 21, 1, 'job/schedule', 'job_scedule', 1);
INSERT INTO sys_menu(menu_id, parent_id, name, url, perms, type, icon, order_num, enabled, component_url, route_name, route_only) VALUES ('4', '1', '菜单管理', '/sys/menu', 'sys:menu:list', 1, 'menu', 3, 1, 'sys/menu/list', 'sys_menu', 0);
INSERT INTO sys_menu(menu_id, parent_id, name, url, perms, type, icon, order_num, enabled, component_url, route_name, route_only) VALUES ('73', '7', '更新', NULL, 'sys:config:update,sys:config:save,sys:config:delete', 3, NULL, 2, 1, NULL, 'sys_config_update', 1);
INSERT INTO sys_menu(menu_id, parent_id, name, url, perms, type, icon, order_num, enabled, component_url, route_name, route_only) VALUES ('43', '4', '更新', NULL, 'sys:menu:update,sys:menu:save,sys:menu:delete', 3, NULL, 2, 1, NULL, 'sys_menu_update', 1);
INSERT INTO sys_menu(menu_id, parent_id, name, url, perms, type, icon, order_num, enabled, component_url, route_name, route_only) VALUES ('23', '2', '更新', NULL, 'sys:user:update,sys:user:delete,sys:user:resetPass,sys:user:save', 3, NULL, 2, 1, NULL, 'sys_user_update', 1);
INSERT INTO sys_menu(menu_id, parent_id, name, url, perms, type, icon, order_num, enabled, component_url, route_name, route_only) VALUES ('33', '3', '更新', NULL, 'sys:role:update,sys:role:delete,sys:role:save', 3, NULL, 2, 1, NULL, 'sys_role_update', 1);
INSERT INTO sys_menu(menu_id, parent_id, name, url, perms, type, icon, order_num, enabled, component_url, route_name, route_only) VALUES ('63', '6', '更新', NULL, 'sys:sysdict:update,sys:sysdict:delete,sys:sysdict:save', 3, NULL, 3, 1, NULL, 'sys_dict_update', 1);
INSERT INTO sys_menu(menu_id, parent_id, name, url, perms, type, icon, order_num, enabled, component_url, route_name, route_only) VALUES ('6', '1', '数据字典', '/sys/dict', 'sys:sysdict:list', 1, 'pinglun', 0, 1, 'sys/dict/list', 'sys_dict', 0);
INSERT INTO sys_menu(menu_id, parent_id, name, url, perms, type, icon, order_num, enabled, component_url, route_name, route_only) VALUES ('29', '1', '系统日志', '/sys/log', 'sys:log:list', 1, 'log', 7, 1, 'sys/log', 'sys_log', 0);
INSERT INTO sys_menu(menu_id, parent_id, name, url, perms, type, icon, order_num, enabled, component_url, route_name, route_only) VALUES ('7', '1', '参数管理', '/sys/config', 'sys:config:list,sys:config:info', 1, 'config', 6, 1, 'sys/config/list', 'sys_config', 0);
INSERT INTO sys_menu(menu_id, parent_id, name, url, perms, type, icon, order_num, enabled, component_url, route_name, route_only) VALUES ('2', '1', '用户管理', '/sys/user', 'sys:user', 1, 'admin', 1, 1, 'sys/user/list', 'sys_user', 1);
INSERT INTO sys_menu(menu_id, parent_id, name, url, perms, type, icon, order_num, enabled, component_url, route_name, route_only) VALUES ('3', '1', '角色管理', '/sys/role', 'sys:role', 1, 'role', 2, 1, 'sys/role/list', 'sys_role', 1);
INSERT INTO sys_menu(menu_id, parent_id, name, url, perms, type, icon, order_num, enabled, component_url, route_name, route_only) VALUES ('689fac31128df410811d43fc967b51b8', '9ff75d507236cd5a3ef6c516aa378884', '三方应用', '/exter/app', 'exter:app:list,exter:app:info,exter:app:update,exter:app:delete,exter:app:save', 1, 'log', 2, 1, 'exter/app/list', 'exter_app', 1);
INSERT INTO sys_menu(menu_id, parent_id, name, url, perms, type, icon, order_num, enabled, component_url, route_name, route_only) VALUES ('517517fc505487a279b3ee8e9b34e044', '9ff75d507236cd5a3ef6c516aa378884', '权限列表', 'exter/perms', 'exter:perms:list,exter:perms:info,exter:perms:update,exter:perms:delete,exter:perms:save', 1, 'zhedie', 1, 1, 'exter/perms/list', 'exter_perms', 1);
INSERT INTO sys_menu(menu_id, parent_id, name, url, perms, type, icon, order_num, enabled, component_url, route_name, route_only) VALUES ('880c79d0d137eb431c7d6f1d83489db9', '9ff75d507236cd5a3ef6c516aa378884', '调用记录', '/exter/log', 'exter:log', 1, 'pinglun', 3, 1, 'exter/log/index', 'exter_log', 1);
INSERT INTO sys_menu(menu_id, parent_id, name, url, perms, type, icon, order_num, enabled, component_url, route_name, route_only) VALUES ('1', '0', '系统管理', NULL, NULL, 0, 'system', 0, 1, 'main/black-main', 'sys', 1);
INSERT INTO sys_menu(menu_id, parent_id, name, url, perms, type, icon, order_num, enabled, component_url, route_name, route_only) VALUES ('9ff75d507236cd5a3ef6c516aa378884', '0', '三方授权', NULL, NULL, 0, 'role', 1, 1, NULL, NULL, 1);
INSERT INTO sys_menu(menu_id, parent_id, name, url, perms, type, icon, order_num, enabled, component_url, route_name, route_only) VALUES ('00954845842afea1b8537744185b6a42', '827c2c75173f7887271d23ce3cc6e105', '修改', '', 'form:template:update', 3, '', 0, 1, '', '', 1);
INSERT INTO sys_menu(menu_id, parent_id, name, url, perms, type, icon, order_num, enabled, component_url, route_name, route_only) VALUES ('75b18d7136131a1bab75438bc1dab2bf', '827c2c75173f7887271d23ce3cc6e105', '删除', '', 'form:template:delete', 3, '', 1, 1, '', '', 1);
INSERT INTO sys_menu(menu_id, parent_id, name, url, perms, type, icon, order_num, enabled, component_url, route_name, route_only) VALUES ('dfe66974746b39e3f763946495c5e58b', '094a9ddd1d9fbbc6cd8388d408e62929', '新增', '', 'form:data:save', 3, '', 0, 1, '', '', 1);
INSERT INTO sys_menu(menu_id, parent_id, name, url, perms, type, icon, order_num, enabled, component_url, route_name, route_only) VALUES ('10f9cd8724f945c8f4361215e551b616', '094a9ddd1d9fbbc6cd8388d408e62929', '更新', '', 'form:data:update', 3, '', 1, 1, '', '', 1);
INSERT INTO sys_menu(menu_id, parent_id, name, url, perms, type, icon, order_num, enabled, component_url, route_name, route_only) VALUES ('905a2599db4ea9bf9a0d268e4d20a403', '094a9ddd1d9fbbc6cd8388d408e62929', '处理', '', 'form:data:solve', 3, '', 2, 1, '', '', 1);
INSERT INTO sys_menu(menu_id, parent_id, name, url, perms, type, icon, order_num, enabled, component_url, route_name, route_only) VALUES ('0f70ee74fe3eb2f5c7f5bc25a3d97c92', '094a9ddd1d9fbbc6cd8388d408e62929', '删除', '', 'form:data:delete', 3, '', 3, 1, '', '', 1);
INSERT INTO sys_menu(menu_id, parent_id, name, url, perms, type, icon, order_num, enabled, component_url, route_name, route_only) VALUES ('094a9ddd1d9fbbc6cd8388d408e62929', '9c3c36cfabddca5fb932ed27065b4063', '数据登记', '/form/data?code=F00001', 'form:data:list', 1, 'icon-log', 1, 1, 'form/data/index', 'form_data', 1);
INSERT INTO sys_menu(menu_id, parent_id, name, url, perms, type, icon, order_num, enabled, component_url, route_name, route_only) VALUES ('d01784ec66976990e76ee52741606038', '0', '动态表单', '', '', 0, 'icon-feeds', 2, 1, '', '', 1);
INSERT INTO sys_menu(menu_id, parent_id, name, url, perms, type, icon, order_num, enabled, component_url, route_name, route_only) VALUES ('827c2c75173f7887271d23ce3cc6e105', 'd01784ec66976990e76ee52741606038', '表单模板', '/form/template', 'form:template:list', 1, 'icon-copy2', 0, 1, 'form/template/index', 'form_template', 1);
INSERT INTO sys_menu(menu_id, parent_id, name, url, perms, type, icon, order_num, enabled, component_url, route_name, route_only) VALUES ('9c3c36cfabddca5fb932ed27065b4063', '0', '信息登记', '', '', 0, 'icon-copy2', 3, 1, '', '', 1);


-- Table: sys_role

-- DROP TABLE sys_role;

CREATE TABLE sys_role
(
    role_id varchar(32)  NOT NULL,
    role_name varchar(100) ,
    remark varchar(100) ,
    create_user_id varchar(40) ,
    create_time timestamp(6) without time zone,
    user_default boolean DEFAULT false,
    CONSTRAINT sys_role_pkey PRIMARY KEY (role_id)
);

COMMENT ON COLUMN sys_role.role_id IS '主键';
COMMENT ON COLUMN sys_role.role_name  IS '角色名称';
COMMENT ON COLUMN sys_role.remark  IS '备注';
COMMENT ON COLUMN sys_role.create_user_id IS '创建人id';
COMMENT ON COLUMN sys_role.create_time  IS '创建时间';
COMMENT ON COLUMN sys_role.user_default IS '默认';

INSERT INTO sys_role(role_id, role_name, remark, create_user_id, create_time, user_default) VALUES ('642b25226705c4eb754be80b9790a735', '管理员', '管理员角色', 'superAdmin', '2019-12-23 13:59:36.474', 'f');


-- Table: sys_role_menu

-- DROP TABLE sys_role_menu;

CREATE TABLE sys_role_menu
(
    id varchar(32)  NOT NULL,
    role_id varchar(32) ,
    menu_id varchar(32) ,
    seq integer,
    CONSTRAINT sys_role_menu_pkey PRIMARY KEY (id)
);

COMMENT ON COLUMN sys_role_menu.id IS '主键';
COMMENT ON COLUMN sys_role_menu.role_id  IS '角色id';
COMMENT ON COLUMN sys_role_menu.menu_id  IS '菜单id';

INSERT INTO sys_role_menu(id, role_id, menu_id, seq) VALUES ('97c8e18dd0dc700ace4d1cc8ba468523', '642b25226705c4eb754be80b9790a735', '1', 1);
INSERT INTO sys_role_menu(id, role_id, menu_id, seq) VALUES ('c3351fe2d284fa707bbcbf7219f9a7b1', '642b25226705c4eb754be80b9790a735', '98b8dd41b16e4feabd7cf639090dbd7e', 2);
INSERT INTO sys_role_menu(id, role_id, menu_id, seq) VALUES ('69a0b960cb690892ed450d228fbbafaa', '642b25226705c4eb754be80b9790a735', '4', 3);
INSERT INTO sys_role_menu(id, role_id, menu_id, seq) VALUES ('05719de64f4c469fb62b8647bf62a88f', '642b25226705c4eb754be80b9790a735', '43', 4);
INSERT INTO sys_role_menu(id, role_id, menu_id, seq) VALUES ('44255c65c4cdb7aec5cfb91e6a194867', '642b25226705c4eb754be80b9790a735', '6', 5);
INSERT INTO sys_role_menu(id, role_id, menu_id, seq) VALUES ('cfc182b974393e2b4df6d295ed8feabd', '642b25226705c4eb754be80b9790a735', '63', 6);
INSERT INTO sys_role_menu(id, role_id, menu_id, seq) VALUES ('ca63edf846c5d184f0488d4b28ed937c', '642b25226705c4eb754be80b9790a735', '29', 7);
INSERT INTO sys_role_menu(id, role_id, menu_id, seq) VALUES ('dcac8c29a09798a871c57582bda9d703', '642b25226705c4eb754be80b9790a735', '7', 8);
INSERT INTO sys_role_menu(id, role_id, menu_id, seq) VALUES ('6826c5b7cdc4dafccf1b046a9fcf5919', '642b25226705c4eb754be80b9790a735', '73', 9);
INSERT INTO sys_role_menu(id, role_id, menu_id, seq) VALUES ('33da1496c861304a91061e14850416b5', '642b25226705c4eb754be80b9790a735', '2', 10);
INSERT INTO sys_role_menu(id, role_id, menu_id, seq) VALUES ('2685f3c5c56190228cf7cdc9aae9ebc4', '642b25226705c4eb754be80b9790a735', '23', 11);
INSERT INTO sys_role_menu(id, role_id, menu_id, seq) VALUES ('60fa93280f78b5500da78ec6b8827e81', '642b25226705c4eb754be80b9790a735', '3', 12);
INSERT INTO sys_role_menu(id, role_id, menu_id, seq) VALUES ('1aaee441070f60a713bb0d631b3f4f52', '642b25226705c4eb754be80b9790a735', '33', 13);
INSERT INTO sys_role_menu(id, role_id, menu_id, seq) VALUES ('1b29f26c95b459c14b980c3622d8bec4', '642b25226705c4eb754be80b9790a735', '827c2c75173f7887271d23ce3cc6e105', 14);
INSERT INTO sys_role_menu(id, role_id, menu_id, seq) VALUES ('38eda0aea03e4aa1f26de00a246f799f', '642b25226705c4eb754be80b9790a735', '00954845842afea1b8537744185b6a42', 15);
INSERT INTO sys_role_menu(id, role_id, menu_id, seq) VALUES ('394fbb2c6429cee7a4b816c2bc459d69', '642b25226705c4eb754be80b9790a735', '75b18d7136131a1bab75438bc1dab2bf', 16);
INSERT INTO sys_role_menu(id, role_id, menu_id, seq) VALUES ('b53a2be3154d3373149b398ade1e8969', '642b25226705c4eb754be80b9790a735', '9ff75d507236cd5a3ef6c516aa378884', 17);
INSERT INTO sys_role_menu(id, role_id, menu_id, seq) VALUES ('5cea8a5f701d6621acbc1f5537a44936', '642b25226705c4eb754be80b9790a735', '689fac31128df410811d43fc967b51b8', 18);
INSERT INTO sys_role_menu(id, role_id, menu_id, seq) VALUES ('92a783c8a9070e604d4d18e413f88a0a', '642b25226705c4eb754be80b9790a735', '517517fc505487a279b3ee8e9b34e044', 19);
INSERT INTO sys_role_menu(id, role_id, menu_id, seq) VALUES ('2aa04c8e6fbd5645e39f60e85ffbca3f', '642b25226705c4eb754be80b9790a735', '880c79d0d137eb431c7d6f1d83489db9', 20);
INSERT INTO sys_role_menu(id, role_id, menu_id, seq) VALUES ('2b5d60083fc96805dbc4944efa671cde', '642b25226705c4eb754be80b9790a735', '-666666', 21);



-- Table: sys_user

-- DROP TABLE sys_user;

CREATE TABLE sys_user
(
    user_id varchar(32)  NOT NULL,
    username varchar(50)  NOT NULL,
    password varchar(100)  NOT NULL,
    salt varchar(20)  NOT NULL,
    email varchar(100) ,
    mobile varchar(100) ,
    status smallint,
    create_time timestamp(6) without time zone,
    user_no varchar(50) ,
    sex smallint,
    logo varchar(200) ,
    last_login_time timestamp(6) with time zone,
    depart_id varchar(40) ,
    birthday date,
    create_id varchar(40) ,
    update_id varchar(40) ,
    update_time timestamp(4) with time zone,
    CONSTRAINT sys_user_pkey PRIMARY KEY (user_id),
    CONSTRAINT sys_user_user_no_key UNIQUE (user_no)
);

COMMENT ON TABLE sys_user IS '用户表';
COMMENT ON COLUMN sys_user.user_id IS 'id';
COMMENT ON COLUMN sys_user.username IS '姓名';
COMMENT ON COLUMN sys_user.password IS '密码';
COMMENT ON COLUMN sys_user.salt IS '盐';
COMMENT ON COLUMN sys_user.email IS '电子邮箱';
COMMENT ON COLUMN sys_user.mobile IS '手机号';
COMMENT ON COLUMN sys_user.status IS '状态';
COMMENT ON COLUMN sys_user.create_time IS '创建时间';
COMMENT ON COLUMN sys_user.user_no IS '账号名称';
COMMENT ON COLUMN sys_user.sex IS '性别';
COMMENT ON COLUMN sys_user.logo IS '头像';
COMMENT ON COLUMN sys_user.last_login_time IS '最后登录时间';
COMMENT ON COLUMN sys_user.depart_id IS '部门id';
COMMENT ON COLUMN sys_user.birthday IS '出生日期';
COMMENT ON COLUMN sys_user.create_id IS '创建人';
COMMENT ON COLUMN sys_user.update_id  IS '修改人';
COMMENT ON COLUMN sys_user.update_time IS '修改时间';


INSERT INTO sys_user(user_id, username, password, salt, email, mobile, status, create_time, user_no, sex, logo, last_login_time, depart_id, birthday, create_id, update_id, update_time) VALUES ('6c076dd48511c66f1a692b50d3d41bbd', 'admin', '6be95b1449cd098307e27c354c77ec93', 'NIyufT7h9K9muR3sab6R', NULL, '11', 1, '2021-08-08 17:07:32.245', 'admin', 1, NULL, '2021-08-13 14:48:30.066+08', NULL, NULL, NULL, NULL, NULL);


-- Table: sys_user_role

-- DROP TABLE sys_user_role;

CREATE TABLE sys_user_role
(
    id varchar(32)  NOT NULL,
    user_id varchar(32)  NOT NULL,
    role_id varchar(32)  NOT NULL,
    CONSTRAINT sys_user_role_pkey PRIMARY KEY (id)
);

COMMENT ON COLUMN sys_user_role.id IS '主键';
COMMENT ON COLUMN sys_user_role.user_id IS '用户id';
COMMENT ON COLUMN sys_user_role.role_id IS '角色id';

INSERT INTO sys_user_role(id, user_id, role_id) VALUES ('78f8a00eed34cc7712088ed3c30cec01', '6c076dd48511c66f1a692b50d3d41bbd', '642b25226705c4eb754be80b9790a735');

