--
-- PostgreSQL database dump
--

-- Dumped from database version 13.3
-- Dumped by pg_dump version 13.3

-- Started on 2021-08-13 15:48:38

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on; 
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off; 
--
-- TOC entry 236 (class 1255 OID 24755)
-- Name: execute_sql(character varying, anyarray); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.execute_sql(id_ character varying, params_ anyarray) RETURNS json
    LANGUAGE plpgsql
    AS $$

DECLARE

	_sql_row  sys_sql%ROWTYPE ; -- sql 查询语句

	_n_sql 		text ;

	_result		json ;

BEGIN

	

	select * into _sql_row from sys_sql where id = id_  ;



	if _sql_row is NULL then 

		return json_build_object('code' , 404, 'error','找不到对应查询语句');

	end if ;

	

	 _n_sql = ' with t_tmp as (' || _sql_row.sql || ') ';





	-- 判断返回类型

	if _sql_row.result_type = 1 THEN

		-- list

		_n_sql = _n_sql ||  ' select json_agg(row_to_json(t)) from t_tmp as t ' ;



	else 

		--only one

		_n_sql = _n_sql ||  ' select row_to_json(t)  from t_tmp as t limit 1' ;



	end if ;



	-- raise notice 'sql:%' , _n_sql;

	

	execute _n_sql using params_  into _result ;

	

	

	return  json_build_object('code' , 0 , 'data' , _result); 



END;

$$;


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 235 (class 1259 OID 32777)
-- Name: ng_form_data; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.ng_form_data (
    id character varying(32) NOT NULL,
    form_data jsonb,
    form_code character varying(20) NOT NULL,
    form_id character varying(32) NOT NULL, 
    create_id character varying(32) NOT NULL,
    create_time timestamp without time zone NOT NULL,
    update_id character varying(32),
    update_time timestamp without time zone
);


--
-- TOC entry 234 (class 1259 OID 32768)
-- Name: ng_form_template; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.ng_form_template (
    id character varying(32) NOT NULL,
    code character varying(20),
    name character varying(200) NOT NULL,
    template_data text,
    version integer,
    status smallint DEFAULT 0,
    remark text,
    create_id character varying(32) NOT NULL,
    create_time timestamp without time zone NOT NULL,
    update_id character varying(32),
    update_time timestamp without time zone
);


--
-- TOC entry 205 (class 1259 OID 24588)
-- Name: qrtz_blob_triggers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.qrtz_blob_triggers (
    sched_name character varying(120) NOT NULL,
    trigger_name character varying(200) NOT NULL,
    trigger_group character varying(200) NOT NULL,
    blob_data bytea
);


--
-- TOC entry 206 (class 1259 OID 24594)
-- Name: qrtz_calendars; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.qrtz_calendars (
    sched_name character varying(120) NOT NULL,
    calendar_name character varying(200) NOT NULL,
    calendar bytea NOT NULL
);


--
-- TOC entry 207 (class 1259 OID 24600)
-- Name: qrtz_cron_triggers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.qrtz_cron_triggers (
    sched_name character varying(120) NOT NULL,
    trigger_name character varying(200) NOT NULL,
    trigger_group character varying(200) NOT NULL,
    cron_expression character varying(120) NOT NULL,
    time_zone_id character varying(80)
);


--
-- TOC entry 208 (class 1259 OID 24606)
-- Name: qrtz_fired_triggers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.qrtz_fired_triggers (
    sched_name character varying(120) NOT NULL,
    entry_id character varying(95) NOT NULL,
    trigger_name character varying(200) NOT NULL,
    trigger_group character varying(200) NOT NULL,
    instance_name character varying(200) NOT NULL,
    fired_time bigint NOT NULL,
    sched_time bigint NOT NULL,
    priority integer NOT NULL,
    state character varying(16) NOT NULL,
    job_name character varying(200),
    job_group character varying(200),
    is_nonconcurrent boolean,
    requests_recovery boolean
);


--
-- TOC entry 209 (class 1259 OID 24612)
-- Name: qrtz_job_details; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.qrtz_job_details (
    sched_name character varying(120) NOT NULL,
    job_name character varying(200) NOT NULL,
    job_group character varying(200) NOT NULL,
    description character varying(250),
    job_class_name character varying(250) NOT NULL,
    is_durable boolean NOT NULL,
    is_nonconcurrent boolean NOT NULL,
    is_update_data boolean NOT NULL,
    requests_recovery boolean NOT NULL,
    job_data bytea
);


--
-- TOC entry 210 (class 1259 OID 24618)
-- Name: qrtz_locks; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.qrtz_locks (
    sched_name character varying(120) NOT NULL,
    lock_name character varying(40) NOT NULL
);


--
-- TOC entry 211 (class 1259 OID 24621)
-- Name: qrtz_paused_trigger_grps; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.qrtz_paused_trigger_grps (
    sched_name character varying(120) NOT NULL,
    trigger_group character varying(200) NOT NULL
);


--
-- TOC entry 212 (class 1259 OID 24624)
-- Name: qrtz_scheduler_state; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.qrtz_scheduler_state (
    sched_name character varying(120) NOT NULL,
    instance_name character varying(200) NOT NULL,
    last_checkin_time bigint NOT NULL,
    checkin_interval bigint NOT NULL
);


--
-- TOC entry 213 (class 1259 OID 24627)
-- Name: qrtz_simple_triggers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.qrtz_simple_triggers (
    sched_name character varying(120) NOT NULL,
    trigger_name character varying(200) NOT NULL,
    trigger_group character varying(200) NOT NULL,
    repeat_count bigint NOT NULL,
    repeat_interval bigint NOT NULL,
    times_triggered bigint NOT NULL
);


--
-- TOC entry 214 (class 1259 OID 24633)
-- Name: qrtz_simprop_triggers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.qrtz_simprop_triggers (
    sched_name character varying(120) NOT NULL,
    trigger_name character varying(200) NOT NULL,
    trigger_group character varying(200) NOT NULL,
    str_prop_1 character varying(512),
    str_prop_2 character varying(512),
    str_prop_3 character varying(512),
    int_prop_1 integer,
    int_prop_2 integer,
    long_prop_1 bigint,
    long_prop_2 bigint,
    dec_prop_1 numeric(13,4),
    dec_prop_2 numeric(13,4),
    bool_prop_1 boolean,
    bool_prop_2 boolean
);


--
-- TOC entry 215 (class 1259 OID 24639)
-- Name: qrtz_triggers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.qrtz_triggers (
    sched_name character varying(120) NOT NULL,
    trigger_name character varying(200) NOT NULL,
    trigger_group character varying(200) NOT NULL,
    job_name character varying(200) NOT NULL,
    job_group character varying(200) NOT NULL,
    description character varying(250),
    next_fire_time bigint,
    prev_fire_time bigint,
    priority integer,
    trigger_state character varying(16) NOT NULL,
    trigger_type character varying(8) NOT NULL,
    start_time bigint NOT NULL,
    end_time bigint,
    calendar_name character varying(200),
    misfire_instr smallint,
    job_data bytea
);


--
-- TOC entry 216 (class 1259 OID 24645)
-- Name: schedule_job; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.schedule_job (
    job_id bigint NOT NULL,
    bean_name character varying(200),
    method_name character varying(100),
    params character varying(2000),
    cron_expression character varying(100),
    status integer,
    remark character varying(255),
    create_time timestamp(6) without time zone
);


--
-- TOC entry 200 (class 1259 OID 24578)
-- Name: schedule_job_job_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.schedule_job_job_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3255 (class 0 OID 0)
-- Dependencies: 200
-- Name: schedule_job_job_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.schedule_job_job_id_seq OWNED BY public.schedule_job.job_id;


--
-- TOC entry 217 (class 1259 OID 24652)
-- Name: schedule_job_log; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.schedule_job_log (
    log_id bigint NOT NULL,
    job_id bigint,
    bean_name character varying(200),
    method_name character varying(100),
    params character varying(2000),
    status integer NOT NULL,
    error character varying(2000),
    times integer NOT NULL,
    create_time timestamp(6) without time zone
);


--
-- TOC entry 201 (class 1259 OID 24580)
-- Name: schedule_job_log_log_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.schedule_job_log_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3256 (class 0 OID 0)
-- Dependencies: 201
-- Name: schedule_job_log_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.schedule_job_log_log_id_seq OWNED BY public.schedule_job_log.log_id;


--
-- TOC entry 218 (class 1259 OID 24659)
-- Name: sys_config; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sys_config (
    param_key character varying(50) NOT NULL,
    param_value character varying(2000),
    status smallint DEFAULT 1,
    remark character varying(500),
    param_type integer DEFAULT 1
);


--
-- TOC entry 3257 (class 0 OID 0)
-- Dependencies: 218
-- Name: COLUMN sys_config.param_key; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_config.param_key IS 'key';


--
-- TOC entry 3258 (class 0 OID 0)
-- Dependencies: 218
-- Name: COLUMN sys_config.param_value; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_config.param_value IS 'value';


--
-- TOC entry 3259 (class 0 OID 0)
-- Dependencies: 218
-- Name: COLUMN sys_config.status; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_config.status IS '状态   0：隐藏   1：显示';


--
-- TOC entry 3260 (class 0 OID 0)
-- Dependencies: 218
-- Name: COLUMN sys_config.remark; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_config.remark IS '备注';


--
-- TOC entry 3261 (class 0 OID 0)
-- Dependencies: 218
-- Name: COLUMN sys_config.param_type; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_config.param_type IS '参数类型 1-字符串,2-数组,3-表格';
 
INSERT INTO sys_config(param_key, param_value, status, remark, param_type) VALUES ('login_expire_time', '3600', 1, '用户登录超时时间，单位秒', 1);
INSERT INTO sys_config(param_key, param_value, status, remark, param_type) VALUES ('repeat_login', '1', 1, '一个用户是否可以重复登录，1-允许，其他不允许', 1);


--
-- TOC entry 220 (class 1259 OID 24670)
-- Name: sys_dict; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sys_dict (
    dict_id character varying(40) NOT NULL,
    value character varying(255),
    dict_name character varying(50),
    type character varying(50),
    description text,
    seq integer,
    create_date timestamp(6) without time zone DEFAULT now(),
    update_date timestamp(6) without time zone DEFAULT now(),
    del_flag integer DEFAULT 0,
    create_by character varying(100),
    update_by character varying(100)
);


--
-- TOC entry 3271 (class 0 OID 0)
-- Dependencies: 220
-- Name: TABLE sys_dict; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.sys_dict IS '数据字典';


--
-- TOC entry 3272 (class 0 OID 0)
-- Dependencies: 220
-- Name: COLUMN sys_dict.dict_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_dict.dict_id IS 'ID';


--
-- TOC entry 3273 (class 0 OID 0)
-- Dependencies: 220
-- Name: COLUMN sys_dict.value; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_dict.value IS '值';


--
-- TOC entry 3274 (class 0 OID 0)
-- Dependencies: 220
-- Name: COLUMN sys_dict.dict_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_dict.dict_name IS '标签';


--
-- TOC entry 3275 (class 0 OID 0)
-- Dependencies: 220
-- Name: COLUMN sys_dict.type; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_dict.type IS '字典分类';


--
-- TOC entry 3276 (class 0 OID 0)
-- Dependencies: 220
-- Name: COLUMN sys_dict.description; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_dict.description IS '类型描述';


--
-- TOC entry 3277 (class 0 OID 0)
-- Dependencies: 220
-- Name: COLUMN sys_dict.seq; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_dict.seq IS '序号';


--
-- TOC entry 3278 (class 0 OID 0)
-- Dependencies: 220
-- Name: COLUMN sys_dict.create_date; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_dict.create_date IS '创建时间';


--
-- TOC entry 3279 (class 0 OID 0)
-- Dependencies: 220
-- Name: COLUMN sys_dict.update_date; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_dict.update_date IS '更新时间';


--
-- TOC entry 3280 (class 0 OID 0)
-- Dependencies: 220
-- Name: COLUMN sys_dict.del_flag; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_dict.del_flag IS '删除状态（0：可用   1：不可用）';


--
-- TOC entry 3281 (class 0 OID 0)
-- Dependencies: 220
-- Name: COLUMN sys_dict.create_by; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_dict.create_by IS '创建Id';


--
-- TOC entry 3282 (class 0 OID 0)
-- Dependencies: 220
-- Name: COLUMN sys_dict.update_by; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_dict.update_by IS '更新Id';


--
-- TOC entry 221 (class 1259 OID 24679)
-- Name: sys_exter_app; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sys_exter_app (
    id character varying(32) NOT NULL,
    name text NOT NULL,
    remark text,
    ak character varying(255),
    sk character varying(255),
    status integer NOT NULL,
    username character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    create_time timestamp(6) without time zone NOT NULL,
    update_time timestamp(6) without time zone,
    create_id character varying(32),
    update_id character varying(32)
);


--
-- TOC entry 222 (class 1259 OID 24685)
-- Name: sys_exter_app_perms; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sys_exter_app_perms (
    app_id character varying(32) NOT NULL,
    perm_id character varying(32) NOT NULL
);


--
-- TOC entry 223 (class 1259 OID 24688)
-- Name: sys_exter_app_token; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sys_exter_app_token (
    app_id character varying(32) NOT NULL,
    type integer DEFAULT 1,
    token character varying(32) NOT NULL,
    expire_time timestamp(6) without time zone NOT NULL,
    create_time timestamp(6) without time zone NOT NULL
);


--
-- TOC entry 224 (class 1259 OID 24692)
-- Name: sys_exter_log; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sys_exter_log (
    id bigint NOT NULL,
    appname character varying(255),
    operation character varying(255),
    method character varying(255),
    params character varying(5000),
    "time" bigint DEFAULT 0 NOT NULL,
    ip character varying(64),
    create_date timestamp(6) without time zone DEFAULT now()
);


--
-- TOC entry 202 (class 1259 OID 24582)
-- Name: sys_exter_log_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sys_exter_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3283 (class 0 OID 0)
-- Dependencies: 202
-- Name: sys_exter_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.sys_exter_log_id_seq OWNED BY public.sys_exter_log.id;


--
-- TOC entry 225 (class 1259 OID 24701)
-- Name: sys_exter_perms; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sys_exter_perms (
    id character varying(32) NOT NULL,
    name character varying(32) NOT NULL,
    remark text,
    perms character varying(255)
);


--
-- TOC entry 226 (class 1259 OID 24707)
-- Name: sys_file; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sys_file (
    id character varying(32) NOT NULL,
    old_file_name character varying(255) NOT NULL,
    file_suffix character varying(50),
    file_url character varying(500) NOT NULL,
    file_size integer,
    create_time timestamp(0) without time zone NOT NULL,
    new_file_name character varying(255) NOT NULL,
    store_type smallint NOT NULL,
    create_id character varying(40)
);


--
-- TOC entry 3284 (class 0 OID 0)
-- Dependencies: 226
-- Name: TABLE sys_file; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.sys_file IS '文件信息表（上传）';


--
-- TOC entry 3285 (class 0 OID 0)
-- Dependencies: 226
-- Name: COLUMN sys_file.id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_file.id IS '主键id';


--
-- TOC entry 3286 (class 0 OID 0)
-- Dependencies: 226
-- Name: COLUMN sys_file.old_file_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_file.old_file_name IS '原始文件名称';


--
-- TOC entry 3287 (class 0 OID 0)
-- Dependencies: 226
-- Name: COLUMN sys_file.file_suffix; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_file.file_suffix IS '文件类型，即后缀';


--
-- TOC entry 3288 (class 0 OID 0)
-- Dependencies: 226
-- Name: COLUMN sys_file.file_url; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_file.file_url IS '文件路径';


--
-- TOC entry 3289 (class 0 OID 0)
-- Dependencies: 226
-- Name: COLUMN sys_file.file_size; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_file.file_size IS '文件大小（byte），预留';


--
-- TOC entry 3290 (class 0 OID 0)
-- Dependencies: 226
-- Name: COLUMN sys_file.create_time; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_file.create_time IS '创建时间';


--
-- TOC entry 3291 (class 0 OID 0)
-- Dependencies: 226
-- Name: COLUMN sys_file.new_file_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_file.new_file_name IS '保存时文件名称';


--
-- TOC entry 227 (class 1259 OID 24713)
-- Name: sys_log; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sys_log (
    id bigint NOT NULL,
    username character varying(50),
    operation character varying(50),
    method character varying(200),
    params character varying(5000),
    "time" bigint DEFAULT 0 NOT NULL,
    ip character varying(64),
    create_date timestamp(6) without time zone DEFAULT now(),
    system character varying(20)
);


--
-- TOC entry 3292 (class 0 OID 0)
-- Dependencies: 227
-- Name: TABLE sys_log; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.sys_log IS '系统日志';


--
-- TOC entry 3293 (class 0 OID 0)
-- Dependencies: 227
-- Name: COLUMN sys_log.username; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_log.username IS '用户';


--
-- TOC entry 3294 (class 0 OID 0)
-- Dependencies: 227
-- Name: COLUMN sys_log.operation; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_log.operation IS '用户操作';


--
-- TOC entry 3295 (class 0 OID 0)
-- Dependencies: 227
-- Name: COLUMN sys_log.method; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_log.method IS '请求方法';


--
-- TOC entry 3296 (class 0 OID 0)
-- Dependencies: 227
-- Name: COLUMN sys_log.params; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_log.params IS '用户操作';


--
-- TOC entry 3297 (class 0 OID 0)
-- Dependencies: 227
-- Name: COLUMN sys_log."time"; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_log."time" IS '执行时长(毫秒)';


--
-- TOC entry 3298 (class 0 OID 0)
-- Dependencies: 227
-- Name: COLUMN sys_log.ip; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_log.ip IS 'IP地址';


--
-- TOC entry 3299 (class 0 OID 0)
-- Dependencies: 227
-- Name: COLUMN sys_log.create_date; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_log.create_date IS 'params';


--
-- TOC entry 3300 (class 0 OID 0)
-- Dependencies: 227
-- Name: COLUMN sys_log.system; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_log.system IS '系统模块';


--
-- TOC entry 203 (class 1259 OID 24584)
-- Name: sys_log_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sys_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3301 (class 0 OID 0)
-- Dependencies: 203
-- Name: sys_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.sys_log_id_seq OWNED BY public.sys_log.id;


--
-- TOC entry 228 (class 1259 OID 24722)
-- Name: sys_menu; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sys_menu (
    menu_id character varying(32) NOT NULL,
    parent_id character varying(32),
    name character varying(254),
    url character varying(254),
    perms character varying(254),
    type integer,
    icon character varying(254),
    order_num integer,
    enabled integer DEFAULT 1,
    component_url character varying(254),
    route_name character varying(254),
    route_only smallint DEFAULT 1
);


--
-- TOC entry 3302 (class 0 OID 0)
-- Dependencies: 228
-- Name: TABLE sys_menu; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.sys_menu IS '菜单管理';


--
-- TOC entry 3303 (class 0 OID 0)
-- Dependencies: 228
-- Name: COLUMN sys_menu.menu_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_menu.menu_id IS '菜单ID';


--
-- TOC entry 3304 (class 0 OID 0)
-- Dependencies: 228
-- Name: COLUMN sys_menu.parent_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_menu.parent_id IS '父菜单ID，一级菜单为null';


--
-- TOC entry 3305 (class 0 OID 0)
-- Dependencies: 228
-- Name: COLUMN sys_menu.name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_menu.name IS '菜单名称';


--
-- TOC entry 3306 (class 0 OID 0)
-- Dependencies: 228
-- Name: COLUMN sys_menu.url; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_menu.url IS '菜单URL';


--
-- TOC entry 3307 (class 0 OID 0)
-- Dependencies: 228
-- Name: COLUMN sys_menu.perms; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_menu.perms IS '授权(多个用逗号分隔，如：user:list,user:create)';


--
-- TOC entry 3308 (class 0 OID 0)
-- Dependencies: 228
-- Name: COLUMN sys_menu.type; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_menu.type IS '类型   0：目录   1：菜单   2：按钮';


--
-- TOC entry 3309 (class 0 OID 0)
-- Dependencies: 228
-- Name: COLUMN sys_menu.icon; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_menu.icon IS '菜单图标';


--
-- TOC entry 3310 (class 0 OID 0)
-- Dependencies: 228
-- Name: COLUMN sys_menu.order_num; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_menu.order_num IS '排序';


--
-- TOC entry 3311 (class 0 OID 0)
-- Dependencies: 228
-- Name: COLUMN sys_menu.enabled; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_menu.enabled IS '是否启用 1-启用';


--
-- TOC entry 229 (class 1259 OID 24730)
-- Name: sys_role; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sys_role (
    role_id character varying(50) NOT NULL,
    role_name character varying(100),
    remark character varying(100),
    create_user_id character varying(40),
    create_time timestamp(6) without time zone,
    user_default boolean DEFAULT false
);


--
-- TOC entry 3312 (class 0 OID 0)
-- Dependencies: 229
-- Name: COLUMN sys_role.role_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_role.role_id IS '主键';


--
-- TOC entry 3313 (class 0 OID 0)
-- Dependencies: 229
-- Name: COLUMN sys_role.role_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_role.role_name IS '角色名称';


--
-- TOC entry 3314 (class 0 OID 0)
-- Dependencies: 229
-- Name: COLUMN sys_role.remark; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_role.remark IS '备注';


--
-- TOC entry 3315 (class 0 OID 0)
-- Dependencies: 229
-- Name: COLUMN sys_role.create_user_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_role.create_user_id IS '创建人id';


--
-- TOC entry 3316 (class 0 OID 0)
-- Dependencies: 229
-- Name: COLUMN sys_role.create_time; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_role.create_time IS '创建时间';


--
-- TOC entry 3317 (class 0 OID 0)
-- Dependencies: 229
-- Name: COLUMN sys_role.user_default; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_role.user_default IS '默认';


--
-- TOC entry 230 (class 1259 OID 24734)
-- Name: sys_role_menu; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sys_role_menu (
    id character varying(32) NOT NULL,
    role_id character varying(32),
    menu_id character varying(32),
    seq integer
);


--
-- TOC entry 3318 (class 0 OID 0)
-- Dependencies: 230
-- Name: COLUMN sys_role_menu.id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_role_menu.id IS '主键';


--
-- TOC entry 3319 (class 0 OID 0)
-- Dependencies: 230
-- Name: COLUMN sys_role_menu.role_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_role_menu.role_id IS '角色id';


--
-- TOC entry 3320 (class 0 OID 0)
-- Dependencies: 230
-- Name: COLUMN sys_role_menu.menu_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_role_menu.menu_id IS '菜单id';


--
-- TOC entry 204 (class 1259 OID 24586)
-- Name: sys_role_menu_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sys_role_menu_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3321 (class 0 OID 0)
-- Dependencies: 204
-- Name: sys_role_menu_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.sys_role_menu_id_seq OWNED BY public.sys_role_menu.id;


--
-- TOC entry 231 (class 1259 OID 24738)
-- Name: sys_sql; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sys_sql (
    id character varying(32) DEFAULT md5((clock_timestamp())::text) NOT NULL,
    sql text NOT NULL,
    result_type integer DEFAULT 1 NOT NULL,
    description text
);


--
-- TOC entry 3322 (class 0 OID 0)
-- Dependencies: 231
-- Name: COLUMN sys_sql.result_type; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_sql.result_type IS '1-列表,2-单数据';


--
-- TOC entry 232 (class 1259 OID 24746)
-- Name: sys_user; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sys_user (
    user_id character varying(32) NOT NULL,
    username character varying(50) NOT NULL,
    password character varying(100) NOT NULL,
    salt character varying(20) NOT NULL,
    email character varying(100),
    mobile character varying(100),
    status smallint,
    create_time timestamp(6) without time zone,
    user_no character varying(50),
    sex smallint,
    logo character varying(200),
    last_login_time timestamp(6) with time zone,
    depart_id character varying(40),
    birthday date,
    create_id character varying(40),
    update_id character varying(40),
    update_time timestamp(4) with time zone
);


--
-- TOC entry 3323 (class 0 OID 0)
-- Dependencies: 232
-- Name: TABLE sys_user; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE public.sys_user IS '用户表';


--
-- TOC entry 3324 (class 0 OID 0)
-- Dependencies: 232
-- Name: COLUMN sys_user.user_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_user.user_id IS 'id';


--
-- TOC entry 3325 (class 0 OID 0)
-- Dependencies: 232
-- Name: COLUMN sys_user.username; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_user.username IS '姓名';


--
-- TOC entry 3326 (class 0 OID 0)
-- Dependencies: 232
-- Name: COLUMN sys_user.password; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_user.password IS '密码';


--
-- TOC entry 3327 (class 0 OID 0)
-- Dependencies: 232
-- Name: COLUMN sys_user.salt; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_user.salt IS '盐';


--
-- TOC entry 3328 (class 0 OID 0)
-- Dependencies: 232
-- Name: COLUMN sys_user.email; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_user.email IS '电子邮箱';


--
-- TOC entry 3329 (class 0 OID 0)
-- Dependencies: 232
-- Name: COLUMN sys_user.mobile; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_user.mobile IS '手机号';


--
-- TOC entry 3330 (class 0 OID 0)
-- Dependencies: 232
-- Name: COLUMN sys_user.status; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_user.status IS '状态';


--
-- TOC entry 3331 (class 0 OID 0)
-- Dependencies: 232
-- Name: COLUMN sys_user.create_time; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_user.create_time IS '创建时间';


--
-- TOC entry 3332 (class 0 OID 0)
-- Dependencies: 232
-- Name: COLUMN sys_user.user_no; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_user.user_no IS '账号名称';


--
-- TOC entry 3333 (class 0 OID 0)
-- Dependencies: 232
-- Name: COLUMN sys_user.sex; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_user.sex IS '性别';


--
-- TOC entry 3334 (class 0 OID 0)
-- Dependencies: 232
-- Name: COLUMN sys_user.logo; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_user.logo IS '头像';


--
-- TOC entry 3335 (class 0 OID 0)
-- Dependencies: 232
-- Name: COLUMN sys_user.last_login_time; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_user.last_login_time IS '最后登录时间';


--
-- TOC entry 3336 (class 0 OID 0)
-- Dependencies: 232
-- Name: COLUMN sys_user.depart_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_user.depart_id IS '部门id';


--
-- TOC entry 3337 (class 0 OID 0)
-- Dependencies: 232
-- Name: COLUMN sys_user.birthday; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_user.birthday IS '出生日期';


--
-- TOC entry 3338 (class 0 OID 0)
-- Dependencies: 232
-- Name: COLUMN sys_user.create_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_user.create_id IS '创建人';


--
-- TOC entry 3339 (class 0 OID 0)
-- Dependencies: 232
-- Name: COLUMN sys_user.update_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_user.update_id IS '修改人';


--
-- TOC entry 3340 (class 0 OID 0)
-- Dependencies: 232
-- Name: COLUMN sys_user.update_time; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_user.update_time IS '修改时间';


--
-- TOC entry 233 (class 1259 OID 24752)
-- Name: sys_user_role; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sys_user_role (
    id character varying(32) NOT NULL,
    user_id character varying(32) NOT NULL,
    role_id character varying(32) NOT NULL
);


--
-- TOC entry 3341 (class 0 OID 0)
-- Dependencies: 233
-- Name: COLUMN sys_user_role.id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_user_role.id IS '主键';


--
-- TOC entry 3342 (class 0 OID 0)
-- Dependencies: 233
-- Name: COLUMN sys_user_role.user_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_user_role.user_id IS '用户id';


--
-- TOC entry 3343 (class 0 OID 0)
-- Dependencies: 233
-- Name: COLUMN sys_user_role.role_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN public.sys_user_role.role_id IS '角色id';


--
-- TOC entry 3001 (class 2604 OID 24648)
-- Name: schedule_job job_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.schedule_job ALTER COLUMN job_id SET DEFAULT nextval('public.schedule_job_job_id_seq'::regclass);


--
-- TOC entry 3002 (class 2604 OID 24655)
-- Name: schedule_job_log log_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.schedule_job_log ALTER COLUMN log_id SET DEFAULT nextval('public.schedule_job_log_log_id_seq'::regclass);


--
-- TOC entry 3009 (class 2604 OID 24695)
-- Name: sys_exter_log id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sys_exter_log ALTER COLUMN id SET DEFAULT nextval('public.sys_exter_log_id_seq'::regclass);


--
-- TOC entry 3012 (class 2604 OID 24716)
-- Name: sys_log id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sys_log ALTER COLUMN id SET DEFAULT nextval('public.sys_log_id_seq'::regclass);


--
-- TOC entry 3018 (class 2604 OID 24737)
-- Name: sys_role_menu id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sys_role_menu ALTER COLUMN id SET DEFAULT nextval('public.sys_role_menu_id_seq'::regclass);


 

--
-- TOC entry 3248 (class 0 OID 32768)
-- Dependencies: 234
-- Data for Name: ng_form_template; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.ng_form_template VALUES ('5254b611b1dcce3c0b580ed76febf91a', 'F00001', '测试模板', '{"list":[{"type":"input","label":"姓名","options":{"type":"text","width":"100%","defaultValue":"","placeholder":"请输入","clearable":false,"maxLength":0,"prepend":"","append":"","tooptip":"","hidden":false,"disabled":false,"dynamicHide":false,"dynamicHideValue":""},"model":"name","key":"input_1628838173424","rules":[{"required":true,"message":"姓名不能为空","trigger":["blur"]}]},{"type":"textarea","label":"地址","options":{"width":"100%","maxLength":0,"defaultValue":"","rows":4,"clearable":false,"tooptip":"","hidden":false,"disabled":false,"placeholder":"请输入","dynamicHide":false,"dynamicHideValue":""},"model":"address","key":"textarea_1628838173424","rules":[{"required":true,"message":"地址不能为空","trigger":["blur"]}]}],"config":{"labelPosition":"left","labelWidth":100,"size":"mini","outputHidden":true,"hideRequiredMark":true,"customStyle":"","showList":["name","address"],"queryList":["name","address"]}}', 1, 1, NULL, 'superAdmin', '2021-08-13 15:06:34.881', NULL, NULL);
 

INSERT INTO public.sys_dict VALUES ('a0cf269261206dff5653d69a7567f45a', '1', '已处理', 'solve_status', '处理类型', 0, '2021-08-13 15:27:35.548', '2021-08-13 15:27:35.548', 0, 'superAdmin', 'superAdmin');
INSERT INTO public.sys_dict VALUES ('dcde7405ed0e2aaa1226d4198448233b', '2', '拒绝处理', 'solve_status', '处理类型', 1, '2021-08-13 15:27:47.833', '2021-08-13 15:27:47.833', 0, 'superAdmin', 'superAdmin');

 
--
-- TOC entry 3242 (class 0 OID 24722)
-- Dependencies: 228
-- Data for Name: sys_menu; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.sys_menu VALUES ('98b8dd41b16e4feabd7cf639090dbd7e', '1', '定时任务', 'job/schedule', 'job:schedule', 1, 'job', 21, 1, 'job/schedule', 'job_scedule', 1);
INSERT INTO public.sys_menu VALUES ('4', '1', '菜单管理', '/sys/menu', 'sys:menu:list', 1, 'menu', 3, 1, 'sys/menu/list', 'sys_menu', 0);
INSERT INTO public.sys_menu VALUES ('73', '7', '更新', NULL, 'sys:config:update,sys:config:save,sys:config:delete', 3, NULL, 2, 1, NULL, 'sys_config_update', 1);
INSERT INTO public.sys_menu VALUES ('43', '4', '更新', NULL, 'sys:menu:update,sys:menu:save,sys:menu:delete', 3, NULL, 2, 1, NULL, 'sys_menu_update', 1);
INSERT INTO public.sys_menu VALUES ('23', '2', '更新', NULL, 'sys:user:update,sys:user:delete,sys:user:resetPass,sys:user:save', 3, NULL, 2, 1, NULL, 'sys_user_update', 1);
INSERT INTO public.sys_menu VALUES ('33', '3', '更新', NULL, 'sys:role:update,sys:role:delete,sys:role:save', 3, NULL, 2, 1, NULL, 'sys_role_update', 1);
INSERT INTO public.sys_menu VALUES ('63', '6', '更新', NULL, 'sys:sysdict:update,sys:sysdict:delete,sys:sysdict:save', 3, NULL, 3, 1, NULL, 'sys_dict_update', 1);
INSERT INTO public.sys_menu VALUES ('5', '1', 'SQL监控', '/sys/sql', NULL, 1, 'sql', 4, 1, NULL, NULL, 1);
INSERT INTO public.sys_menu VALUES ('6', '1', '数据字典', '/sys/dict', 'sys:sysdict:list', 1, 'pinglun', 0, 1, 'sys/dict/list', 'sys_dict', 0);
INSERT INTO public.sys_menu VALUES ('29', '1', '系统日志', '/sys/log', 'sys:log:list', 1, 'log', 7, 1, 'sys/log', 'sys_log', 0);
INSERT INTO public.sys_menu VALUES ('7', '1', '参数管理', '/sys/config', 'sys:config:list,sys:config:info', 1, 'config', 6, 1, 'sys/config/list', 'sys_config', 0);
INSERT INTO public.sys_menu VALUES ('2', '1', '用户管理', '/sys/user', 'sys:user', 1, 'admin', 1, 1, 'sys/user/list', 'sys_user', 1);
INSERT INTO public.sys_menu VALUES ('3', '1', '角色管理', '/sys/role', 'sys:role', 1, 'role', 2, 1, 'sys/role/list', 'sys_role', 1);
INSERT INTO public.sys_menu VALUES ('689fac31128df410811d43fc967b51b8', '9ff75d507236cd5a3ef6c516aa378884', '三方应用', '/exter/app', 'exter:app:list,exter:app:info,exter:app:update,exter:app:delete,exter:app:save', 1, 'log', 2, 1, 'exter/app/list', 'exter_app', 1);
INSERT INTO public.sys_menu VALUES ('517517fc505487a279b3ee8e9b34e044', '9ff75d507236cd5a3ef6c516aa378884', '权限列表', 'exter/perms', 'exter:perms:list,exter:perms:info,exter:perms:update,exter:perms:delete,exter:perms:save', 1, 'zhedie', 1, 1, 'exter/perms/list', 'exter_perms', 1);
INSERT INTO public.sys_menu VALUES ('880c79d0d137eb431c7d6f1d83489db9', '9ff75d507236cd5a3ef6c516aa378884', '调用记录', '/exter/log', 'exter:log', 1, 'pinglun', 3, 1, 'exter/log/index', 'exter_log', 1);
INSERT INTO public.sys_menu VALUES ('e7ac5a4fd20d6fc66a02098e3cfb5e9f', '1', '文件上传', '/sys/fileupload?id=1', NULL, 1, 'icon-arrow-up', 12, 1, 'sys/fileupload', 'fileupload', 1);
INSERT INTO public.sys_menu VALUES ('1', '0', '系统管理', NULL, NULL, 0, 'system', 0, 1, 'main/black-main', 'sys', 1);
INSERT INTO public.sys_menu VALUES ('9ff75d507236cd5a3ef6c516aa378884', '0', '三方授权', NULL, NULL, 0, 'role', 1, 1, NULL, NULL, 1);
INSERT INTO public.sys_menu VALUES ('00954845842afea1b8537744185b6a42', '827c2c75173f7887271d23ce3cc6e105', '修改', '', 'form:template:update', 3, '', 0, 1, '', '', 1);
INSERT INTO public.sys_menu VALUES ('75b18d7136131a1bab75438bc1dab2bf', '827c2c75173f7887271d23ce3cc6e105', '删除', '', 'form:template:delete', 3, '', 1, 1, '', '', 1);
INSERT INTO public.sys_menu VALUES ('dfe66974746b39e3f763946495c5e58b', '094a9ddd1d9fbbc6cd8388d408e62929', '新增', '', 'form:data:save', 3, '', 0, 1, '', '', 1);
INSERT INTO public.sys_menu VALUES ('10f9cd8724f945c8f4361215e551b616', '094a9ddd1d9fbbc6cd8388d408e62929', '更新', '', 'form:data:update', 3, '', 1, 1, '', '', 1);
INSERT INTO public.sys_menu VALUES ('905a2599db4ea9bf9a0d268e4d20a403', '094a9ddd1d9fbbc6cd8388d408e62929', '处理', '', 'form:data:solve', 3, '', 2, 1, '', '', 1);
INSERT INTO public.sys_menu VALUES ('0f70ee74fe3eb2f5c7f5bc25a3d97c92', '094a9ddd1d9fbbc6cd8388d408e62929', '删除', '', 'form:data:delete', 3, '', 3, 1, '', '', 1);
INSERT INTO public.sys_menu VALUES ('9c3c36cfabddca5fb932ed27065b4063', '0', '信息登记', '', '', 0, 'icon-copy2', 2, 1, '', '', 1);
INSERT INTO public.sys_menu VALUES ('094a9ddd1d9fbbc6cd8388d408e62929', '9c3c36cfabddca5fb932ed27065b4063', '数据登记', '/form/data?code=F00001', 'form:data:list', 1, 'icon-log', 1, 1, 'form/data/index', 'form_data', 1);
INSERT INTO public.sys_menu VALUES ('827c2c75173f7887271d23ce3cc6e105', '1', '表单模板', '/form/template', 'form:template:list', 1, 'icon-copy2', 0, 1, 'form/template/index', 'form_template', 1);


--
-- TOC entry 3243 (class 0 OID 24730)
-- Dependencies: 229
-- Data for Name: sys_role; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.sys_role VALUES ('642b25226705c4eb754be80b9790a735', '管理员', '管理员角色', 'superAdmin', '2019-12-23 13:59:36.474', false);


--
-- TOC entry 3244 (class 0 OID 24734)
-- Dependencies: 230
-- Data for Name: sys_role_menu; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.sys_role_menu VALUES ('4690a7d314930446d629a4a953ded8e0', '642b25226705c4eb754be80b9790a735', '1', 1);
INSERT INTO public.sys_role_menu VALUES ('3d1e06b95d653a967dc13cf72745180d', '642b25226705c4eb754be80b9790a735', '98b8dd41b16e4feabd7cf639090dbd7e', 2);
INSERT INTO public.sys_role_menu VALUES ('598a60e816024681d0df4adb4fd02008', '642b25226705c4eb754be80b9790a735', '4', 3);
INSERT INTO public.sys_role_menu VALUES ('a79fe405c49006d7dc716df65ab9ceef', '642b25226705c4eb754be80b9790a735', '43', 4);
INSERT INTO public.sys_role_menu VALUES ('4f38a55c7c8932e3e520102c2b6ee793', '642b25226705c4eb754be80b9790a735', '5', 5);
INSERT INTO public.sys_role_menu VALUES ('3b95b2cf0dfe7ff0c276a740f2812eaf', '642b25226705c4eb754be80b9790a735', '6', 6);
INSERT INTO public.sys_role_menu VALUES ('3016cb8bf1fdd1478604b3579a107b78', '642b25226705c4eb754be80b9790a735', '63', 7);
INSERT INTO public.sys_role_menu VALUES ('bb1d2b250f97e89ddb4bdda9e7398ca6', '642b25226705c4eb754be80b9790a735', '29', 8);
INSERT INTO public.sys_role_menu VALUES ('74d5a103cccb0690a341d33f243e083d', '642b25226705c4eb754be80b9790a735', '7', 9);
INSERT INTO public.sys_role_menu VALUES ('dd8b778f1978d225c489e29ccb039316', '642b25226705c4eb754be80b9790a735', '73', 10);
INSERT INTO public.sys_role_menu VALUES ('b7b95107a02e7284e6ee9ea277e160cb', '642b25226705c4eb754be80b9790a735', '2', 11);
INSERT INTO public.sys_role_menu VALUES ('1ad54989fe2baa35f9c2ad7f550985ea', '642b25226705c4eb754be80b9790a735', '23', 12);
INSERT INTO public.sys_role_menu VALUES ('1131d39806f5f4d46b0d9f659be93140', '642b25226705c4eb754be80b9790a735', '3', 13);
INSERT INTO public.sys_role_menu VALUES ('f600636478f283ec0e2e4373a0a0b21e', '642b25226705c4eb754be80b9790a735', '33', 14);
INSERT INTO public.sys_role_menu VALUES ('f6c8615083dad398cd97e985076e58ef', '642b25226705c4eb754be80b9790a735', 'e7ac5a4fd20d6fc66a02098e3cfb5e9f', 15);
INSERT INTO public.sys_role_menu VALUES ('77867f02f2e1b77a0a3012d35826ca51', '642b25226705c4eb754be80b9790a735', '9ff75d507236cd5a3ef6c516aa378884', 16);
INSERT INTO public.sys_role_menu VALUES ('ebe6d3ba120d9747ea65116b3fdf854c', '642b25226705c4eb754be80b9790a735', '689fac31128df410811d43fc967b51b8', 17);
INSERT INTO public.sys_role_menu VALUES ('db4d177ffcc7d328a6f3cabedfbeb387', '642b25226705c4eb754be80b9790a735', '517517fc505487a279b3ee8e9b34e044', 18);
INSERT INTO public.sys_role_menu VALUES ('54e7dd85375066a7422e2b3f935f90eb', '642b25226705c4eb754be80b9790a735', '880c79d0d137eb431c7d6f1d83489db9', 19);
INSERT INTO public.sys_role_menu VALUES ('e5e79b51f69a8f8970d14f303311c6b9', '642b25226705c4eb754be80b9790a735', '-666666', 20);


--
-- TOC entry 3245 (class 0 OID 24738)
-- Dependencies: 231
-- Data for Name: sys_sql; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3246 (class 0 OID 24746)
-- Dependencies: 232
-- Data for Name: sys_user; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.sys_user VALUES ('6c076dd48511c66f1a692b50d3d41bbd', 'admin', '923ddfe973765683b48339c85605e9ad', 'NIyufT7h9K9muR3sab6R', NULL, '', 1, '2021-08-08 17:07:32.245', 'admin', 1, NULL, '2021-08-13 14:48:30.066+08', NULL, NULL, NULL, NULL, NULL);


--
-- TOC entry 3247 (class 0 OID 24752)
-- Dependencies: 233
-- Data for Name: sys_user_role; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.sys_user_role VALUES ('519731ab38d425f6ba8d2a39898ca80f', '6c076dd48511c66f1a692b50d3d41bbd', '642b25226705c4eb754be80b9790a735');


--
-- TOC entry 3344 (class 0 OID 0)
-- Dependencies: 200
-- Name: schedule_job_job_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.schedule_job_job_id_seq', 5, true);


--
-- TOC entry 3345 (class 0 OID 0)
-- Dependencies: 201
-- Name: schedule_job_log_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.schedule_job_log_log_id_seq', 18, true);


--
-- TOC entry 3346 (class 0 OID 0)
-- Dependencies: 202
-- Name: sys_exter_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.sys_exter_log_id_seq', 6, true);


--
-- TOC entry 3347 (class 0 OID 0)
-- Dependencies: 203
-- Name: sys_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.sys_log_id_seq', 1515, true);


--
-- TOC entry 3348 (class 0 OID 0)
-- Dependencies: 204
-- Name: sys_role_menu_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.sys_role_menu_id_seq', 32, true);


--
-- TOC entry 3063 (class 2606 OID 24797)
-- Name: sys_file file_info_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sys_file
    ADD CONSTRAINT file_info_pkey PRIMARY KEY (id);


--
-- TOC entry 3083 (class 2606 OID 32784)
-- Name: ng_form_data ng_form_data_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ng_form_data
    ADD CONSTRAINT ng_form_data_pkey PRIMARY KEY (id);


--
-- TOC entry 3081 (class 2606 OID 32776)
-- Name: ng_form_template ng_form_template_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ng_form_template
    ADD CONSTRAINT ng_form_template_pkey PRIMARY KEY (id);


--
-- TOC entry 3031 (class 2606 OID 24765)
-- Name: qrtz_job_details qrtz_job_details_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.qrtz_job_details
    ADD CONSTRAINT qrtz_job_details_pkey PRIMARY KEY (sched_name, job_name, job_group);


--
-- TOC entry 3047 (class 2606 OID 24781)
-- Name: schedule_job_log schedule_job_log_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.schedule_job_log
    ADD CONSTRAINT schedule_job_log_pkey PRIMARY KEY (log_id);


--
-- TOC entry 3045 (class 2606 OID 24779)
-- Name: schedule_job schedule_job_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.schedule_job
    ADD CONSTRAINT schedule_job_pkey PRIMARY KEY (job_id);


--
-- TOC entry 3049 (class 2606 OID 24783)
-- Name: sys_config sys_config_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sys_config
    ADD CONSTRAINT sys_config_pkey PRIMARY KEY (param_key);


--
-- TOC entry 3051 (class 2606 OID 24785)
-- Name: sys_department sys_department_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sys_department
    ADD CONSTRAINT sys_department_pkey PRIMARY KEY (department_id);


--
-- TOC entry 3053 (class 2606 OID 24787)
-- Name: sys_dict sys_dict_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sys_dict
    ADD CONSTRAINT sys_dict_pkey PRIMARY KEY (dict_id);


--
-- TOC entry 3057 (class 2606 OID 24791)
-- Name: sys_exter_app_perms sys_exter_app_perms_app_id_perm_id_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sys_exter_app_perms
    ADD CONSTRAINT sys_exter_app_perms_app_id_perm_id_key UNIQUE (app_id, perm_id);


--
-- TOC entry 3055 (class 2606 OID 24789)
-- Name: sys_exter_app sys_exter_app_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sys_exter_app
    ADD CONSTRAINT sys_exter_app_pkey PRIMARY KEY (id);


--
-- TOC entry 3059 (class 2606 OID 24793)
-- Name: sys_exter_log sys_exter_log_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sys_exter_log
    ADD CONSTRAINT sys_exter_log_pkey PRIMARY KEY (id);


--
-- TOC entry 3061 (class 2606 OID 24795)
-- Name: sys_exter_perms sys_exter_perms_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sys_exter_perms
    ADD CONSTRAINT sys_exter_perms_pkey PRIMARY KEY (id);


--
-- TOC entry 3065 (class 2606 OID 24799)
-- Name: sys_log sys_log_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sys_log
    ADD CONSTRAINT sys_log_pkey PRIMARY KEY (id);


--
-- TOC entry 3067 (class 2606 OID 24801)
-- Name: sys_menu sys_menu_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sys_menu
    ADD CONSTRAINT sys_menu_pkey PRIMARY KEY (menu_id);


--
-- TOC entry 3071 (class 2606 OID 24805)
-- Name: sys_role_menu sys_role_menu_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sys_role_menu
    ADD CONSTRAINT sys_role_menu_pkey PRIMARY KEY (id);


--
-- TOC entry 3069 (class 2606 OID 24803)
-- Name: sys_role sys_role_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sys_role
    ADD CONSTRAINT sys_role_pkey PRIMARY KEY (role_id);


--
-- TOC entry 3073 (class 2606 OID 24807)
-- Name: sys_sql sys_sql_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sys_sql
    ADD CONSTRAINT sys_sql_pkey PRIMARY KEY (id);


--
-- TOC entry 3075 (class 2606 OID 24811)
-- Name: sys_user sys_user_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sys_user
    ADD CONSTRAINT sys_user_pkey PRIMARY KEY (user_id);


--
-- TOC entry 3079 (class 2606 OID 24813)
-- Name: sys_user_role sys_user_role_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sys_user_role
    ADD CONSTRAINT sys_user_role_pkey PRIMARY KEY (id);


--
-- TOC entry 3077 (class 2606 OID 24809)
-- Name: sys_user sys_user_user_no_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sys_user
    ADD CONSTRAINT sys_user_user_no_key UNIQUE (user_no);


--
-- TOC entry 3022 (class 1259 OID 24756)
-- Name: idx_qrtz_ft_inst_job_req_rcvry; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_qrtz_ft_inst_job_req_rcvry ON public.qrtz_fired_triggers USING btree (sched_name, instance_name, requests_recovery);


--
-- TOC entry 3023 (class 1259 OID 24757)
-- Name: idx_qrtz_ft_j_g; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_qrtz_ft_j_g ON public.qrtz_fired_triggers USING btree (sched_name, job_name, job_group);


--
-- TOC entry 3024 (class 1259 OID 24758)
-- Name: idx_qrtz_ft_jg; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_qrtz_ft_jg ON public.qrtz_fired_triggers USING btree (sched_name, job_group);


--
-- TOC entry 3025 (class 1259 OID 24759)
-- Name: idx_qrtz_ft_t_g; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_qrtz_ft_t_g ON public.qrtz_fired_triggers USING btree (sched_name, trigger_name, trigger_group);


--
-- TOC entry 3026 (class 1259 OID 24760)
-- Name: idx_qrtz_ft_tg; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_qrtz_ft_tg ON public.qrtz_fired_triggers USING btree (sched_name, trigger_group);


--
-- TOC entry 3027 (class 1259 OID 24761)
-- Name: idx_qrtz_ft_trig_inst_name; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_qrtz_ft_trig_inst_name ON public.qrtz_fired_triggers USING btree (sched_name, instance_name);


--
-- TOC entry 3028 (class 1259 OID 24762)
-- Name: idx_qrtz_j_grp; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_qrtz_j_grp ON public.qrtz_job_details USING btree (sched_name, job_group);


--
-- TOC entry 3029 (class 1259 OID 24763)
-- Name: idx_qrtz_j_req_recovery; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_qrtz_j_req_recovery ON public.qrtz_job_details USING btree (sched_name, requests_recovery);


--
-- TOC entry 3032 (class 1259 OID 24766)
-- Name: idx_qrtz_t_c; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_qrtz_t_c ON public.qrtz_triggers USING btree (sched_name, calendar_name);


--
-- TOC entry 3033 (class 1259 OID 24767)
-- Name: idx_qrtz_t_g; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_qrtz_t_g ON public.qrtz_triggers USING btree (sched_name, trigger_group);


--
-- TOC entry 3034 (class 1259 OID 24768)
-- Name: idx_qrtz_t_j; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_qrtz_t_j ON public.qrtz_triggers USING btree (sched_name, job_name, job_group);


--
-- TOC entry 3035 (class 1259 OID 24769)
-- Name: idx_qrtz_t_jg; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_qrtz_t_jg ON public.qrtz_triggers USING btree (sched_name, job_group);


--
-- TOC entry 3036 (class 1259 OID 24770)
-- Name: idx_qrtz_t_n_g_state; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_qrtz_t_n_g_state ON public.qrtz_triggers USING btree (sched_name, trigger_group, trigger_state);


--
-- TOC entry 3037 (class 1259 OID 24771)
-- Name: idx_qrtz_t_n_state; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_qrtz_t_n_state ON public.qrtz_triggers USING btree (sched_name, trigger_name, trigger_group, trigger_state);


--
-- TOC entry 3038 (class 1259 OID 24772)
-- Name: idx_qrtz_t_next_fire_time; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_qrtz_t_next_fire_time ON public.qrtz_triggers USING btree (sched_name, next_fire_time);


--
-- TOC entry 3039 (class 1259 OID 24773)
-- Name: idx_qrtz_t_nft_misfire; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_qrtz_t_nft_misfire ON public.qrtz_triggers USING btree (sched_name, misfire_instr, next_fire_time);


--
-- TOC entry 3040 (class 1259 OID 24774)
-- Name: idx_qrtz_t_nft_st; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_qrtz_t_nft_st ON public.qrtz_triggers USING btree (sched_name, trigger_state, next_fire_time);


--
-- TOC entry 3041 (class 1259 OID 24775)
-- Name: idx_qrtz_t_nft_st_misfire; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_qrtz_t_nft_st_misfire ON public.qrtz_triggers USING btree (sched_name, misfire_instr, next_fire_time, trigger_state);


--
-- TOC entry 3042 (class 1259 OID 24776)
-- Name: idx_qrtz_t_nft_st_misfire_grp; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_qrtz_t_nft_st_misfire_grp ON public.qrtz_triggers USING btree (sched_name, misfire_instr, next_fire_time, trigger_group, trigger_state);


--
-- TOC entry 3043 (class 1259 OID 24777)
-- Name: idx_qrtz_t_state; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_qrtz_t_state ON public.qrtz_triggers USING btree (sched_name, trigger_state);


-- Completed on 2021-08-13 15:48:42

--
-- PostgreSQL database dump complete
--

