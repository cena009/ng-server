package com.ng;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
 

@SpringBootApplication(exclude={DataSourceAutoConfiguration.class})
@EnableAutoConfiguration(exclude = { 
	    SecurityAutoConfiguration.class 
	})
public class NgApplication  extends SpringBootServletInitializer{

	public static void main(String[] args) {
		SpringApplication.run(NgApplication.class, args);
		
		System.setProperty("tomcat.util.http.parser.HttpParser.requestTargetAllow","|{}");
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(NgApplication.class);
	}
}
