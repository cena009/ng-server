package com.ng.db.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ng.db.dao.ADao;
import com.ng.db.service.AService;

@Service
public class AServiceImpl implements AService{

	
	@Autowired
	private ADao dao ;
	
	 
	public void insertLog() {
		dao.insertLog();
		if(Math.random() > 0.1) {
			throw new RuntimeException("11");
		}
	}
}
