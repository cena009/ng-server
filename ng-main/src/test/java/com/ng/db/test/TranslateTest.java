package com.ng.db.test;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;

import com.ng.db.service.AService;
 

/**
 * 事务测试
 * @author lyf
 *
 */  
@SpringBootTest
@SpringBootApplication(scanBasePackages = "com.ng")
public class TranslateTest {

	@Autowired
	private AService service ;
	
	@Test
	@DisplayName("测试事务插入")
	public void testA() {
		
		service.insertLog();
		
		
		
	}
	
}
