package com.ng.common.fileupload.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ng.common.fileupload.dao.UploadFileInfoDao;
import com.ng.common.fileupload.entity.UploadFileInfoEntity;
import com.ng.common.fileupload.service.UploadFileInfoService;




@Service
public class UploadFileInfoServiceImpl extends ServiceImpl<UploadFileInfoDao, UploadFileInfoEntity> implements UploadFileInfoService {




	@Override
	public boolean insert(UploadFileInfoEntity entity) {
		// TODO Auto-generated method stub
		if(entity.getCreateTime() == null) {
			entity.setCreateTime(new Date());
		}
		
		return this.save(entity);
	}
 

	@Override
	public Collection<UploadFileInfoEntity> selectList(String[] ids) {
		// TODO Auto-generated method stub
		 Collection<UploadFileInfoEntity> list = listByIds(Arrays.asList(ids));
		 
		 // 回填name
		/* list.stream().forEach(t->  {
			 if(t.getName() == null) {
				 t.setName(t.getOldFileName());
			 }
		 });*/
		 
		 return list ;
	}


	@Override
	public List<String> selectUrlList(String id) {
		List<String> stringList = new ArrayList<>();
		QueryWrapper<UploadFileInfoEntity>  warWrapper = new QueryWrapper<>();
		warWrapper.eq(StringUtils.isNotBlank(id),"id",id);
		 List<UploadFileInfoEntity>  sysUploadFileEntities = this.list(warWrapper);
		 if(sysUploadFileEntities!=null && sysUploadFileEntities.size()>0){
			 stringList.addAll(sysUploadFileEntities.stream().map(UploadFileInfoEntity::getFileUrl).collect(Collectors.toList()));
		 }
		return stringList;
	}


	@Override
	public UploadFileInfoEntity selectFileEntity(String id) {
		QueryWrapper<UploadFileInfoEntity>  wrapper = new QueryWrapper<>();
		wrapper.eq(StringUtils.isNotBlank(id),"id",id);
		UploadFileInfoEntity sysUploadFileEntity =  getOne(wrapper);
		return sysUploadFileEntity;
	}

	 
 
}
