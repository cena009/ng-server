package com.ng.common.fileupload.service;

import java.util.Collection;
import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ng.common.fileupload.entity.UploadFileInfoEntity;

/**
 *
 *
 * @author lyf
 * @email jjxliu306@163.com
 * @date 2019-03-25 15:18:30
 */ 
public interface UploadFileInfoService  extends IService<UploadFileInfoEntity> {
 

	/**
	 * 插入一条附件信息
	 * @param entity
	 * @return
	 */
	boolean insert(UploadFileInfoEntity entity);



 


	/**
	 * 根据多条ID 查询附件集合
	 * @param ids
	 * @return
	 */
	Collection<UploadFileInfoEntity> selectList(String[] ids);
 
 
	/**
	 * 根据ID 查询附件地址
	 * @param id
	 * @return
	 */
	List<String>  selectUrlList(String id);

	/**
	 * 根据ID 查询附件地址
	 * @param id
	 * @return
	 */
	UploadFileInfoEntity  selectFileEntity(String id);
}

