package com.ng.common.fileupload.dao;


import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ng.common.fileupload.entity.UploadFileInfoEntity;

/**
 * 
 * 
 * @author lyf
 * @email jjxliu306@163.com
 * @date 2019-03-25 15:18:30
 */
@Mapper
public interface UploadFileInfoDao extends BaseMapper<UploadFileInfoEntity> {


}
