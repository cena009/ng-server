package com.ng.common.fileupload.entity;

import java.io.Serializable;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.enums.IEnum;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 文件信息表
 * @author CaoRui
 * @email caorui@tr-software.com
 * @date 2019-04-10 18:20:48
 */
@TableName("sys_file")
@Data
public class UploadFileInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	/**
	 * 存储方式
	 * @author lyf
	 *
	 */
	public enum StoreType implements IEnum<Integer>{
		lcoal, // 本地
		ftp, //ftp
		fastDfs ,
		qiniu,
		minio;

		@Override
		public Integer getValue() {
			// TODO Auto-generated method stub
			return this.ordinal();
		} // fastdfs
		
		 
	}

    /**
   	 * 存储类型 0- 本地  1- ftp  2- fastdfs
   	 */
   	
    @TableField("store_type")
   	private Integer storeType ; 
   	
   	public void setStoreType(Integer storeType) {
   		this.storeType = storeType;
   	}
   	
   	public Integer getStoreType() {
   		return storeType;
   	}
 
	/**
	 * 创建时间
	 */
	@TableField("create_time")
	@ApiModelProperty("创建时间")
	private Date createTime;
	
	@Autowired
	@TableField("create_id")
	@ApiModelProperty("创建人ID")
	private String createId;
	/**
	 * 主键id
	 */
	@TableId(type = IdType.UUID)
	@TableField("id")
	@ApiModelProperty("主键id")
	private String id;
	/**
	 * 文件大小（byte），预留
	 */
	@TableField("file_size")
	@ApiModelProperty("文件大小（byte），预留")
	private Long fileSize;
	/**
	 * 文件类型，即后缀
	 */
	@TableField("file_suffix")
	@ApiModelProperty("文件类型，即后缀")
	private String fileSuffix;
	/**
	 * 文件路径
	 */
	@TableField("file_url")
	@ApiModelProperty("文件路径")
	private String fileUrl;
	/**
	 * 保存时文件名称
	 */
	@TableField("new_file_name")
	@ApiModelProperty("保存时文件名称")
	private String newFileName;
	/**
	 * 原始文件名称
	 */
	@TableField("old_file_name")
	@ApiModelProperty("原始文件名称")
	private String oldFileName;
 
       
}
