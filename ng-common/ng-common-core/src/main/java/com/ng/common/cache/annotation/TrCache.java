package com.ng.common.cache.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *  cache 注解
 * 
 * 
 * 注解支持表达式和字符串拼接 <br>
 * 1、参数可以用#p0 #p1 表示第几个参数 支持表达式，也支持直接使用参数名称 譬如: #param1 <br>
 * 2、默认缓存key为 当前类的simpleName+ 当前方法的名称+所有参数key-value的 拼接字符串<br>
 * 3、缓存可以基于token 使用方法 #scope.token <br>
 * 
 * eg: @trCache(value="'cacheKey' + #p0") <br>
 * 
 * @author lyf
 * @date 2018年10月13日 上午10:19:56
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface TrCache {

	/**
	 * 缓存key
	 * @return
	 */
	String value() default "";
	 
	/**
	 * 缓存秒数 -1 描述参考系统默认缓存时间
	 */
	int cacheSecond() default 60*60;
	
	/**
	 * 是否缓存null 默认返回结果为null不缓存
	 * @return
	 */
	boolean cacheNull() default false ;
	
	/**
	 * class名称附带到key前面 默认为true
	 * @return
	 */
	boolean classNamePrefixKey() default true ;
}
