package com.ng.common.cache.aspect;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSON;
import com.ng.common.cache.annotation.TrCache;
import com.ng.common.cache.annotation.TrCacheEvict;
import com.ng.common.cache.annotation.TrCacheEvict.EvictType;
import com.ng.common.redis.RedisUtils;
import com.ng.common.utils.HttpContextUtils;

import lombok.Data;

/**
 * 注解缓存拦截器
 * @author lyf
 *
 */
@Aspect
@Component
public class CacheAspect {

	private Logger logger = LoggerFactory.getLogger(getClass());
 
	@Autowired
	private RedisUtils redisUtils ;

 
 
	// 定义一个切入点
	@Around("@annotation(trCache)")
	public Object aroundCache(ProceedingJoinPoint point , TrCache trCache) throws Throwable {
		
		
		// 获取注解信息
		MethodSignature signature = (MethodSignature) point.getSignature();
		Method method = signature.getMethod();
		
		  
		if(trCache == null) {
			return point.proceed();
		}
		//请求的参数
		Object[] args = point.getArgs();
		String[] paramNames = signature.getParameterNames();
		 
		String key = trCache.value();
 
		
		if(StringUtils.isBlank(key)) {
			// key为空 就拼所有参数得key-value
			key = method.getName() ;
			
			
			 
			Map<String ,Object> jo = new HashMap<>();
			for(int i = 0 ; i < paramNames.length && i < args.length ; i++) {
				String pname = paramNames[i];
				Object value = args[i];

				if(value instanceof MultipartFile) {
					Map<String, Object> p_map = new HashMap<>();
					MultipartFile file = (MultipartFile)value;
					p_map.put("filename", file.getOriginalFilename());
					p_map.put("file_size", file.getSize());

					value = p_map;
				}
				String tempValue = JSON.toJSONString(value) ;//value.toString(); 
				jo.put(pname, tempValue);
			}
			 
			key += redisUtils.mapToString(jo);
		} else if(key.contains("'") || key.contains("#")){
			
			// 判断key是否有表达式
			key = getEpValue(key, method.getName(), point.getTarget().getClass().getSimpleName(), paramNames, args);
			
		}
		
		
		
		// 判断是否需要class前缀
		if(trCache.classNamePrefixKey()) {
			key = point.getTarget().getClass().getSimpleName() + "." + key ;
		}
		
		
		
		// 判断缓存中是否存在
		if(redisUtils.containKey(key)) {
			//logger.debug("has cache , key: [{}]   " , key );
			return redisUtils.get(key);
		}
		
		
		Object result = point.proceed();
		
		if(result != null) {
			redisUtils.set(key, result , trCache.cacheSecond());
			
			logger.debug("set cache , key: [{}]  " , key );
		}

		return result;
	}  
	
	
	// 定义一个切入点 
	@Around("@annotation(trCacheEvict)")
	public Object aroundEvict(ProceedingJoinPoint point, TrCacheEvict trCacheEvict) throws Throwable {
			 

			// 获取注解信息
			MethodSignature signature = (MethodSignature) point.getSignature();
			Method method = signature.getMethod();
			 
			
			if(trCacheEvict == null) {
				return point.proceed();
			}
			
			String key = trCacheEvict.value();
			TrCacheEvict.EvictType type = trCacheEvict.type();
			
		
			
			// 如果什么都不填 则直接清空该class下得所有缓存
			if(StringUtils.isBlank(key)) {
				key =  point.getTarget().getClass().getSimpleName() ;
				type = EvictType.start;
			}  else if(key.contains("'") || key.contains("#")){
				//请求的参数
				Object[] args = point.getArgs();
				String[] paramNames = signature.getParameterNames();
				key = getEpValue(key, method.getName(), point.getTarget().getClass().getSimpleName(), paramNames, args);
				
				if(trCacheEvict.classNamePrefixKey()) {
					key = point.getTarget().getClass().getSimpleName() + "." + key ;
				}
			}
			
			
			
			
			if(trCacheEvict.beforeInvocation()) {
				// 判断策略
				switch (type) {
				case equal:
					redisUtils.delete(key);
					break;
				case contains:
					redisUtils.deleteContains(key);
					break;
				case start:
					redisUtils.deleteStartKey(key);
					break;

				default:
					break;
				}
				
				logger.debug("remove cache , key: [{}]  , type:[{}] " , key , trCacheEvict.type());
			}
			
			
			Object result = point.proceed();

			if(!trCacheEvict.beforeInvocation()) {
				// 判断策略
				switch (type) {
				case equal:
					redisUtils.delete(key);
					break;
				case contains:
					redisUtils.deleteContains(key);
					break;
				case start:
					redisUtils.deleteStartKey(key);
					break;

				default:
					break;
				}
				logger.debug("remove cache , key: [{}]  , type:[{}] " , key , trCacheEvict.type());
			}
				 
			return result;
		}  
		 
		
	ExpressionParser ep = new SpelExpressionParser();
	CacheStandardEvaluationContext context =new CacheStandardEvaluationContext();
	
	
	private synchronized String getEpValue(String script , String method , String className , String[] argNames , Object[] argValues) {
		
		context.clearVals();
		
		for(int i = 0 ; i < argNames.length ; i++) {
			context.setVariable(argNames[i], argValues[i]);
			
			context.setVariable("p" + i, argValues[i]);
		}
		
		context.setVariable("method", method);
		context.setVariable("className", className);
		
		//logger.debug("cache key script : [{}]" , script);
		
		if(script.contains("#scope")) {
			_Scope scope = new _Scope();
			
			// 先捞取token
			String token = HttpContextUtils.getRequestToken();
			scope.setToken(token);
			
			context.setVariable("scope", scope);
		}
		
		Expression e = ep.parseExpression(script);
		
		Object value = e.getValue(context );
		
		String key = value == null ? "" : value.toString(); 
		 
		return key ;
		
	}
		
	
	@Data
	protected class _Scope {
		
		private String token;
		
	}
	
 
}
