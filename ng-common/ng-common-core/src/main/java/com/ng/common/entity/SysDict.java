package com.ng.common.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;

public class SysDict implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4754957935328935572L;
	/**
	 * id
	 */
	@TableId(type = IdType.UUID)
	@TableField("dict_id")
	private String dictId;
	/**
	 * 值
	 */
	@TableField("value")
	private String value;
	/**
	 * 标签
	 */
	@TableField("dict_name")
	private String dictName;
	/**
	 * 字典分类
	 */
	@TableField("type")
	private String type;
	/**
	 * 类型名称
	 */
	@TableField("description")
	private String description;
	/**
	 * 序号
	 */
	@TableField("seq")
	private Integer seq;
	/**
	 * 创建时间
	 */
	@TableField("create_date")
	private Date createDate;
	/**
	 * 更新时间
	 */
	@TableField("update_date")
	private Date updateDate;
	/**
	 * 删除状态（0：可用   1：不可用）
	 */
	@TableField("del_flag")
	private Integer delFlag;
	/**
	 * 创建Id
	 */
	@TableField("create_by")
	private String createBy;
	/**
	 * 更新Id
	 */
	@TableField("update_by")
	private String updateBy;


	@TableField(exist = false)
	private int count;

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getDictId() {
		return dictId;
	}
	public void setDictId(String dictId) {
		this.dictId = dictId;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getDictName() {
		return dictName;
	}
	public void setDictName(String dictName) {
		this.dictName = dictName;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getSeq() {
		return seq;
	}
	public void setSeq(Integer seq) {
		this.seq = seq;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	public Integer getDelFlag() {
		return delFlag;
	}
	public void setDelFlag(Integer delFlag) {
		this.delFlag = delFlag;
	}
	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	 
	
}
