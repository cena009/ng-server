package com.ng.common.redis.lock.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RedisLock {

    String key() default "";
    
    /**
     * 锁过期时间
     * @return
     */
    int expireTime() default 30;//30秒
    
    /**
	 * class名称附带到key前面 默认为true
	 * @return
	 */
	boolean classNamePrefixKey() default true ;

}
