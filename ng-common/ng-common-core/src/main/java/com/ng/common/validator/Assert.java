package com.ng.common.validator;

import org.apache.commons.lang.StringUtils;

import com.ng.common.exception.NgException;

/**
 * 数据校验
 * @author lyf
 * 
 * @date 2017-03-23 15:50
 */
public abstract class Assert {

    public static void isBlank(String str, String message) {
        if (StringUtils.isBlank(str)) {
            throw new NgException(message);
        }
    }
    
    public static void expression(boolean value, String message) {
        if (value) {
            throw new NgException(message);
        }
    }

    public static void isNull(Object object, String message) {
        if (object == null) {
            throw new NgException(message);
        }
    }
}
