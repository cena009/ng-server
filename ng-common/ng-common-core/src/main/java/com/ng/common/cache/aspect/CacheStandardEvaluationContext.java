package com.ng.common.cache.aspect;

import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.lang.Nullable;

public class CacheStandardEvaluationContext extends StandardEvaluationContext {

	private final Map<String, Object> variables = new ConcurrentHashMap<>();
 
	@Override
	public void setVariable(@Nullable String name, @Nullable Object value) {
		// For backwards compatibility, we ignore null names here...
		// And since ConcurrentHashMap cannot store null values, we simply take null
		// as a remove from the Map (with the same result from lookupVariable below).
		if (name != null) {
			if (value != null) {
				this.variables.put(name, value);
			}
			else {
				this.variables.remove(name);
			}
		}
	}

	public void setVariables(Map<String, Object> variables) {
		variables.forEach(this::setVariable);
	}

	public void registerFunction(String name, Method method) {
		this.variables.put(name, method);
	}

	@Override
	@Nullable
	public Object lookupVariable(String name) {
		return this.variables.get(name);
	}
	
	public void clearVals() {
		variables.clear();
	}
	
}
