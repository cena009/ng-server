package com.ng.common.utils;

public interface TValue<T> {

	Object getValue(T t);
	
}
