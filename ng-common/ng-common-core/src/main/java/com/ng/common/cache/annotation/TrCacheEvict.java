package com.ng.common.cache.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 缓存清除 <br>
 * 不帶key就清除該service下的所有緩存
 * @author lyf
 *
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface TrCacheEvict {

	/**
	 * 要清除得key  
	 * @return
	 */
	String value() default "";
	
	EvictType type() default EvictType.equal;
	
	/**
	 * 前置清空缓存
	 * @return
	 */
	boolean beforeInvocation() default false;
	
	/**
	 * class名称附带到key前面 默认为true
	 * @return
	 */
	boolean classNamePrefixKey() default true ;
	
	/**
	 * 清除类型
	 * @author lyf
	 *
	 */
	enum EvictType {
		
		/**
		 * 全匹配
		 */
		equal ,
		
		/**
		 * 包含
		 */
		contains , 
		/**
		 * startWith
		 */
		start
	}
	
}
