package com.ng.common.utils;

import java.security.MessageDigest;
import java.util.UUID;

import com.ng.common.exception.NgException;

/**
 * 生成token
 *
 * @author lyf
 * 
 * @date 2017-05-20 14:41
 */
public class TokenGenerator {

    public static String generateValue() {
        return generateValue(UUID.randomUUID().toString());
    }

    private static final char[] hexCode = "0123456789abcdef".toCharArray();

    public static String toHexString(byte[] data) {
        if(data == null) {
            return null;
        }
        StringBuilder r = new StringBuilder(data.length*2);
        for ( byte b : data) {
            r.append(hexCode[(b >> 4) & 0xF]);
            r.append(hexCode[(b & 0xF)]);
        }
        String code = r.toString();
        
        if(code.length() >= 40) {
        	code = code.substring(0 , 39);
        }
        
        return code ;
    }

    public static String generateValue(String param) {
        try {
            MessageDigest algorithm = MessageDigest.getInstance("MD5");
            algorithm.reset();
            algorithm.update(param.getBytes());
            byte[] messageDigest = algorithm.digest();
            return toHexString(messageDigest);
        } catch (Exception e) {
            throw new NgException("生成Token失败", e);
        }
    }
}
