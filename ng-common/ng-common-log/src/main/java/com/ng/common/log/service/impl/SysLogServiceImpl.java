
package com.ng.common.log.service.impl;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ng.common.log.dao.SysLogDao;
import com.ng.common.log.entity.SysLogEntity;
import com.ng.common.log.service.SysLogService;
import com.ng.common.utils.PageUtils;
import com.ng.common.utils.Query;


@Service
public class SysLogServiceImpl extends ServiceImpl<SysLogDao, SysLogEntity> implements SysLogService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String key = (String)params.get("key");
 
         
        
        IPage<SysLogEntity> page = this.page(
            new Query<SysLogEntity>(params).getPage(),
           // pageI,
            new QueryWrapper<SysLogEntity>().like(StringUtils.isNotBlank(key),"username", key)
            
            .orderByDesc("create_date")
        );

        return new PageUtils(page);
    }
}
