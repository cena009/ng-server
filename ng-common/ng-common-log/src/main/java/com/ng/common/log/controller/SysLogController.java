

package com.ng.common.log.controller;

import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ng.common.log.service.SysLogService;
import com.ng.common.utils.PageUtils;
import com.ng.common.utils.R;


/**
 * 系统日志
 * 
 * @author lyf
 * @email liuyf@gs-softwares.com
 * @date 2017-03-08 10:40:56
 */
@Controller
@RequestMapping("/log")
public class SysLogController {
	@Autowired
	private SysLogService sysLogService;
	
	/**
	 * 列表
	 */
	@ResponseBody
	@GetMapping("/list")
	@RequiresPermissions("sys:log:list")
	public R<PageUtils> list(@RequestParam Map<String, Object> params){
		PageUtils page = sysLogService.queryPage(params);

		return R.ok(PageUtils.class).setData( page);
	}
	
}
