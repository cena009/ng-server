
package com.ng.common.log.dao;


import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ng.common.log.entity.SysLogEntity;

/**
 * 系统日志
 * 
 * @author lyf
 * 
 * @date 2017-03-08 10:40:56
 */
@Mapper
public interface SysLogDao extends BaseMapper<SysLogEntity> {
	
}
