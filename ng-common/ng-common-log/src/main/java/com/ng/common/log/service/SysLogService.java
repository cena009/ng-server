
package com.ng.common.log.service;


import java.util.Map;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ng.common.log.entity.SysLogEntity;
import com.ng.common.utils.PageUtils;


/**
 * 系统日志
 * 
 * @author lyf
 * 
 * @date 2017-03-08 10:40:56
 */
public interface SysLogService extends IService<SysLogEntity> {

    PageUtils queryPage(Map<String, Object> params);

}
