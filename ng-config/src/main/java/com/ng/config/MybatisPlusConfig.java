
package com.ng.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.extension.plugins.pagination.optimize.JsqlParserCountOptimize;

/**
 * mybatis-plus配置
 *
 * @author Mark sunlightcs@gmail.com
 * @since 2.0.0 2018-02-05
 */
@Configuration
@MapperScan(basePackages= {"com.ng.*.dao","com.ng.*.*.dao"})
public class MybatisPlusConfig {
 
	 /**
     * 分页插件
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
    	NgPaginationInterceptor p = new NgPaginationInterceptor();
        
    	p.setOverflow(true);
    	p.setCountSqlParser(new JsqlParserCountOptimize(true));
    	
    	 
    	
    	return p ;
    }
	
    /**
	 * Sequence主键自增
	 *
	 * @return 返回oracle自增类 
	 */
//	@Bean
//	public OracleKeyGenerator oracleKeyGenerator(){
//		return new OracleKeyGenerator();
//	} 
}
