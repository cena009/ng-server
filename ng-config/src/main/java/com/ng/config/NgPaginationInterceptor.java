package com.ng.config;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.ibatis.executor.statement.StatementHandler;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Signature;
import org.apache.ibatis.scripting.defaults.DefaultParameterHandler;

import com.baomidou.mybatisplus.core.MybatisDefaultParameterHandler;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.ExceptionUtils;
import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;

import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * 修改分页策略, 如果当前页码大于实际总页码,则取最后一页而不是默认得第一页
 * @author lyf
 *
 */
@Setter
@Accessors(chain = true)
@Intercepts({@Signature(type = StatementHandler.class, method = "prepare", args = {Connection.class, Integer.class})})
public class NgPaginationInterceptor extends PaginationInterceptor {
	
	
//	@Override
//	public void beforeQuery(Executor executor, MappedStatement ms, Object parameter, RowBounds rowBounds,
//			ResultHandler resultHandler, BoundSql boundSql) throws SQLException {
//		// TODO Auto-generated method stub
//		super.beforeQuery(executor, ms, parameter, rowBounds, resultHandler, boundSql);
//	}
//	
//	
//
	@Override
	protected void queryTotal(boolean overflowCurrent, String sql, MappedStatement mappedStatement, BoundSql boundSql,
			IPage<?> page, Connection connection) {
		try (PreparedStatement statement = connection.prepareStatement(sql)) {
            DefaultParameterHandler parameterHandler = new MybatisDefaultParameterHandler(mappedStatement, boundSql.getParameterObject(), boundSql);
            parameterHandler.setParameters(statement);
            long total = 0;
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    total = resultSet.getLong(1);
                }
            }
            page.setTotal(total);
            /*
             * 溢出总页数，设置最后一页
             */
            long pages = page.getPages();
            if (overflowCurrent && page.getCurrent() > pages) {
                // 设置最后一页
                page.setCurrent(pages);
            }
        } catch (Exception e) {
            throw ExceptionUtils.mpe("Error: Method queryTotal execution error of sql : \n %s \n", e, sql);
        }
	}
	
}
