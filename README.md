# ng-server
 
## 项目说明
- ng-*是一系列开源开发工具，包含vue动态表单系列 ng-form， 和一个轻量级、前后端分离Java快速开发平台ng-server（后端）和ng-ui（前端）
 
- 支持MySQL、Oracle、SQL Server、PostgreSQL等主流数据库（启动针对动态表单的示例只适配到mysql和postgresql）

- ng-ui地址： https://gitee.com/jjxliu306/ng-ui
- ng-server地址：https://gitee.com/jjxliu306/ng-server
 
<br>

**ng-form 动态表单系列**
- ng-form-element地址：https://gitee.com/jjxliu306/ng-form-element
- ng-form-iview地址：https://gitee.com/jjxliu306/ng-form-iview
- ng-form-elementplus地址：https://gitee.com/jjxliu306/ng-form-elementplus

## 项目特点 
- 友好的代码结构及注释，便于阅读及二次开发
- 实现前后端分离，通过token进行数据交互，前端再也不用关注后端技术
- 使用flyway，数据库升级和迁移完全不用执行sql，项目初始化启动只需要创建好数据库和用户即可，表结构和初始数据默认构建
- 灵活的权限控制，可控制到页面或按钮，满足绝大部分的权限需求
- 前端使用vue2.X，极大的提高了开发效率
- ORM框架使用mybatisplus，减少一半以上的数据库查询代码
- 引入quartz定时任务，可动态完成任务的添加、修改、删除、暂停、恢复及日志查看等功能
- 完善的三方平台对接接口，根据三方APP信息授权AK,SK作为令牌，极大的三方对接接口开发
- 引入Hibernate Validator校验框架，轻松实现后端校验
- 支持fastdfs，七牛云，minio，ftp，本地文件存储等多种文件存储方式
- 引入swagger文档支持，方便编写API接口文档
<br> 

## 项目结构
```
ng-server
│
├─ng-common 公共模块
│  ├─ng-common-core 核心公共依赖，提供缓存、全局异常、校验、excel等多个基础功能
│  ├─ng-common-fileupload 文件上传下载公共依赖（可移除）
│  ├─ng-common-log 应用访问日志记录依赖（可移除）
│ 
├─ng-config 配置信息
│ 
├─ng-main 启动入口
│
├─ng-modules 功能模块
│  ├─ng-modules-exterauth 三方app对接模块（可移除）
│  ├─ng-modules-form ng-form动态表单示例模块（可移除）
│  ├─ng-modules-quartz 定时任务模块（可移除）
│  └─ng-modules-stat 统计分析模块（可移除）
│ 
├─ng-sys 核心基础模块。包含用户、角色、菜单等基础信息配置和鉴权


```
<br> 
 

## 技术选型： 
- 核心框架：Spring Boot 2.4.8
- 安全框架：Apache Shiro 1.4.0 
- 持久层框架：MyBatisPlus 3.2.0
- 定时器：Quartz 2.3.0
- 数据库连接池：Druid 1.1.13
- 日志管理：SLF4J 2.17.0
<br> 


## 后端部署
- 通过git下载源码
- idea、eclipse需安装lombok插件，不然会提示找不到entity的get set方法
- 创建数据库ng-server，数据库编码为UTF-8（切记无需创建表,项目启动的时候会通过flyway自动创建表和初始化数据）
- 修改application-dev.yml，修改数据库链接信息,修改flyway配置信息（注意mysql和postgresql的locations是不一样的）
- Eclipse、IDEA中进入ng-main模块，运行NgApplication.java，则可启动项目 
- 启用后默认超管账号为: superAdmin,密码: admin。（超管账号存在与缓存中，不在数据库用户内）
- Swagger文档路径：http://localhost:8080/ng-server/swagger-ui.html

<br> 

## 前端部署
 - 本项目是前后端分离的，还需要部署前端，才能运行起来
 - 前端地址：https://gitee.com/jjxliu306/ng-ui 
 
 <br>
 
## 效果示例

**接口文档效果图：**
![swagger接口截图](images/swagger.png)

<br> <br> <br> 


**效果图：**
![输入图片说明](images/QQ图片20211231194821.png "在这里输入图片标题")
![输入图片说明](images/QQ图片20211231194932.png "在这里输入图片标题")
![输入图片说明](images/QQ图片20211231194947.png "在这里输入图片标题")
![输入图片说明](images/QQ图片20211231195014.png "在这里输入图片标题")

<br>
 