/**
 * @author lyf
 * @email jjxliu306@163.com
 * @date 2018年10月31日 下午11:46:07
 * 
 */
package com.ng.stat.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * @author lyf
 * @email jjxliu306@163.com
 * @date 2017年10月31日 下午11:46:07
 */
@Mapper
public interface StatDao {
	 

	@Select("select execute_sql(#{sqlId} , #{params , jdbcType=ARRAY, typeHandler=arrayTypeHandler})") 
	public String select(@Param("sqlId") String sqlId ,@Param("params") Object[] params);
	
	/**
	 * 调用函数返回 
	 * @param fun_name
	 * @param params
	 * @return
	 */
	@Select("select ${funName}(#{params, jdbcType=ARRAY, typeHandler=arrayTypeHandler})")
	public String selectFun(@Param("funName") String fun_name ,@Param("params") Object[] params);
	
}
