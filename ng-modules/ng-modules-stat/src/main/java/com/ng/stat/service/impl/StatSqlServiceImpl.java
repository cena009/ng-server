/**
 * @author lyf
 * @email jjxliu306@163.com
 * @date 2018年10月31日 下午11:50:52
 * 
 */
package com.ng.stat.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ng.common.utils.R;
import com.ng.stat.dao.StatDao;
import com.ng.stat.service.StatSqlService;

/**
 * @author lyf
 * @email jjxliu306@163.com
 * @date 2017年10月31日 下午11:50:52
 */
@Service
public class StatSqlServiceImpl implements StatSqlService {

	@Autowired
	private StatDao statDao;
	
	/* (non-Javadoc)
	 * @see com.haohang.livina.modules.stat.service.StatSqlService#select(java.lang.String, java.lang.Object[])
	 */
	@Override
	public R<Object> select(String sqlId, Object[] params) {
		// TODO Auto-generated method stub
		String stat = statDao.select(sqlId, params);
		if(stat != null) {
			JSONObject jObject = JSON.parseObject(stat);
			
			Integer code = jObject.getInteger("code");
			Object data = jObject.get("data");
			R<Object> r = R.ok();
			 
			r.setCode(code);
			r.setData(data);
		 
			 
			return r;
		}
		return R.error();
		
		
	}

	/* (non-Javadoc)
	 * @see com.haohang.livina.modules.stat.service.StatSqlService#selectByFunction(java.lang.String, java.lang.Object[])
	 */
	@Override
	public R<Object> selectByFunction(String function, Object[] params) {
		String stat = statDao.selectFun(function, params);
		if(stat != null) {
			Object jObject = JSON.parse(stat);
			 
			R<Object> r = R.ok();
			
			r.setCode(0);
			r.setData(jObject);
			 
			return r;
		}
		return R.error();
	}

	public String selectFunction(String function, Object[] params) {
		String stat = statDao.selectFun(function, params);
		 
		return stat;
	}
	
}
