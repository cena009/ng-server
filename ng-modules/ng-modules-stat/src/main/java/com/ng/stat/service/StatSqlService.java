/**
 * @author lyf
 * @email jjxliu306@163.com
 * @date 2018年10月31日 下午11:49:32
 * 
 */
package com.ng.stat.service;

import com.ng.common.utils.R;

/**
 *  统计查询入口
 * @author lyf
 * @email jjxliu306@163.com
 * @date 2017年10月31日 下午11:49:32
 */
public interface StatSqlService {

	/**
	 *  查询结果
	 * @param sqlId sys_sql id
	 * @param params 参数
	 * @return
	 */
	public R<Object> select(String sqlId , Object[] params);
	
	/**
	 * 调用函数返回查询结果
	 * @param function 函数名称
	 * @param params 参数数组
	 * @return
	 */
	public R<Object> selectByFunction(String function , Object[] params);
	
	/* (non-Javadoc)
	 * @see com.haohang.livina.modules.stat.service.StatSqlService#selectByFunction(java.lang.String, java.lang.Object[])
	 */
	 
	public String selectFunction(String function, Object[] params) ;
	
}
