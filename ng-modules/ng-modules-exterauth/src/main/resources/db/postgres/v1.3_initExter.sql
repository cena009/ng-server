create table sys_exter_app (
	id	varchar(32) primary key ,
	name	text not null , -- 外部应用名称
	remark 	text ,
	ak		varchar(255)   , -- 授权ak
	sk		varchar(255)  , -- 授权sk
	status	int			not null , -- 状态 1-启用 0-未启用
	username	varchar(255) not null , -- 登录名
	password	varchar(255) not null , -- 密码
	
	create_time	timestamp not null ,
	update_time timestamp ,
	create_id	varchar(32) ,
	update_id	varchar(32)


);
 
create table sys_exter_app_token (
	app_id	varchar(32) not null ,
	type	int default 1 , -- 登录类型 1-PC ,2-接口
	token	varchar(32) not null ,
	expire_time	timestamp not null , -- 过期时间
	create_time	timestamp not null -- 创建时间


);

-- 外部授权信息
create table sys_exter_perms (
	id	varchar(32) primary key ,
	name	varchar(32) not null ,-- 权限名称
	remark	text ,
	perms	varchar(255) -- 授权码


);


-- 应用授权中间表
create table sys_exter_app_perms(
	app_id	varchar(32) not null ,
	perm_id	varchar(32) not null ,
	unique(app_id , perm_id)
);



create table sys_exter_log (
  id bigserial primary key,
  appname varchar(255) ,
  operation varchar(255) ,
  method varchar(255) ,
  params varchar(5000) ,
  time int8 NOT NULL DEFAULT 0,
  ip varchar(64) ,
  create_date timestamp(6) DEFAULT now() 
)
