package com.ng.api.exterauth.form;

import javax.validation.constraints.NotBlank;

import com.ng.common.validator.group.AddGroup;
import com.ng.common.validator.group.UpdateGroup;

public class AppLoginForm {

	@NotBlank(message="Ak不能为空" , groups= {AddGroup.class , UpdateGroup.class})
	private String ak ;
	@NotBlank(message="Sk不能为空" , groups= {AddGroup.class , UpdateGroup.class})
	private String sk ;
	public String getAk() {
		return ak;
	}
	public void setAk(String ak) {
		this.ak = ak;
	}
	public String getSk() {
		return sk;
	}
	public void setSk(String sk) {
		this.sk = sk;
	}
	
	
	
}
