package com.ng.api.exterauth.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.ng.common.utils.R;
import com.ng.log.exterauth.annotation.ExterLog;
import com.ng.security.exterauth.annotation.AppPerm;
 

@RequestMapping("exapi/test")
@RestController
public class TestController {

	@PostMapping("/save")
	@AppPerm("test:save")
	@ExterLog("测试save")
	public R<Object> save1(@RequestBody TForm form) {
		
		System.out.println("save form: " + JSON.toJSONString(form));
		
		form.setName(form.getName() + "-save");
		
		return R.ok().setData(form);
	}
	
	
	@PostMapping("/update")
	@AppPerm("test:update")
	@ExterLog("测试update")
	public R<Object> update1(@RequestBody TForm form) {
		
		System.out.println("update form: " + JSON.toJSONString(form));
		
		form.setName(form.getName() + "-update");
		
		return R.ok().setData(form);
	}
	
}

 
class TForm{
	private String id ;
	private String name;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
	
}

