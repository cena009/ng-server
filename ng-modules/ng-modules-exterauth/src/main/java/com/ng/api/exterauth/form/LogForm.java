package com.ng.api.exterauth.form;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
public class LogForm {
	
	@JsonIgnore
	private String appname;
	
	
	private String operation;
	private Long startTime;
	private Long endTime;
	
	 
	private Integer pageIndex = 1 ;
	 
	private Integer pageSize = 10 ;
	
}
