package com.ng.security.exterauth.resolver;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import com.ng.common.utils.HttpContextUtils;
import com.ng.exterauth.entity.SysExterAppEntity;
import com.ng.exterauth.entity.SysExterAppTokenEntity;
import com.ng.exterauth.service.SysExterAppService;
import com.ng.exterauth.service.SysExterAppTokenService;
import com.ng.security.exterauth.annotation.LoginApp;
import com.ng.security.exterauth.interceptor.AuthorizationInterceptor;

/**
 * 有@LoginUser注解的方法参数，注入当前登录用户
 * @author lyf
 * @email liuyf@gs-softwares.com
 * @date 2017-03-23 22:02
 */
@Component
public class LoginAppHandlerMethodArgumentResolver implements HandlerMethodArgumentResolver {
	@Autowired
    private  SysExterAppService exterAppService ;
    
    @Autowired
    private  SysExterAppTokenService exterAppTokenService ;
     
  

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.getParameterType().isAssignableFrom(SysExterAppEntity.class)  && parameter.hasParameterAnnotation(LoginApp.class) ;
    }

    @Override
    public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer container,
                                  NativeWebRequest request, WebDataBinderFactory factory) throws Exception {
       
    	
    	
    	SysExterAppEntity app = null;
    	
    	// 判断request里是否已经由了user
    	Object app_ = request.getAttribute("app", RequestAttributes.SCOPE_REQUEST);
    	if(app_ != null && app_ instanceof SysExterAppEntity) {
    		app = (SysExterAppEntity) app_ ;
    	}
    	if(app == null) {
    		  
    		// 再次判断请求的request里是否包含token 尝试通过token获取user

    		String token = HttpContextUtils.getRequestToken((HttpServletRequest)request,AuthorizationInterceptor.TOKEN_HEADER);

    		if(!StringUtils.isBlank(token)) {

    			// user 存在说明还在有效期并且用户存在
    			 // user 存在说明还在有效期并且用户存在
    	        SysExterAppTokenEntity appToken = exterAppTokenService.queryByToken(token);
    	        
    			if(appToken != null  ) {
    				app = exterAppService.getById(appToken.getAppId());
        			 
    			}
    			 

    		}
            	 
           
    	}
    	 
       /*  
        if(user == null  ) {
        	return null;
			//throw new RRException("请先登录", HttpStatus.UNAUTHORIZED.value());
		}
        
       */
        
        return app;
     
    }
}
