package com.ng.security.exterauth.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * app perm验证
 * @author lyf
 * 
 * @date 2017/9/23 14:30
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface AppPerm {
	String value()  default ""; // 权限码
	//UserType role() default UserType.unknow; // 用户类型
}
