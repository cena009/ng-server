package com.ng.exterauth.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;



import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * ${comments}
 * 
 * @author lyf
 * @email jjxliu306@163.com
 * @date 2020-05-26 13:52:22
 */
@TableName("sys_exter_app_token")
@ApiModel("${comments}")
@Data
public class SysExterAppTokenEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * app_id
	 */
	@TableId(type=IdType.UUID)
	@TableField("app_id") 
	@ApiModelProperty("app_id")
	private String appId;
	/**
	 * create_time
	 */
	@TableField("create_time") 
	@ApiModelProperty("create_time")
	private Date createTime;
	/**
	 * expire_time
	 */
	@TableField("expire_time") 
	@ApiModelProperty("expire_time")
	private Date expireTime;
	/**
	 * token
	 */
	@TableField("token") 
	@ApiModelProperty("token")
	private String token;
	/**
	 * type
	 */
	@TableField("type") 
	@ApiModelProperty("type")
	private Integer type;
 
}
