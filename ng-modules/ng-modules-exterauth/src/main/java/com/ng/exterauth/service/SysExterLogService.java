
package com.ng.exterauth.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ng.api.exterauth.form.LogForm;
import com.ng.common.utils.PageUtils;
import com.ng.exterauth.entity.SysExterLogEntity;

/**
 * 系统日志
 * 
 * @author lyf
 * 
 * @date 2017-03-08 10:40:56
 */
public interface SysExterLogService extends IService<SysExterLogEntity> {

	PageUtils queryPage(LogForm form );

}
