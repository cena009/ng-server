package com.ng.exterauth.dao;

import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ng.exterauth.entity.SysExterAppTokenEntity;

/**
 * ${comments}
 * 
 * @author lyf
 * @email jjxliu306@163.com
 * @date 2020-05-26 13:52:22
 */
@Mapper
public interface SysExterAppTokenDao extends BaseMapper<SysExterAppTokenEntity> {
	
	/*推荐在此直接添加注解写sql,可读性比较好*/
	
}
