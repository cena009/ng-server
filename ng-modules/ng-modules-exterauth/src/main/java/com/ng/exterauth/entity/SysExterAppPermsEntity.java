package com.ng.exterauth.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * ${comments}
 * 
 * @author lyf
 * @email jjxliu306@163.com
 * @date 2020-05-25 14:06:47
 */
@TableName("sys_exter_app_perms")
@ApiModel("${comments}")
@Data
public class SysExterAppPermsEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * app_id
	 */
	@TableId(type=IdType.UUID)
	@TableField("app_id") 
	@ApiModelProperty("app_id")
	private String appId;
	/**
	 * perm_id
	 */
	@TableField("perm_id") 
	@ApiModelProperty("perm_id")
	private String permId;
 
}
