package com.ng.exterauth.service;
 
import com.baomidou.mybatisplus.extension.service.IService;
import com.ng.exterauth.entity.SysExterAppTokenEntity;
 

/**
 * ${comments}
 *
 * @author lyf
 * @email jjxliu306@163.com
 * @date 2020-05-26 13:52:22
 */
public interface SysExterAppTokenService extends IService<SysExterAppTokenEntity> {
 
	
	SysExterAppTokenEntity queryByToken(String token);
	
	/**
	 * 更新APP token
	 * @param appId
	 * @param type
	 * @return
	 */
	SysExterAppTokenEntity generateToken(String appId , String type);
}

