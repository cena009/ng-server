package com.ng.exterauth.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ng.common.utils.PageUtils;
import com.ng.common.utils.R;
import com.ng.common.validator.ValidatorUtils;
import com.ng.common.validator.group.AddGroup;
import com.ng.common.validator.group.UpdateGroup;
import com.ng.exterauth.entity.SysExterAppEntity;
import com.ng.exterauth.service.SysExterAppService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;

/**
 * 外部应用授权
 *
 * @author lyf
 * @email jjxliu306@163.com
 * @date 2020-05-25 14:06:47
 */
@RestController
@RequestMapping("exter/app")
@Api(value = "外部应用授权API", tags = { "外部应用授权API-通用" })
public class SysExterAppController {
	@Autowired
	private SysExterAppService sysExterAppService;

	/**
	 * 列表
	 */
	@GetMapping("/list")
	@ApiOperation("外部应用授权列表")
	@ApiResponse(code = 0, message = "查询成功", response = SysExterAppEntity.class)
	public R<PageUtils> list(SysExterAppEntity SysExterApp, PageUtils page) {

		PageUtils pageData = sysExterAppService.queryPage(SysExterApp, page);

		return R.ok(PageUtils.class).setData(pageData);
	}

	/**
	 * 信息
	 */
	@GetMapping("/info/{id}")
	@ApiOperation("根据ID获取外部应用授权信息")
	@ApiResponse(code = 0, message = "查询成功", response = SysExterAppEntity.class)
	public R<SysExterAppEntity> info(@PathVariable("id") String id) {
		SysExterAppEntity sysExterApp = sysExterAppService.getById(id);

		return R.ok(SysExterAppEntity.class).setData(sysExterApp);
	}

	/**
	 * 保存
	 */
	@PostMapping("/save")
	@RequiresPermissions("exter:app:save")
	@ApiOperation("保存外部应用授权信息")
	public R<Object> save(@RequestBody SysExterAppEntity sysExterApp) {
		ValidatorUtils.validateEntity(sysExterApp, AddGroup.class);
		sysExterAppService.save(sysExterApp);

		return R.ok();
	}

	/**
	 * 修改
	 */
	@PostMapping("/update")
	@RequiresPermissions("exter:app:update")
	@ApiOperation("修改外部应用授权信息")
	public R<Object> update(@RequestBody SysExterAppEntity sysExterApp) {
		ValidatorUtils.validateEntity(sysExterApp, UpdateGroup.class);
		sysExterAppService.updateById(sysExterApp);

		return R.ok();
	}

	@GetMapping("updateAk/{id}")
	@RequiresPermissions("exter:app:update")
	public R<Object> updateAk(@PathVariable String id) {
		
		
		String[] keys = sysExterAppService.generateAppKey(id);
		
		Map<String, Object> map = new HashMap<>();
		map.put("ak", keys[0]);
		map.put("sk", keys[1]);
		
		return R.ok().setData(map);
	}
	
	/**
	 * 删除
	 */
	@PostMapping("/delete")
	@RequiresPermissions("exter:app:delete")
	@ApiOperation("删除外部应用授权信息")
	public R<Object> delete(@RequestBody String[] ids) {
		sysExterAppService.removeByIds(Arrays.asList(ids));

		return R.ok();
	}

}
