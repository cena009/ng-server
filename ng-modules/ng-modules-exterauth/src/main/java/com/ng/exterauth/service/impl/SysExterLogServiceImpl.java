
package com.ng.exterauth.service.impl;

import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ng.api.exterauth.form.LogForm;
import com.ng.common.utils.PageUtils;
import com.ng.common.utils.Query;
import com.ng.exterauth.dao.SysExterLogDao;
import com.ng.exterauth.entity.SysExterLogEntity;
import com.ng.exterauth.service.SysExterLogService;


@Service
public class SysExterLogServiceImpl extends ServiceImpl<SysExterLogDao, SysExterLogEntity> implements SysExterLogService {

    @Override
    public PageUtils queryPage(LogForm form ) {
        
    	QueryWrapper<SysExterLogEntity> query = new QueryWrapper<SysExterLogEntity>();
    	 
    	
    	query.likeRight(StringUtils.isNotBlank(form.getAppname()), "appname" , form.getAppname());
    	query.like(StringUtils.isNotBlank(form.getOperation()), "operation" , form.getOperation());
    	if(form.getEndTime() != null && form.getEndTime() > 0) {
    		query.le("create_date", new Date(form.getEndTime()));
    	}
    	if(form.getStartTime() != null && form.getStartTime() > 0) {
    		query.ge("create_date", new Date(form.getStartTime()));
    	}
    	
    	Integer pageIndex = form.getPageIndex();
    	Integer pageSize = form.getPageSize();
    	if(pageIndex == null || pageIndex <= 0) {
    		pageIndex = 1;
    	}
    	if(pageSize == null || pageSize <= 0 || pageSize >= 200) {
    		pageSize = 10;
    	}
    	
    	Query<SysExterLogEntity>  pquery = new Query<>(pageIndex, pageSize/*, "create_date", false*/);
        
        IPage<SysExterLogEntity> page = this.page(
        		pquery.getPage(),query 
        		.orderByDesc("create_date")
        );

        return new PageUtils(page);
    }
}
