package com.ng.exterauth.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ng.common.validator.group.AddGroup;
import com.ng.common.validator.group.UpdateGroup;
import com.baomidou.mybatisplus.annotation.TableField;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;



import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Length;

import lombok.Data;

/**
 * ${comments}
 * 
 * @author lyf
 * @email jjxliu306@163.com
 * @date 2020-05-25 14:06:47
 */
@TableName("sys_exter_app")
@ApiModel("${comments}")
@Data
public class SysExterAppEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * ak
	 */
	@TableField("ak") 
	@ApiModelProperty("ak")
	private String ak;
	/**
	 * create_id
	 */
	@TableField("create_id") 
	@ApiModelProperty("create_id")
	private String createId;
	/**
	 * create_time
	 */
	@TableField("create_time") 
	@ApiModelProperty("create_time")
	private Date createTime;
	/**
	 * id
	 */
	@TableId(type=IdType.UUID)
	@TableField("id") 
	@ApiModelProperty("id")
	@NotBlank(message="ID不能为空" , groups= {UpdateGroup.class})
	private String id;
	/**
	 * name
	 */
	@TableField("name") 
	@ApiModelProperty("name")
	@NotBlank(message="名称不能为空" , groups= {AddGroup.class , UpdateGroup.class})
	@Length(min=1,max=200,message="名称最大长度不能超过200个字符"   , groups= {AddGroup.class , UpdateGroup.class})
	private String name;
	/**
	 * remark
	 */
	@TableField("remark") 
	@ApiModelProperty("remark")
	private String remark;
	/**
	 * sk
	 */
	@TableField("sk") 
	@ApiModelProperty("sk")
	private String sk;
	/**
	 * update_id
	 */
	@TableField("update_id") 
	@ApiModelProperty("update_id")
	private String updateId;
	/**
	 * update_time
	 */
	@TableField("update_time") 
	@ApiModelProperty("update_time")
	private Date updateTime;
	
	//status	int			not null , -- 状态 1-启用 0-未启用
	@TableField("status") 
	@ApiModelProperty("状态 1-启用 0-未启用")
	private Integer status = 1;
	
//	username	varchar(255) not null , -- 登录名
//	password	varchar(255) not null , -- 密码
	
	@TableField("username") 
	@ApiModelProperty("username")
	private String username;
	
	@TableField("password") 
	@ApiModelProperty("password")
	private String password;
	
	/**
	 * 授权列表
	 */
	@TableField(exist=false)
	private List<String> perms;
 
}
