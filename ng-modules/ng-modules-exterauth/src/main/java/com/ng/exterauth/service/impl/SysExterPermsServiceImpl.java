package com.ng.exterauth.service.impl;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ng.common.utils.PageUtils;
import com.ng.exterauth.dao.SysExterPermsDao;
import com.ng.exterauth.entity.SysExterPermsEntity;
import com.ng.exterauth.service.SysExterPermsService;


@Service("sysExterPermsService")
public class SysExterPermsServiceImpl extends ServiceImpl<SysExterPermsDao, SysExterPermsEntity> implements SysExterPermsService {

    @Override
    public PageUtils queryPage(SysExterPermsEntity SysExterPerms , PageUtils page ) {
        
        IPage<SysExterPermsEntity> ipage = this.page(
                page.getPage(SysExterPermsEntity.class),
                new QueryWrapper<SysExterPermsEntity>()
        );

        return new PageUtils(ipage);
    }

	@Override
	public Set<String> queryByApp(String appId) {
		
		Set<String> perms = baseMapper.queryByAppId(appId);
		
		// perms 拆解
		Set<String> ps = new HashSet<>();
		for(String p : perms) {
			if(StringUtils.isBlank(p))  continue ;
			String[] ss = p.split(",");
			
			if(ss == null || ss.length == 0) continue ;
			
			for(String s : ss) {
				ps.add(s);
			}
			
		}
		
		return ps;
	}

}
