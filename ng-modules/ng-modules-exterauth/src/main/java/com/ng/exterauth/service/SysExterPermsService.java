package com.ng.exterauth.service;
 
import java.util.Set;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ng.common.utils.PageUtils;
import com.ng.exterauth.entity.SysExterPermsEntity;
 

/**
 * 三方应用调用权限服务接口
 *
 * @author lyf
 * @email jjxliu306@163.com
 * @date 2020-05-25 14:06:47
 */
public interface SysExterPermsService extends IService<SysExterPermsEntity> {

    PageUtils queryPage(SysExterPermsEntity SysExterPerms , PageUtils page );
    
    
    Set<String> queryByApp(String appId);
    
}

