package com.ng.exterauth.controller;

import java.util.Arrays;
import java.util.List;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ng.common.utils.PageUtils;
import com.ng.common.utils.R;
import com.ng.common.validator.ValidatorUtils;
import com.ng.common.validator.group.AddGroup;
import com.ng.common.validator.group.UpdateGroup;
import com.ng.exterauth.entity.SysExterPermsEntity;
import com.ng.exterauth.service.SysExterPermsService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;

/**
 * 三方应用调用权限
 *
 * @author lyf
 * @email jjxliu306@163.com
 * @date 2020-05-25 14:06:47
 */
@RestController
@RequestMapping("exter/perms")
@Api(value = "三方应用调用权限API", tags = { "三方应用调用权限API-通用" })
public class SysExterPermsController {
	@Autowired
	private SysExterPermsService sysExterPermsService;

	/**
	 * 列表
	 */
	@GetMapping("/list")
	@RequiresPermissions("exter:perms:list")
	@ApiOperation("三方应用调用权限列表-分页")
	@ApiResponse(code = 0, message = "查询成功", response = SysExterPermsEntity.class)
	public R<PageUtils> list(SysExterPermsEntity SysExterPerms, PageUtils page) {

		PageUtils pageData = sysExterPermsService.queryPage(SysExterPerms, page);

		return R.ok(PageUtils.class).setData(pageData);
	}
	
	/**
	 * 列表
	 */
	@SuppressWarnings("rawtypes")
	@GetMapping("/all")
	@RequiresPermissions("exter:perms:list")
	@ApiOperation("三方应用调用权限列表-全部")
	@ApiResponse(code = 0, message = "查询成功", response = SysExterPermsEntity.class)
	public R<List> listAll() {

		List<SysExterPermsEntity> all = sysExterPermsService.list();
		  
		return R.ok(List.class).setData(all);
	}


	/**
	 * 信息
	 */
	@GetMapping("/info/{id}")
	@RequiresPermissions("exter:perms:info")
	@ApiOperation("根据ID获取三方应用调用权限信息")
	@ApiResponse(code = 0, message = "查询成功", response = SysExterPermsEntity.class)
	public R<SysExterPermsEntity> info(@PathVariable("id") String id) {
		SysExterPermsEntity sysExterPerms = sysExterPermsService.getById(id);

		return R.ok(SysExterPermsEntity.class).setData(sysExterPerms);
	}

	/**
	 * 保存
	 */
	@PostMapping("/save")
	@RequiresPermissions("exter:perms:save")
	@ApiOperation("保存$三方应用调用权限信息")
	public R<Object> save(@RequestBody SysExterPermsEntity sysExterPerms) {
		ValidatorUtils.validateEntity(sysExterPerms, AddGroup.class);
		sysExterPermsService.save(sysExterPerms);

		return R.ok();
	}

	/**
	 * 修改
	 */
	@PostMapping("/update")
	@RequiresPermissions("exter:perms:update")
	@ApiOperation("修改三方应用调用权限信息")
	public R<Object> update(@RequestBody SysExterPermsEntity sysExterPerms) {
		
		ValidatorUtils.validateEntity(sysExterPerms, UpdateGroup.class);
		
		sysExterPermsService.updateById(sysExterPerms);

		return R.ok();
	}

	/**
	 * 删除
	 */
	@PostMapping("/delete")
	@RequiresPermissions("exter:perms:delete")
	@ApiOperation("删除三方应用调用权限信息")
	public R<Object> delete(@RequestBody String[] ids) {
		sysExterPermsService.removeByIds(Arrays.asList(ids));

		return R.ok();
	}

}
