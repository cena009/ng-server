package com.ng.exterauth.dao;

import java.util.Set;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ng.exterauth.entity.SysExterPermsEntity;

/**
 * 三方应用调用权限DAO
 * 
 * @author lyf
 * @email jjxliu306@163.com
 * @date 2020-05-25 14:06:47
 */
@Mapper
public interface SysExterPermsDao extends BaseMapper<SysExterPermsEntity> {
	
	/*推荐在此直接添加注解写sql,可读性比较好*/
	@Select("select t1.perms from sys_exter_perms as t1 , sys_exter_app_perms as t2 "
			+ " where t1.id = t2.perm_id and t2.app_id = #{appId}")
	Set<String> queryByAppId(@Param("appId") String appId);
	
}
