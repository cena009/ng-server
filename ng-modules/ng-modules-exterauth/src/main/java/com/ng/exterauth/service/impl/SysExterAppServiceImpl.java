package com.ng.exterauth.service.impl;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ng.common.utils.MD5Utils;
import com.ng.common.utils.PageUtils;
import com.ng.exterauth.dao.SysExterAppDao;
import com.ng.exterauth.entity.SysExterAppEntity;
import com.ng.exterauth.entity.SysExterAppPermsEntity;
import com.ng.exterauth.service.SysExterAppPermsService;
import com.ng.exterauth.service.SysExterAppService;


@Service("sysExterAppService")
public class SysExterAppServiceImpl extends ServiceImpl<SysExterAppDao, SysExterAppEntity> implements SysExterAppService {

	@Autowired
	private SysExterAppPermsService appPermsService ;
	
    @Override
    public PageUtils queryPage(SysExterAppEntity SysExterApp , PageUtils page ) {
        
        IPage<SysExterAppEntity> ipage = this.page(
                page.getPage(SysExterAppEntity.class),
                new QueryWrapper<SysExterAppEntity>()
        );

        return new PageUtils(ipage);
    }
    
    @Override
    public SysExterAppEntity getById(Serializable id) {
    	// TODO Auto-generated method stub
    	SysExterAppEntity entity = super.getById(id);
    	
    	// 关联查询对应的权限信息
    	List<SysExterAppPermsEntity> perms = appPermsService.queryByAppId(id.toString());
    	
    	if(perms != null) {
    		List<String> ps = perms.stream().map(t -> t.getPermId()).collect(Collectors.toList());
    		entity.setPerms(ps);
    	}
    	
    	return entity ;
    }
    
    
    @Override
    public boolean save(SysExterAppEntity entity) {
    	// TODO Auto-generated method stub
    	entity.setCreateTime(new Date());
    	
    	
    	boolean save = super.save(entity);
    	
    	//更新权限信息 
    	if(entity.getPerms() != null)
    		appPermsService.insertAll(entity.getId(), entity.getPerms());
    	
    	//默认生成ak sk
    	generateAppKey(entity.getId());
    	 
    	return save ;
    }
    
    @Override
    public boolean updateById(SysExterAppEntity entity) {
    	// TODO Auto-generated method stub
    	entity.setUpdateTime(new Date());
    	boolean update = super.updateById(entity);
    	
    	
    	//更新权限信息 
    	appPermsService.deleteByAppId(entity.getId());
    	
    	appPermsService.insertAll(entity.getId(), entity.getPerms());
    	
    	return update ;
    }
    
    
    @Override
    public boolean removeById(Serializable id) {
    	// TODO Auto-generated method stub
    	boolean remove = super.removeById(id);
    	
    	//更新权限信息 
    	appPermsService.deleteByAppId(id.toString());
    	
    	return remove ;
    }

	@Override
	public String[] generateAppKey(String appId) {
		
		// 生成 ak和 sk sk通过md5产生
		String ak = RandomStringUtils.randomAlphanumeric(20);
		String sk = MD5Utils.md5Pass(appId, ak);
		
		// 更新
		SysExterAppEntity entity = new SysExterAppEntity();
		entity.setId(appId);
		entity.setAk(ak);
		entity.setSk(sk);
		
		super.updateById(entity);
		
		return new String[] {ak , sk};
	}

	@Override
	public SysExterAppEntity queryByAkSk(String ak, String sk) {
		
		QueryWrapper<SysExterAppEntity> query = new QueryWrapper<>();
		query.eq("ak", ak);
		query.eq("sk", sk);
		
		return getOne(query);
	}

}
