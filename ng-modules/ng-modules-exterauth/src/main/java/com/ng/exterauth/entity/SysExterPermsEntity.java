package com.ng.exterauth.entity;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Length;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ng.common.validator.group.AddGroup;
import com.ng.common.validator.group.UpdateGroup;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 三方应用调用权限实体
 * 
 * @author lyf
 * @email jjxliu306@163.com
 * @date 2020-05-25 14:06:47
 */
@TableName("sys_exter_perms")
@ApiModel("${comments}")
@Data
public class SysExterPermsEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId(type=IdType.UUID)
	@TableField("id") 
	@ApiModelProperty("id")
	private String id;
	/**
	 * name
	 */
	@TableField("name") 
	@ApiModelProperty("name")
	@NotBlank(message="名称不能为空" , groups= {AddGroup.class , UpdateGroup.class})
	@Length(min=1,max=200,message="名称最大长度不能超过200个字符"   , groups= {AddGroup.class , UpdateGroup.class})
	private String name;
	/**
	 * perms
	 */
	@TableField("perms") 
	@ApiModelProperty("perms")
	@NotBlank(message="权限码不能为空" , groups= {AddGroup.class , UpdateGroup.class})
	@Length(min=1,max=200,message="权限码最大长度不能超过200个字符"   , groups= {AddGroup.class , UpdateGroup.class})
	private String perms;
	/**
	 * remark
	 */
	@TableField("remark") 
	@ApiModelProperty("remark")
	private String remark;
 
}
