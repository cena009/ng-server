package com.ng.exterauth.controller;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ng.api.exterauth.form.LogForm;
import com.ng.common.utils.PageUtils;
import com.ng.common.utils.R;
import com.ng.exterauth.service.SysExterLogService;

import io.swagger.annotations.Api;

/**
 * 外部应用访问日志记录
 *
 * @author lyf
 * @email jjxliu306@163.com
 * @date 2020-05-25 14:06:47
 */
@RestController
@RequestMapping("exter/log")
@Api(value = "外部应用访问日志记录API", tags = { "外部应用访问日志记录-通用" })
public class SysExterLogController {
	@Autowired
	private SysExterLogService logService ;

	@GetMapping("/list")
	@RequiresPermissions("exter:log")
	public R<Object> logs(LogForm form) {
		 
		
		PageUtils pu = logService.queryPage(form);
		
		
		return R.ok().setData(pu);
	}
	
	
}
