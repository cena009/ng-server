package com.ng.exterauth.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ng.exterauth.dao.SysExterAppPermsDao;
import com.ng.exterauth.entity.SysExterAppPermsEntity;
import com.ng.exterauth.service.SysExterAppPermsService;


@Service("sysExterAppPermsService")
public class SysExterAppPermsServiceImpl extends ServiceImpl<SysExterAppPermsDao, SysExterAppPermsEntity> implements SysExterAppPermsService {

	@Override
	public List<SysExterAppPermsEntity> queryByAppId(String appId) {
		
		QueryWrapper<SysExterAppPermsEntity> query = new QueryWrapper<>();
		query.eq("app_id", appId);
		
		return list(query);
	}

	@Override
	public boolean deleteByAppId(String appId) {
		
		QueryWrapper<SysExterAppPermsEntity> query = new QueryWrapper<>();
		query.eq("app_id", appId);
		
		return remove(query);
	}

	@Override
	public boolean insertAll(String appId, List<String> perms) {
		
		List<SysExterAppPermsEntity> batch = new ArrayList<SysExterAppPermsEntity>();
		if(perms != null)
		for(String p : perms) {
			SysExterAppPermsEntity entity = new SysExterAppPermsEntity();
			entity.setAppId(appId);
			entity.setPermId(p);
			
			batch.add(entity);
		}
		
		 
		return saveBatch(batch);
	}

   

}
