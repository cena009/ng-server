package com.ng.exterauth.service;
 
import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ng.exterauth.entity.SysExterAppPermsEntity;
 

/**
 * ${comments}
 *
 * @author lyf
 * @email jjxliu306@163.com
 * @date 2020-05-25 14:06:47
 */
public interface SysExterAppPermsService extends IService<SysExterAppPermsEntity> {

	/**
	 * 查询外部应用的授权的权限列表
	 * @param appId 应用ID
	 * @return
	 */
    List<SysExterAppPermsEntity> queryByAppId(String appId);
    
    /**
     * 删除外部应用的授权信息
     * @param appId 应用ID
     * @return
     */
    boolean deleteByAppId(String appId);
    
    /**
     * 批量插入外部应用权限
     * @param appId 应用ID
     * @param perms 权限列表
     * @return
     */
    boolean insertAll(String appId , List<String> perms);
    
}

