package com.ng.exterauth.service;
 
import com.baomidou.mybatisplus.extension.service.IService;
import com.ng.common.utils.PageUtils;
import com.ng.exterauth.entity.SysExterAppEntity;
 

/**
 * ${comments}
 *
 * @author lyf
 * @email jjxliu306@163.com
 * @date 2020-05-25 14:06:47
 */
public interface SysExterAppService extends IService<SysExterAppEntity> {

    PageUtils queryPage(SysExterAppEntity SysExterApp , PageUtils page );
    
    /**
     * 生成或者更新回填app的ak和sk
     * @param appId 三方应用ID
     * @return
     */
    String[] generateAppKey(String appId);
    
    /**
     * 根据AK 和SK 查询
     * @param ak
     * @param sk
     * @return
     */
    SysExterAppEntity queryByAkSk(String ak , String sk);
    
}

