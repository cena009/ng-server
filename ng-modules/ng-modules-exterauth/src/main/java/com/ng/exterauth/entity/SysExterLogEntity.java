

package com.ng.exterauth.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;


/**
 * 系统日志
 * 
 * @author lyf
 * 
 * @date 2017-03-08 10:40:56
 */
@TableName("sys_exter_log")
@Data
public class SysExterLogEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	@TableId
	private Long id;
	//用户名
	private String appname;
	//用户操作
	private String operation;
	//请求方法
	@JsonIgnore
	private String method;
	//请求参数
	private String params;
	//执行时长(毫秒)
	private Long time;
	//IP地址
	private String ip;
	//创建时间
	private Date createDate;
  
	
}
