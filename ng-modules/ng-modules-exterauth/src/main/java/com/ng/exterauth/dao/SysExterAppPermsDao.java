package com.ng.exterauth.dao;

import org.apache.ibatis.annotations.Mapper; 
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ng.exterauth.entity.SysExterAppPermsEntity;

/**
 * ${comments}
 * 
 * @author lyf
 * @email jjxliu306@163.com
 * @date 2020-05-25 14:06:47
 */
@Mapper
public interface SysExterAppPermsDao extends BaseMapper<SysExterAppPermsEntity> {
	
	/*推荐在此直接添加注解写sql,可读性比较好*/
	
}
