

-- Table: qrtz_blob_triggers

-- DROP TABLE qrtz_blob_triggers;

CREATE TABLE qrtz_blob_triggers
(
    sched_name varchar(120)  NOT NULL,
    trigger_name varchar(200)  NOT NULL,
    trigger_group varchar(200)  NOT NULL,
    blob_data bytea
);

-- Table: qrtz_calendars

-- DROP TABLE qrtz_calendars;

CREATE TABLE qrtz_calendars
(
    sched_name varchar(120)  NOT NULL,
    calendar_name varchar(200)  NOT NULL,
    calendar bytea NOT NULL
) ;


-- Table: qrtz_cron_triggers

-- DROP TABLE qrtz_cron_triggers;

CREATE TABLE qrtz_cron_triggers
(
    sched_name varchar(120)  NOT NULL,
    trigger_name varchar(200)  NOT NULL,
    trigger_group varchar(200)  NOT NULL,
    cron_expression varchar(120)  NOT NULL,
    time_zone_id varchar(80) 
) ;

-- Table: qrtz_fired_triggers

-- DROP TABLE qrtz_fired_triggers;

CREATE TABLE qrtz_fired_triggers
(
    sched_name varchar(120)  NOT NULL,
    entry_id varchar(95)  NOT NULL,
    trigger_name varchar(200)  NOT NULL,
    trigger_group varchar(200)  NOT NULL,
    instance_name varchar(200)  NOT NULL,
    fired_time bigint NOT NULL,
    sched_time bigint NOT NULL,
    priority integer NOT NULL,
    state varchar(16)  NOT NULL,
    job_name varchar(200) ,
    job_group varchar(200) ,
    is_nonconcurrent boolean,
    requests_recovery boolean
);
-- Index: idx_qrtz_ft_inst_job_req_rcvry

-- DROP INDEX idx_qrtz_ft_inst_job_req_rcvry;

CREATE INDEX idx_qrtz_ft_inst_job_req_rcvry
    ON qrtz_fired_triggers USING btree
    (sched_name  ASC NULLS LAST, instance_name  ASC NULLS LAST, requests_recovery ASC NULLS LAST)
    TABLESPACE pg_default;
-- Index: idx_qrtz_ft_j_g

-- DROP INDEX idx_qrtz_ft_j_g;

CREATE INDEX idx_qrtz_ft_j_g
    ON qrtz_fired_triggers USING btree
    (sched_name  ASC NULLS LAST, job_name  ASC NULLS LAST, job_group  ASC NULLS LAST)
    TABLESPACE pg_default;
-- Index: idx_qrtz_ft_jg

-- DROP INDEX idx_qrtz_ft_jg;

CREATE INDEX idx_qrtz_ft_jg
    ON qrtz_fired_triggers USING btree
    (sched_name  ASC NULLS LAST, job_group  ASC NULLS LAST)
    TABLESPACE pg_default;
-- Index: idx_qrtz_ft_t_g

-- DROP INDEX idx_qrtz_ft_t_g;

CREATE INDEX idx_qrtz_ft_t_g
    ON qrtz_fired_triggers USING btree
    (sched_name  ASC NULLS LAST, trigger_name  ASC NULLS LAST, trigger_group  ASC NULLS LAST)
    TABLESPACE pg_default;
-- Index: idx_qrtz_ft_tg

-- DROP INDEX idx_qrtz_ft_tg;

CREATE INDEX idx_qrtz_ft_tg
    ON qrtz_fired_triggers USING btree
    (sched_name  ASC NULLS LAST, trigger_group  ASC NULLS LAST)
    TABLESPACE pg_default;
-- Index: idx_qrtz_ft_trig_inst_name

-- DROP INDEX idx_qrtz_ft_trig_inst_name;

CREATE INDEX idx_qrtz_ft_trig_inst_name
    ON qrtz_fired_triggers USING btree
    (sched_name  ASC NULLS LAST, instance_name  ASC NULLS LAST)
    TABLESPACE pg_default;
    
-- Table: qrtz_job_details

-- DROP TABLE qrtz_job_details;

CREATE TABLE qrtz_job_details
(
    sched_name varchar(120)  NOT NULL,
    job_name varchar(200)  NOT NULL,
    job_group varchar(200)  NOT NULL,
    description varchar(250) ,
    job_class_name varchar(250)  NOT NULL,
    is_durable boolean NOT NULL,
    is_nonconcurrent boolean NOT NULL,
    is_update_data boolean NOT NULL,
    requests_recovery boolean NOT NULL,
    job_data bytea,
    CONSTRAINT qrtz_job_details_pkey PRIMARY KEY (sched_name, job_name, job_group)
);
-- Index: idx_qrtz_j_grp

-- DROP INDEX idx_qrtz_j_grp;

CREATE INDEX idx_qrtz_j_grp
    ON qrtz_job_details USING btree
    (sched_name  ASC NULLS LAST, job_group  ASC NULLS LAST)
    TABLESPACE pg_default;
-- Index: idx_qrtz_j_req_recovery

-- DROP INDEX idx_qrtz_j_req_recovery;

CREATE INDEX idx_qrtz_j_req_recovery
    ON qrtz_job_details USING btree
    (sched_name  ASC NULLS LAST, requests_recovery ASC NULLS LAST)
    TABLESPACE pg_default;
    
-- Table: qrtz_locks

-- DROP TABLE qrtz_locks;

CREATE TABLE qrtz_locks
(
    sched_name varchar(120)  NOT NULL,
    lock_name varchar(40)  NOT NULL
) ;


-- Table: qrtz_paused_trigger_grps

-- DROP TABLE qrtz_paused_trigger_grps;

CREATE TABLE qrtz_paused_trigger_grps
(
    sched_name varchar(120)  NOT NULL,
    trigger_group varchar(200)  NOT NULL
);    

-- Table: qrtz_scheduler_state

-- DROP TABLE qrtz_scheduler_state;

CREATE TABLE qrtz_scheduler_state
(
    sched_name varchar(120)  NOT NULL,
    instance_name varchar(200)  NOT NULL,
    last_checkin_time bigint NOT NULL,
    checkin_interval bigint NOT NULL
);


-- Table: qrtz_simple_triggers

-- DROP TABLE qrtz_simple_triggers;

CREATE TABLE qrtz_simple_triggers
(
    sched_name varchar(120)  NOT NULL,
    trigger_name varchar(200)  NOT NULL,
    trigger_group varchar(200)  NOT NULL,
    repeat_count bigint NOT NULL,
    repeat_interval bigint NOT NULL,
    times_triggered bigint NOT NULL
) ;


-- Table: qrtz_simprop_triggers

-- DROP TABLE qrtz_simprop_triggers;

CREATE TABLE qrtz_simprop_triggers
(
    sched_name varchar(120)  NOT NULL,
    trigger_name varchar(200)  NOT NULL,
    trigger_group varchar(200)  NOT NULL,
    str_prop_1 varchar(512) ,
    str_prop_2 varchar(512) ,
    str_prop_3 varchar(512) ,
    int_prop_1 integer,
    int_prop_2 integer,
    long_prop_1 bigint,
    long_prop_2 bigint,
    dec_prop_1 numeric(13,4),
    dec_prop_2 numeric(13,4),
    bool_prop_1 boolean,
    bool_prop_2 boolean
) ;


-- Table: qrtz_triggers

-- DROP TABLE qrtz_triggers;

CREATE TABLE qrtz_triggers
(
    sched_name varchar(120)  NOT NULL,
    trigger_name varchar(200)  NOT NULL,
    trigger_group varchar(200)  NOT NULL,
    job_name varchar(200)  NOT NULL,
    job_group varchar(200)  NOT NULL,
    description varchar(250) ,
    next_fire_time bigint,
    prev_fire_time bigint,
    priority integer,
    trigger_state varchar(16)  NOT NULL,
    trigger_type varchar(8)  NOT NULL,
    start_time bigint NOT NULL,
    end_time bigint,
    calendar_name varchar(200) ,
    misfire_instr smallint,
    job_data bytea
) ;

 
-- Index: idx_qrtz_t_c

-- DROP INDEX idx_qrtz_t_c;

CREATE INDEX idx_qrtz_t_c
    ON qrtz_triggers USING btree
    (sched_name  ASC NULLS LAST, calendar_name  ASC NULLS LAST)
    TABLESPACE pg_default;
-- Index: idx_qrtz_t_g

-- DROP INDEX idx_qrtz_t_g;

CREATE INDEX idx_qrtz_t_g
    ON qrtz_triggers USING btree
    (sched_name  ASC NULLS LAST, trigger_group  ASC NULLS LAST)
    TABLESPACE pg_default;
-- Index: idx_qrtz_t_j

-- DROP INDEX idx_qrtz_t_j;

CREATE INDEX idx_qrtz_t_j
    ON qrtz_triggers USING btree
    (sched_name  ASC NULLS LAST, job_name  ASC NULLS LAST, job_group  ASC NULLS LAST)
    TABLESPACE pg_default;
-- Index: idx_qrtz_t_jg

-- DROP INDEX idx_qrtz_t_jg;

CREATE INDEX idx_qrtz_t_jg
    ON qrtz_triggers USING btree
    (sched_name  ASC NULLS LAST, job_group  ASC NULLS LAST)
    TABLESPACE pg_default;
-- Index: idx_qrtz_t_n_g_state

-- DROP INDEX idx_qrtz_t_n_g_state;

CREATE INDEX idx_qrtz_t_n_g_state
    ON qrtz_triggers USING btree
    (sched_name  ASC NULLS LAST, trigger_group  ASC NULLS LAST, trigger_state  ASC NULLS LAST)
    TABLESPACE pg_default;
-- Index: idx_qrtz_t_n_state

-- DROP INDEX idx_qrtz_t_n_state;

CREATE INDEX idx_qrtz_t_n_state
    ON qrtz_triggers USING btree
    (sched_name  ASC NULLS LAST, trigger_name  ASC NULLS LAST, trigger_group  ASC NULLS LAST, trigger_state  ASC NULLS LAST)
    TABLESPACE pg_default;
-- Index: idx_qrtz_t_next_fire_time

-- DROP INDEX idx_qrtz_t_next_fire_time;

CREATE INDEX idx_qrtz_t_next_fire_time
    ON qrtz_triggers USING btree
    (sched_name  ASC NULLS LAST, next_fire_time ASC NULLS LAST)
    TABLESPACE pg_default;
-- Index: idx_qrtz_t_nft_misfire

-- DROP INDEX idx_qrtz_t_nft_misfire;

CREATE INDEX idx_qrtz_t_nft_misfire
    ON qrtz_triggers USING btree
    (sched_name  ASC NULLS LAST, misfire_instr ASC NULLS LAST, next_fire_time ASC NULLS LAST)
    TABLESPACE pg_default;
-- Index: idx_qrtz_t_nft_st

-- DROP INDEX idx_qrtz_t_nft_st;

CREATE INDEX idx_qrtz_t_nft_st
    ON qrtz_triggers USING btree
    (sched_name  ASC NULLS LAST, trigger_state  ASC NULLS LAST, next_fire_time ASC NULLS LAST)
    TABLESPACE pg_default;
-- Index: idx_qrtz_t_nft_st_misfire

-- DROP INDEX idx_qrtz_t_nft_st_misfire;

CREATE INDEX idx_qrtz_t_nft_st_misfire
    ON qrtz_triggers USING btree
    (sched_name  ASC NULLS LAST, misfire_instr ASC NULLS LAST, next_fire_time ASC NULLS LAST, trigger_state  ASC NULLS LAST)
    TABLESPACE pg_default;
-- Index: idx_qrtz_t_nft_st_misfire_grp

-- DROP INDEX idx_qrtz_t_nft_st_misfire_grp;

CREATE INDEX idx_qrtz_t_nft_st_misfire_grp
    ON qrtz_triggers USING btree
    (sched_name  ASC NULLS LAST, misfire_instr ASC NULLS LAST, next_fire_time ASC NULLS LAST, trigger_group  ASC NULLS LAST, trigger_state  ASC NULLS LAST)
    TABLESPACE pg_default;
-- Index: idx_qrtz_t_state

-- DROP INDEX idx_qrtz_t_state;

CREATE INDEX idx_qrtz_t_state
    ON qrtz_triggers USING btree
    (sched_name  ASC NULLS LAST, trigger_state  ASC NULLS LAST)
    TABLESPACE pg_default;
    
    
    
-- Table: schedule_job

-- DROP TABLE schedule_job;

CREATE TABLE schedule_job
(
    job_id varchar(32) NOT NULL ,
    bean_name varchar(200) ,
    method_name varchar(100) ,
    params varchar(2000) ,
    cron_expression varchar(100) ,
    status integer,
    remark varchar(255) ,
    create_time timestamp(6) without time zone,
    CONSTRAINT schedule_job_pkey PRIMARY KEY (job_id)
) ;


-- Table: schedule_job_log

-- DROP TABLE schedule_job_log;

CREATE TABLE schedule_job_log
(
    log_id bigserial NOT NULL,
    job_id varchar(32),
    bean_name varchar(200) ,
    method_name varchar(100) ,
    params varchar(2000) ,
    status integer NOT NULL,
    error varchar(2000) ,
    times integer NOT NULL,
    create_time timestamp(6) without time zone,
    CONSTRAINT schedule_job_log_pkey PRIMARY KEY (log_id)
) ;

create index on schedule_job_log(job_id);


