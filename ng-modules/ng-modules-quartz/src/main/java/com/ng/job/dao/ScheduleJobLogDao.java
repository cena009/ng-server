 
package com.ng.job.dao;

import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ng.job.entity.ScheduleJobLogEntity;

/**
 * 定时任务日志
 *
 * @author lyf
 */
@Mapper
public interface ScheduleJobLogDao extends BaseMapper<ScheduleJobLogEntity> {
	
}
