 
package com.ng.job.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ng.common.utils.PageUtils;
import com.ng.common.utils.Query;
import com.ng.job.dao.ScheduleJobLogDao;
import com.ng.job.entity.ScheduleJobLogEntity;
import com.ng.job.service.ScheduleJobLogService;

@Service("scheduleJobLogService")
public class ScheduleJobLogServiceImpl extends ServiceImpl<ScheduleJobLogDao, ScheduleJobLogEntity> implements ScheduleJobLogService {

	@Override
	public PageUtils queryPage(ScheduleJobLogEntity entity , PageUtils pageUtils) {
		String jobId = entity.getJobId();

		IPage<ScheduleJobLogEntity> page = this.page(
				new Query<ScheduleJobLogEntity>(pageUtils).getPage(),
				new QueryWrapper<ScheduleJobLogEntity>().like(jobId != null,"job_id", jobId)
				.orderByDesc("log_id")
		);

		return new PageUtils(page);
	}

}
