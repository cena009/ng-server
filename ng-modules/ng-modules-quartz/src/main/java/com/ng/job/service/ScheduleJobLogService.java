 
package com.ng.job.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ng.common.utils.PageUtils;
import com.ng.job.entity.ScheduleJobLogEntity;

/**
 * 定时任务日志
 *
 * @author lyf
 */
public interface ScheduleJobLogService extends IService<ScheduleJobLogEntity> {

	PageUtils queryPage(ScheduleJobLogEntity entity , PageUtils pageUtils);
	
}
