 
package com.ng.job.controller;

import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ng.common.log.annotation.SysLog;
import com.ng.common.log.annotation.SysModule;
import com.ng.common.utils.PageUtils;
import com.ng.common.utils.R;
import com.ng.common.validator.ValidatorUtils;
import com.ng.job.entity.ScheduleJobEntity;
import com.ng.job.service.ScheduleJobService;

/**
 * 定时任务
 *
 * @author lyf
 */
@RestController
@RequestMapping("/job/schedule")
public class ScheduleJobController {
	@Autowired
	private ScheduleJobService scheduleJobService;
	
	/**
	 * 定时任务列表
	 */
	@GetMapping("/list")
	@RequiresPermissions("job:schedule")
	public R<PageUtils> list(@RequestParam Map<String, Object> params){
		PageUtils page = scheduleJobService.queryPage(params);

		return R.ok(PageUtils.class).setData( page);
	}
	
	/**
	 * 定时任务信息
	 */
	@GetMapping("/info/{jobId}")
	@RequiresPermissions("job:schedule")
	public R<ScheduleJobEntity> info(@PathVariable("jobId") Long jobId){
		ScheduleJobEntity schedule = scheduleJobService.getById(jobId);
		
		return R.ok(ScheduleJobEntity.class).setData( schedule);
	}
	
	/**
	 * 保存定时任务
	 */
	@SysLog(value="保存定时任务",system = SysModule.sys)
	@PostMapping("/save")
	@RequiresPermissions("job:schedule")
	public R<Object> save(@RequestBody ScheduleJobEntity scheduleJob){
		ValidatorUtils.validateEntity(scheduleJob);
		
		scheduleJobService.save(scheduleJob);
		
		return R.ok();
	}
	
	/**
	 * 修改定时任务
	 */
	@SysLog(value="修改定时任务",system = SysModule.sys)
	@PostMapping("/update")
	@RequiresPermissions("job:schedule")
	public R<Object> update(@RequestBody ScheduleJobEntity scheduleJob){
		ValidatorUtils.validateEntity(scheduleJob);
				
		scheduleJobService.update(scheduleJob);
		
		return R.ok();
	}
	
	/**
	 * 删除定时任务
	 */
	@SysLog(value="删除定时任务",system = SysModule.sys)
	@PostMapping("/delete")
	@RequiresPermissions("job:schedule")
	public R<Object> delete(@RequestBody String[] jobIds){
		scheduleJobService.deleteBatch(jobIds);
		
		return R.ok();
	}
	
	/**
	 * 立即执行任务
	 */
	@SysLog(value="立即执行任务",system = SysModule.sys)
	@PostMapping("/run")
	@RequiresPermissions("job:schedule")
	public R<Object> run(@RequestBody String[] jobIds){
		scheduleJobService.run(jobIds);
		
		return R.ok();
	}
	
	/**
	 * 暂停定时任务
	 */
	@SysLog(value="暂停定时任务",system = SysModule.sys)
	@PostMapping("/pause")
	@RequiresPermissions("job:schedule")
	public R<Object> pause(@RequestBody String[] jobIds){
		scheduleJobService.pause(jobIds);
		
		return R.ok();
	}
	
	/**
	 * 恢复定时任务
	 */
	@SysLog(value="恢复定时任务",system = SysModule.sys)
	@PostMapping("/resume")
	@RequiresPermissions("job:schedule")
	public R<Object> resume(@RequestBody String[] jobIds){
		scheduleJobService.resume(jobIds);
		
		return R.ok();
	}

}
