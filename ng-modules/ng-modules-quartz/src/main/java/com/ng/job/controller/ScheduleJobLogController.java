 
package com.ng.job.controller;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ng.common.utils.PageUtils;
import com.ng.common.utils.R;
import com.ng.job.entity.ScheduleJobLogEntity;
import com.ng.job.service.ScheduleJobLogService;

/**
 * 定时任务日志
 *
 * @author lyf
 */
@RestController
@RequestMapping("/job/scheduleLog")
public class ScheduleJobLogController {
	@Autowired
	private ScheduleJobLogService scheduleJobLogService;
	
	/**
	 * 定时任务日志列表
	 */
	@GetMapping("/list")
	@RequiresPermissions("job:schedule")
	public R<PageUtils> list(ScheduleJobLogEntity entity , PageUtils pageUtils){
		PageUtils page = scheduleJobLogService.queryPage(entity,pageUtils);
		
		return R.ok(PageUtils.class).setData(page);
	}
	
	/**
	 * 定时任务日志信息
	 */
	@GetMapping("/info/{logId}")
	public R<ScheduleJobLogEntity> info(@PathVariable("logId") Long logId){
		ScheduleJobLogEntity log = scheduleJobLogService.getById(logId);
		
		return R.ok(ScheduleJobLogEntity.class).setData( log);
	}
}
