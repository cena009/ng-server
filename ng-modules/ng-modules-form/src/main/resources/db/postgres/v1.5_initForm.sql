CREATE TABLE ng_form_template
(
    id varchar(32) primary key,
    code varchar(20) not null,
    name varchar(200)  NOT NULL,
    template_data text ,
    version integer,
    status int DEFAULT 0,
    remark text ,
    create_id varchar(32)  NOT NULL,
    create_time timestamp   NOT NULL,
    update_id varchar(32) ,
    update_time timestamp 
) ;


create index on ng_form_template(code);

comment on table ng_form_template is '动态表单模板';
comment on column ng_form_template.id is 'ID';
comment on column ng_form_template.code is '编码';
comment on column ng_form_template.template_data is '模板内容json';
comment on column ng_form_template.version is '版本';
comment on column ng_form_template.status is '状态 0-暂存 1-发布';
comment on column ng_form_template.remark is '备注说明';
comment on column ng_form_template.create_id is '创建人';
comment on column ng_form_template.create_time is '创建时间';
comment on column ng_form_template.update_id is '更新人';
comment on column ng_form_template.update_time is '更新时间';

 
CREATE TABLE ng_form_data
(
    id varchar(32) primary key,
    form_data json,
    form_code varchar(20)  NOT NULL,
    form_id varchar(32)  NOT NULL, 
    create_id varchar(32)  NOT NULL,
    create_time timestamp  NOT NULL,
    update_id varchar(32) ,
    update_time timestamp 
);

create index on ng_form_data(create_id);

comment on table ng_form_data is '表单数据';
comment on column ng_form_data.id is 'ID';
comment on column ng_form_data.form_data is '表单填写数据';
comment on column ng_form_data.form_code is '使用表单模板的编码';
comment on column ng_form_data.form_id is '使用表单模板的ID'; 
comment on column ng_form_data.create_id is '创建人-表单填报人';
comment on column ng_form_data.create_time is '创建时间-表单填报时间';
comment on column ng_form_data.update_id is '创建人';
comment on column ng_form_data.update_time is '更新时间';



INSERT INTO ng_form_template(id, code, name, template_data, version, status, remark, create_id, create_time, update_id, update_time) 
VALUES ('c3b42638a4c5d7f9612b59e0075b116f', 'F00001', 'test', '{"list":[{"type":"input","label":"姓名","options":{"type":"text","width":"100%","defaultValue":"","placeholder":"请输入","clearable":false,"maxLength":0,"prepend":"","append":"","tooptip":"","hidden":false,"disabled":false,"dynamicHide":false,"dynamicHideValue":""},"model":"name","key":"input_1628838173424","rules":[{"required":true,"message":"姓名不能为空","trigger":["blur"]}]},{"type":"textarea","label":"地址","options":{"width":"100%","maxLength":0,"defaultValue":"","rows":4,"clearable":false,"tooptip":"","hidden":false,"disabled":false,"placeholder":"请输入","dynamicHide":false,"dynamicHideValue":""},"model":"address","key":"textarea_1628838173424","rules":[{"required":true,"message":"地址不能为空","trigger":["blur"]}]}],"config":{"labelPosition":"left","labelWidth":100,"size":"mini","outputHidden":true,"hideRequiredMark":true,"customStyle":"","showList":["name","address"],"queryList":["name","address"]}}', 1, 1, NULL, 'superAdmin', '2021-12-31 18:11:12.561', NULL, NULL);







create or replace function json_extract(jsonvalue_ json , path_ varchar) returns text as 
$$
	select json_extract_path_text($1, replace($2 , '$.' , ''))
$$
language sql immutable ;



