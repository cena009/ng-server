
-- ----------------------------
-- Table structure for ng_form_data
-- ----------------------------
DROP TABLE IF EXISTS `ng_form_data`;
CREATE TABLE `ng_form_data`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT 'ID',
  `form_data` json   COMMENT '表单填写数据',
  `form_code` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '使用表单模板的编码',
  `form_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '使用表单模板的ID',
  `create_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '创建人-表单填报人',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间-表单填报时间',
  `update_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `ng_form_data_create_id_idx`(`create_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '表单数据' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ng_form_data
-- ----------------------------
INSERT INTO `ng_form_data` VALUES ('184fe2c3a18988bc4400e301c6624c36', '{\"name\": \"22\", \"address\": \"222\"}', 'F00001', 'c3b42638a4c5d7f9612b59e0075b116f', 'superAdmin', '2022-02-09 17:31:12', NULL, NULL);
INSERT INTO `ng_form_data` VALUES ('919e793b9ee85fd00568be9a0b8c3bd3', '{\"name\": \"111\", \"address\": \"2222\"}', 'F00001', 'c3b42638a4c5d7f9612b59e0075b116f', 'superAdmin', '2022-02-09 17:30:39', NULL, NULL);

-- ----------------------------
-- Table structure for ng_form_template
-- ----------------------------
DROP TABLE IF EXISTS `ng_form_template`;
CREATE TABLE `ng_form_template`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT 'ID',
  `code` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '编码',
  `name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `template_data` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL COMMENT '模板内容json',
  `version` int(11) NULL DEFAULT NULL COMMENT '版本',
  `status` int(11) NULL DEFAULT NULL COMMENT '状态 0-暂存 1-发布',
  `remark` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL COMMENT '备注说明',
  `create_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '创建人',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `ng_form_template_code_idx`(`code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '动态表单模板' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ng_form_template
-- ----------------------------
INSERT INTO `ng_form_template` VALUES ('c3b42638a4c5d7f9612b59e0075b116f', 'F00001', 'test', '{\"list\":[{\"type\":\"input\",\"label\":\"姓名\",\"options\":{\"type\":\"text\",\"width\":\"100%\",\"defaultValue\":\"\",\"placeholder\":\"请输入\",\"clearable\":false,\"maxLength\":0,\"prepend\":\"\",\"append\":\"\",\"tooptip\":\"\",\"hidden\":false,\"disabled\":false,\"dynamicHide\":false,\"dynamicHideValue\":\"\"},\"model\":\"name\",\"key\":\"input_1628838173424\",\"rules\":[{\"required\":true,\"message\":\"姓名不能为空\",\"trigger\":[\"blur\"]}]},{\"type\":\"textarea\",\"label\":\"地址\",\"options\":{\"width\":\"100%\",\"maxLength\":0,\"defaultValue\":\"\",\"rows\":4,\"clearable\":false,\"tooptip\":\"\",\"hidden\":false,\"disabled\":false,\"placeholder\":\"请输入\",\"dynamicHide\":false,\"dynamicHideValue\":\"\"},\"model\":\"address\",\"key\":\"textarea_1628838173424\",\"rules\":[{\"required\":true,\"message\":\"地址不能为空\",\"trigger\":[\"blur\"]}]}],\"config\":{\"labelPosition\":\"left\",\"labelWidth\":100,\"size\":\"mini\",\"outputHidden\":true,\"hideRequiredMark\":true,\"customStyle\":\"\",\"showList\":[\"name\",\"address\"],\"queryList\":[\"name\",\"address\"]}}', 1, 1, NULL, 'superAdmin', '2021-12-31 18:11:13', NULL, NULL);
