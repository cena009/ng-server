package com.ng.form.entity;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.handlers.FastjsonTypeHandler;
import com.ng.common.typeHandler.JsonTypeHandler;
import com.ng.common.validator.group.AddGroup;
import com.ng.common.validator.group.UpdateGroup;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 表单填写数据
 * 
 * @author  lyf
 * @date 2021-03-30 11:19:44
 */ 
@TableName(value="ng_form_data"   ,autoResultMap = true) 
@ApiModel("表单填写数据")
@Data
public class FormDataEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * ID
	 */
	@NotBlank(message="ID不能为空", groups = {UpdateGroup.class})
	@TableId(type=IdType.UUID)
	@TableField("id") 
	@ApiModelProperty("ID")
	private String id;
	/**
	 * 填写数据
	 */
	@TableField(value="form_data",typeHandler= JsonTypeHandler.class  ) 
	@ApiModelProperty("填写数据")
	private JSONObject formData;
 
	/**
	 * 模板code
	 */
	@TableField("form_code") 
	@ApiModelProperty("模板code")
	@NotBlank(message="模板code不能为空", groups = {AddGroup.class, UpdateGroup.class})
	@Max(value = 20,message = "模板code最大长度为20")
	private String formCode;
	/**
	 * 模板ID
	 */
	@TableField("form_id") 
	@ApiModelProperty("模板ID")
	@NotBlank(message="模板ID不能为空", groups = {AddGroup.class, UpdateGroup.class})
	@Max(value = 32,message = "模板ID最大长度为32")
	private String formId;
	 
	 
	/**
	 * 创建人
	 */
	@TableField("create_id") 
	@ApiModelProperty("创建人")
	//@NotBlank(message="创建人不能为空", groups = {AddGroup.class, UpdateGroup.class})
	//@Max(value = 32,message = "创建人最大长度为32")
	private String createId;
	/**
	 * 创建时间
	 */
	@TableField("create_time") 
	@ApiModelProperty("创建时间")
	private Date createTime;
	/**
	 * 更新人
	 */
	@TableField("update_id") 
	@ApiModelProperty("更新人")
	private String updateId;
	/**
	 * 更新时间
	 */
	@TableField("update_time") 
	@ApiModelProperty("更新时间")
	private Date updateTime;

}
