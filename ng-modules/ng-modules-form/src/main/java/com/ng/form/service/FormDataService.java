package com.ng.form.service;
 
import java.util.Map;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ng.common.utils.PageUtils;
import com.ng.form.entity.FormDataEntity;
 

/**
 * 表单填写数据
 *
 * @author lyf 
 * @date 2021-03-30 11:19:44
 */
public interface FormDataService extends IService<FormDataEntity> {

    PageUtils queryPage(String code , Map<String,Object> params , PageUtils page );
 
    
}

