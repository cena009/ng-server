package com.ng.form.service.impl;

import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ng.common.security.SecurityUtils;
import com.ng.common.utils.PageUtils;
import com.ng.common.validator.Assert;
import com.ng.form.dao.FormDataDao;
import com.ng.form.entity.FormDataEntity;
import com.ng.form.service.FormDataService;
import com.ng.form.service.FormTemplateService;
import com.ng.sys.entity.SysUserEntity;
 


@Service("formDataService")
public class FormDataServiceImpl extends ServiceImpl<FormDataDao, FormDataEntity> implements FormDataService {

	@Autowired
	private SecurityUtils securityUtils;
	
	@Autowired
	private FormTemplateService formTemplateService ;
	
    @Override
    public PageUtils queryPage(String code , Map<String,Object> params , PageUtils page ) {
        
    	Assert.isNull(code, "模板编码不能为空");
    	
    	QueryWrapper<FormDataEntity> query = new QueryWrapper<FormDataEntity>();
    	query.eq("form_code", code);
    	
    	if(params != null && !params.isEmpty()) {
    		Set<Entry<String, Object>> es = params.entrySet();
    		
    		for(Entry<String, Object> e : es) {
    		 
    			String key = e.getKey();
    			
    			if(key.endsWith("_label") || key.equals("code")) continue ;
    			
    			Object value = e.getValue();
    			//query.like(StringUtils.isNotBlank(key) && StringUtils.isNotBlank(value.toString()), "json_extract(form_data)   " + "'" + key + "'" , value);
    			
    			
    			query.apply(StringUtils.isNotBlank(key) && StringUtils.isNotBlank(value.toString()), "json_extract(form_data , {0}) like concat('%' , {1} , '%')", "$." + key , value);
    		
    		}
    		
    	}
    	
    	query.orderByDesc("create_time");
    	
        IPage<FormDataEntity> ipage = this.page(
                page.getPage(FormDataEntity.class),
                query
        );

        return new PageUtils(ipage);
    }
    
    
    @Override
    public boolean save(FormDataEntity entity) {
    	
    	SysUserEntity user = securityUtils.getCurrentUser();
    	entity.setCreateId(user.getUserId());
    	entity.setCreateTime(new Date());
    	
    	// 表单后台校验
    	String error = formTemplateService.validatorFormData(entity.getFormData(), entity.getFormId());
    	
    	Assert.expression(error != null, error);
    	
    	//entity.setStatus(0);
    	 
    	
    	return super.save(entity);
    }
    
    
    @Override
    public boolean updateById(FormDataEntity entity) {
    	
    	// 状态如果已经是1 则不允许修改
    	FormDataEntity old = getById(entity.getId());
    	
    	Assert.isNull(old, "历史记录不存在");
    	 
    	// 表单后台校验
    	String error = formTemplateService.validatorFormData(entity.getFormData(), entity.getFormId());
    	
    	Assert.expression(error != null, error);
    	
    	SysUserEntity user = securityUtils.getCurrentUser();
    	
    	entity.setUpdateId(user.getUserId());
    	entity.setUpdateTime(new Date());
    	 
    	
    	return super.updateById(entity);
    }

 

}
