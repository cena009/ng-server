package com.ng.form.form;

import lombok.Data;
@Data
public class SolveForm{
	
	String id;
 
	String solveStatus ;
	
	String solveInfo;
}