package com.ng.form.entity;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ng.common.validator.group.AddGroup;
import com.ng.common.validator.group.UpdateGroup;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 动态表单模板
 * 
 * @author lyf 
 * @date 2021-03-30 11:19:44
 */ 
@TableName(value="ng_form_template"   ) 
@ApiModel("动态表单模板")
@Data
public class FormTemplateEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * ID
	 */
	@NotBlank(message="ID不能为空", groups = {UpdateGroup.class})
	@TableId(type=IdType.UUID)
	@TableField("id") 
	@ApiModelProperty("ID")
	private String id;
	/**
	 * 表单编号
	 */
	@TableField("code") 
	@ApiModelProperty("表单编号")
	//@NotBlank(message="表单编号不能为空", groups = {AddGroup.class, UpdateGroup.class})
	//@Max(value = 20,message = "表单编号最大长度为20")
	private String code;
	/**
	 * 模板名称
	 */
	@TableField("name") 
	@ApiModelProperty("模板名称")
	@NotBlank(message="模板名称不能为空", groups = {AddGroup.class, UpdateGroup.class})
	@Max(value = 200,message = "模板名称最大长度为200")
	private String name;
	/**
	 * 模板数据json
	 */
	@TableField("template_data") 
	@ApiModelProperty("模板数据json")
	@NotBlank(message="模板数据json不能为空", groups = {AddGroup.class, UpdateGroup.class})
	@Max(value = 0,message = "模板数据json最大长度为0")
	private String templateData;
	/**
	 * 版本
	 */
	@TableField("version") 
	@ApiModelProperty("版本")
	private Integer version;
	/**
	 * 状态 1-发布
	 */
	@TableField("status") 
	@ApiModelProperty("状态 1-发布")
	private Integer status;
	/**
	 * 创建人
	 */
	@TableField("create_id") 
	@ApiModelProperty("创建人")
	//@NotBlank(message="创建人不能为空", groups = {AddGroup.class, UpdateGroup.class})
	//@Max(value = 32,message = "创建人最大长度为32")
	private String createId;
	/**
	 * 创建时间
	 */
	@TableField("create_time") 
	@ApiModelProperty("创建时间")
	private Date createTime;
	/**
	 * 更新人
	 */
	@TableField("update_id") 
	@ApiModelProperty("更新人")
	private String updateId;
	/**
	 * 更新时间
	 */
	@TableField("update_time") 
	@ApiModelProperty("更新时间")
	private Date updateTime;
	/**
	 * 备注
	 */
	@TableField("remark") 
	@ApiModelProperty("备注")
	private String remark ;
	
	@TableField(exist=false)
	private String createUserName ;

}
