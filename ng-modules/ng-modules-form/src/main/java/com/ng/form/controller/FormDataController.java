package com.ng.form.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.ng.common.utils.PageUtils;
import com.ng.common.utils.R;
import com.ng.common.validator.ValidatorUtils;
import com.ng.common.validator.group.AddGroup;
import com.ng.common.validator.group.UpdateGroup;
import com.ng.form.entity.FormDataEntity;
import com.ng.form.service.FormDataService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse; 

/**
 * 表单填写数据
 *
 * @author lyf 
 * @date 2021-03-30 11:19:44
 */
@RestController
@RequestMapping("form/data")
@Api(value = "表单填写数据API", tags = { "表单填写数据API-通用" })
public class FormDataController {
	@Autowired
	private FormDataService formDataService;

	/**
	 * 列表
	 */
	@GetMapping("/list")
	@RequiresPermissions("form:data:list")
	@ApiOperation("表单填写数据列表")
	@ApiResponse(code=0,message="查询成功",response=FormDataEntity.class)
	public R<PageUtils> list(String code , @RequestParam Map<String,Object> query , PageUtils page){

		Map<String,Object> params = new HashMap<>();
		
		String q = (String) query.get("query");
		if(StringUtils.isNotBlank(q)) {
			params = JSON.parseObject(q);
		}
	 
		PageUtils pageData = formDataService.queryPage(code , params ,page);

		return R.ok(PageUtils.class).setData(pageData);
	}


	/**
	 * 信息
	 */
	@GetMapping("/info/{id}") 
	@ApiOperation("根据ID获取表单填写数据信息")
	@ApiResponse(code=0,message="查询成功",response=FormDataEntity.class)
	public R<FormDataEntity> info(@PathVariable("id") String id){
		FormDataEntity formData = formDataService.getById(id);

		return R.ok(FormDataEntity.class).setData( formData);
	}

	/**
	 * 保存
	 */
	@PostMapping("/save")
	@RequiresPermissions("form:data:save")
	@ApiOperation("保存表单填写数据信息") 
	public R<Object> save(@RequestBody FormDataEntity entity){
		ValidatorUtils.validateEntity(entity, AddGroup.class);
		formDataService.save(entity);

		return R.ok();
	}
	
	 

	/**
	 * 修改
	 */
	@PostMapping("/update")
	@RequiresPermissions("form:data:update")
	@ApiOperation("修改表单填写数据信息") 
	public R<Object> update(@RequestBody FormDataEntity entity){
		ValidatorUtils.validateEntity(entity, UpdateGroup.class);
		  
		formDataService.updateById(entity);
		 
		return R.ok().setData(entity.getId());
	}

	/**
	 * 删除
	 */
	@PostMapping("/delete")
	@RequiresPermissions("form:data:delete")
	@ApiOperation("删除表单填写数据信息") 
	public R<Object> delete(@RequestBody String[] ids){
		formDataService.removeByIds(Arrays.asList(ids));

		return R.ok();
	}

	
	
	
}

