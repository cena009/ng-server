package com.ng.form.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ng.form.entity.FormTemplateEntity;

/**
 * 动态表单模板
 * 
 * @author lyf 
 * @date 2021-03-30 11:19:44
 */
@Mapper
public interface FormTemplateDao extends BaseMapper<FormTemplateEntity> {
	
	/*推荐在此直接添加注解写sql,可读性比较好*/
//	@Select(" <script>"+ 
//			"with t1 as ( " + 
//			"	select t1.id,t1.name , t1.create_time ,t1.version, t1.create_id , t1.code ,t1.status ,row_number() over(partition by code order by version desc) idx from ng_form_template t1 " + 
//			") " + 
//			" select t1.*, t2.username as create_user_name from  t1 left join sys_user t2 on t1.create_id = t2.user_id " + 
//			" where idx = 1 " +  
//			 " <if test=\"entity.name!=null and entity.name!=''\">"+
//			  "  and t1.name like concat(concat('%', #{entity.name}),'%')"+
//			  " </if >" + 
//			  " order by t1.create_time desc "+
//			 "</script>")
	@Select(" <script>"
			+  "select n1.* "
			+ " from ng_form_template n1 ,  "
			+ " (select t1.code , max(t1.version) as version from ng_form_template t1 group by t1.code ) n2  "
			+ " where n1.code = n2.code and n1.version = n2.version"
			 +" <if test=\"entity.name!=null and entity.name!=''\">"
			 + "  and n1.name like concat(concat('%', #{entity.name}),'%') "
			 + " </if >" 
			 + " order by n1.create_time desc "
			 +  "</script>")
	public IPage<FormTemplateEntity> page(Page<FormTemplateEntity> page , @Param("entity") FormTemplateEntity entity);
	
	
	@Select("select max(substr(code,2)) from ng_form_template")
	public String getMaxCode();
}
