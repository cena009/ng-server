package com.ng.form.controller;

import java.util.Arrays;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ng.common.utils.PageUtils;
import com.ng.common.utils.R;
import com.ng.common.validator.Assert;
import com.ng.common.validator.ValidatorUtils;
import com.ng.common.validator.group.AddGroup;
import com.ng.common.validator.group.UpdateGroup;
import com.ng.form.entity.FormTemplateEntity;
import com.ng.form.service.FormTemplateService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse; 

/**
 * 动态表单模板
 *
 * @author lyf 
 * @date 2021-03-30 11:19:44
 */
@RestController
@RequestMapping("form/template")
@Api(value = "动态表单模板API", tags = { "动态表单模板API-通用" })
public class FormTemplateController {
	@Autowired
	private FormTemplateService formTemplateService;

	/**
	 * 列表
	 */
	@GetMapping("/list")
	@RequiresPermissions("form:template:list")
	@ApiOperation("动态表单模板列表")
	@ApiResponse(code=0,message="查询成功",response=FormTemplateEntity.class)
	public R<PageUtils> list(FormTemplateEntity entity , PageUtils page){

		PageUtils pageData = formTemplateService.queryPage(entity , page);

		return R.ok(PageUtils.class).setData(pageData);
	}


	/**
	 * 信息
	 */
	@GetMapping("/info/{id}") 
	@ApiOperation("根据ID获取动态表单模板信息")
	@ApiResponse(code=0,message="查询成功",response=FormTemplateEntity.class)
	public R<FormTemplateEntity> info(@PathVariable("id") String id){
		FormTemplateEntity formTemplate = formTemplateService.getById(id);

		return R.ok(FormTemplateEntity.class).setData( formTemplate);
	}
	
	/**
	 * 信息
	 */
	@GetMapping("/queryByCode") 
	@ApiOperation("根据CODE获取动态表单模板信息")
	@ApiResponse(code=0,message="查询成功",response=FormTemplateEntity.class)
	public R<FormTemplateEntity> queryByCode(String code){
		FormTemplateEntity formTemplate = formTemplateService.queryByCode(code);

		return R.ok(FormTemplateEntity.class).setData( formTemplate);
	}

	/**
	 * 保存
	 */
	@PostMapping("/save")
	@RequiresPermissions("form:template:update")
	@ApiOperation("保存动态表单模板信息") 
	public R<Object> save(@RequestBody FormTemplateEntity entity){
		ValidatorUtils.validateEntity(entity, AddGroup.class);
		formTemplateService.save(entity);

		return R.ok();
	}
	
	/**
	 *  撤回暂存
	 */
	@PostMapping("/revoke")
	@RequiresPermissions("form:template:update")
	@ApiOperation("保存动态表单模板信息") 
	public R<Object> revoke(@RequestBody FormTemplateEntity entity){
		
		Assert.isNull(entity, "表单模板为空");
		Assert.isBlank(entity.getId(), "表单模板为空");
		
		formTemplateService.revoke(entity.getId());

		return R.ok();
	}
	
	/**
	 *  发布
	 */
	@PostMapping("/publish")
	@RequiresPermissions("form:template:update")
	@ApiOperation("发布动态表单模板信息") 
	public R<Object> publish(@RequestBody FormTemplateEntity entity){
		
		Assert.isNull(entity, "表单模板为空");
		Assert.isBlank(entity.getId(), "表单模板为空");
		
		formTemplateService.publish(entity.getId());

		return R.ok();
	}

	/**
	 * 修改
	 */
	@PostMapping("/update")
	@RequiresPermissions("form:template:update")
	@ApiOperation("修改动态表单模板信息") 
	public R<Object> update(@RequestBody FormTemplateEntity entity){
		ValidatorUtils.validateEntity(entity, UpdateGroup.class);
		formTemplateService.updateById(entity);

		return R.ok();
	}

	/**
	 * 删除
	 */
	@PostMapping("/delete")
	@RequiresPermissions("form:template:delete")
	@ApiOperation("删除动态表单模板信息") 
	public R<Object> delete(@RequestBody String[] ids){
		formTemplateService.removeByIds(Arrays.asList(ids));

		return R.ok();
	}

}
