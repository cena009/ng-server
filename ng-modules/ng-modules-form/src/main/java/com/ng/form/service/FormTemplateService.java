package com.ng.form.service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ng.common.utils.PageUtils;
import com.ng.form.entity.FormTemplateEntity;


/**
 * 动态表单模板
 *
 * @author lyf 
 * @date 2021-03-30 11:19:44
 */
public interface FormTemplateService extends IService<FormTemplateEntity> {

	PageUtils queryPage(FormTemplateEntity entity , PageUtils page );

	/**
	 * 发布
	 * @param id
	 * @return
	 */
	boolean publish(String id );
	
	/**
	 * 版本撤回 只针对暂存
	 * @param id
	 * @return
	 */
	boolean revoke(String id);
	
	/**
	 * 根据编号查找
	 * @param code
	 * @return
	 */
	FormTemplateEntity queryByCode(String code);
	
	/**
	 * 校验表单填写的内容
	 * @param data 填写的内容
	 * @param formId 表单模板ID
	 * @return 如果校验异常则返回异常字符串 校验合格返回null 
	 */
	String validatorFormData(JSONObject data , String formId);
	
}

