package com.ng.form.dao;

import org.apache.ibatis.annotations.Mapper; 
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ng.form.entity.FormDataEntity;

/**
 * 表单填写数据
 * 
 * @author lyf 
 * @date 2021-03-30 11:19:44
 */
@Mapper
public interface FormDataDao extends BaseMapper<FormDataEntity> {
	
	/*推荐在此直接添加注解写sql,可读性比较好*/
	
}
